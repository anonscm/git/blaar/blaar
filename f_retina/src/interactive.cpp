//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//


/*
 set palette defined (0      1 1 0, \ # jaune
 0.1666 0 1 0, \ # vert
 0.3333 0 1 1, \ # cyan
 0.5    0 0 1, \ # bleu
 0.6666 1 0 1, \ # magenta
 0.8333 1 0 0, \ # rouge
 1      1 1 0)
 
 */

#include "interactive.hpp"
#include "blQTKit.h"
#include "main.hpp"
#include "blc.h"
#include <sys/signal.h>
#include <stdlib.h>

/**
 @main
 Exemple of using the console to display the camera
*/

static int initialised=0;
static int width, height = 0;
static int columns_nb = 206, rows_nb = 80;
static int is_zoom=0;
static int component_offset=1;
static timeval timestamp;

static void quit()
{
    fflush(stdout);
    print_escape_command("0m");
    print_escape_command(DEL_END_SCREEN);
    printf("\nQuitting QTKit_camera.\n");
}

void resize_callback(int new_columns_nb, int new_rows_nb)
{
    columns_nb = new_columns_nb;
    rows_nb = new_rows_nb;
}

void command_callback(char const *command, char const *, void *)
{
    int i;
    print_escape_command("0m");
    switch (*command)
    {
        case   0:if (status==RUN)
        {
            status=PAUSE;
            FOR(i, BLC_BAR_COLORS_NB-1) color_printf(blc_bar_colors[i],"%d < ", i*256/BLC_BAR_COLORS_NB);
            color_printf(blc_bar_colors[i],"%d\n", i*256/BLC_BAR_COLORS_NB);
            printf("%d iterations, average period: %.3lf ms\n", iteration, period/1000.);
            iteration=0;
            blc_command_display_help();
            blc_command_interpret();
        } else status=RUN;
        break;
        case 'c':component_offset=1-component_offset;break;
        case 'q':status=STOP;break;
        case 'z':is_zoom=1-is_zoom; break;
    }
}

void interactive_mode_callback(CVImageBufferRef videoFrame, void *user_data)
{
    blc_mem mem;
    int offset, step1, length1, step2, length2;
	OSType pixel_format;
    (void)user_data;

    us_time_diff(&timestamp);

	if (!initialised)
    {
		pixel_format = CVPixelBufferGetPixelFormatType(videoFrame);
        if (pixel_format != STRING_TO_UINT32("yuv2")) EXIT_ON_ERROR("Format %.4s is not managed.", (char*)&pixel_format);
		width = CVPixelBufferGetWidth(videoFrame); //2 : bytes per pixel
		height = CVPixelBufferGetHeight(videoFrame);
		initialised = 1;
    }
    
	CVPixelBufferLockBaseAddress(videoFrame, 0);
	mem.data = (char*)CVPixelBufferGetBaseAddress(videoFrame);
    mem.size = width*height*2;

    length1=columns_nb/2;
    length2=rows_nb;
    
    if (is_zoom)
    {
        step1=2;
        step2=width*2;
        offset=component_offset+(int)((width-length1)/2)*2+((int)(height-length2)/2)*step2;
    }
    else
    {
        step1 = (int)(width/length1)*2;
        step2=(int)(height/length2)*width*2;
        offset=component_offset; // 0 for color
    }
    fprint_3Dmatrix(stdout, &mem, offset, 2, 1, step1, length1, step2, length2);
    CVPixelBufferUnlockBaseAddress(videoFrame, 0);
    period = period + ((double)us_time_diff(&timestamp)-period)/(double)iteration;
    blc_command_try_to_interpret();
    if (status==STOP) exit(0);
    print_escape_command("0,0H");
    iteration++;
}

void start_interactive_mode()
{
    if (!force_ansi)  terminal_ansi_detect();
    if (ansi_terminal) terminal_get_size(&columns_nb, &rows_nb);
    
    terminal_set_resize_callback(resize_callback);
    blc_command_add("", command_callback, NULL, "pause/run", NULL);
    blc_command_add("c", command_callback, NULL, "change the component (Y-UV)", (void*)"c");
    blc_command_add("q", command_callback, NULL, "quit", (void*)"q");
    blc_command_add("z", command_callback, NULL, "zoom/unzoom", (void*)"z");
    atexit(quit);
    init_capture(interactive_mode_callback, NULL);
}





