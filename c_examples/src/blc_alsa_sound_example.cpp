#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <math.h>
#include "blc.h"

#define EXIT_ON_SOUND_ERROR(err, msg, ...) EXIT_ON_ERROR("ALSA:%s\n\t"msg, snd_strerror(err), ##__VA_ARGS__)

// Size of the buffer in order to compute average intensity. It does not influence the calculus of direction but the display.
#define SOUND_FRAMES_NB 256
//forgetting factor. 1 means instantaneous value, 0 means average on all the samples. 0.1 allows some time filtering.
#define MU 0.01
// This define the threshold of sound intensity under which the position is considered as impossible to predict
#define CONFIDENCE_THREHOLD 0.1

enum {
   LEFT = 0, RIGHT, POSITION_HISTOGRAM, GRAPHS_NB
};

snd_pcm_t *init_sound(char const *device_name)
{
   int result, dir = 0;
   unsigned int sample_rate = 44100;
   snd_pcm_hw_params_t *hw_params = NULL;
   snd_pcm_t *capture_handle = NULL;

   result = snd_pcm_open(&capture_handle, device_name, SND_PCM_STREAM_CAPTURE, 0);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, " Cannot open audio device %s.\n\t You can give it as first argument. Default is 'hw:0,0'.", device_name);
   result = snd_pcm_hw_params_malloc(&hw_params);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot allocate hardware parameter structure");
   result = snd_pcm_hw_params_any(capture_handle, hw_params);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot initialize hardware parameter structure");
   result = snd_pcm_hw_params_set_access(capture_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set access type");
   result = snd_pcm_hw_params_set_format(capture_handle, hw_params, SND_PCM_FORMAT_S16_LE);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set sample format");
   result = snd_pcm_hw_params_set_rate_near(capture_handle, hw_params, &sample_rate, &dir);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set sample rate");
   result = snd_pcm_hw_params_set_channels(capture_handle, hw_params, 2);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set channel count");
   result = snd_pcm_hw_params(capture_handle, hw_params);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set parameters");
   snd_pcm_hw_params_free(hw_params);
   result = snd_pcm_prepare(capture_handle);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot prepare audio interface for use");
   return capture_handle;
}

int main(int argc, char *argv[])
{
   blc_mem graphs[GRAPHS_NB];
   char const *graph_titles[GRAPHS_NB] =
      { "left", "right", "position" };
   char const *device_name = "hw:0,0";
   int i, j, graph_size, run;
   int result, width, height, position;
   double intensity0, intensity1, confidence, e_left, e_right;
   double consolidation = 0;
   double direction = 0;
   int16_t buf[SOUND_FRAMES_NB*2] =  { 0 };
   snd_pcm_t *capture_handle = NULL;

   console_get_size(&width, &height);

   if (argc >= 2) device_name = argv[1];
   if (argc >= 3) width = atoi(argv[2]);
   if (argc >= 4) height = atoi(argv[3]);

   if (width == 0) width = 64;
   if (height == 0) height = 16;
   else height--;

   graph_size = (width - GRAPHS_NB) / (GRAPHS_NB * 3);
   graphs[LEFT].allocate(graph_size);
   graphs[RIGHT].allocate(graph_size);
   graphs[POSITION_HISTOGRAM].allocate(graph_size);

   capture_handle = init_sound(device_name);

   i = 0;
   run = 1;

   while (run)
   {
      if ((result = snd_pcm_readi(capture_handle, buf, SOUND_FRAMES_NB)) != SOUND_FRAMES_NB)
      {
         EXIT_ON_SOUND_ERROR(result, "read from audio interface failed (%s)\n");
      }

      intensity0 = 0;
      intensity1 = 0;

      FOR (j, SOUND_FRAMES_NB)
      {
         e_left = fabs(buf[j * 2]) / (1 << 15);
         e_right = fabs(buf[j * 2 + 1]) / (1 << 15);
         confidence = (e_left + e_right) / 2;

         intensity0 += e_left;
         intensity1 += e_right;

         if (confidence > CONFIDENCE_THREHOLD)
         {
            position = graph_size * e_right / (e_left + e_right);
            graphs[POSITION_HISTOGRAM].data[position] += confidence / CONFIDENCE_THREHOLD;
            consolidation += confidence - MU * consolidation; //Similar to hebb
            direction += confidence / consolidation * ((e_right - e_left) - direction);
         }
      }

      intensity0 /= SOUND_FRAMES_NB;
      intensity1 /= SOUND_FRAMES_NB;

      graphs[LEFT].data[i] = CLIP_UCHAR(intensity0 * 256);
      graphs[RIGHT].data[i] = CLIP_UCHAR(intensity1 * 256);

      if (i == graph_size)
      {
         fprint_ngraphs(stdout, graphs, graph_titles, GRAPHS_NB, height, 256, 0, "time or histogram", "intensity");
         printf("direction %lf consolidation %lf", direction, consolidation);
         print_escape_command(MOVE_N_UP_CURSOR, height);
         graphs[POSITION_HISTOGRAM].reset();
         i = 0;
      }
      else i++;
   }

   FOR(i, GRAPHS_NB)
      free(graphs[i].data);

   snd_pcm_close(capture_handle);
   exit(0);
}

