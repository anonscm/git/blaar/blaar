#include "blc.h"
#include <unistd.h> //usleep

blc_channel *channel;

void command_callback(blc_mem const *mem, void* pointer)
{
    switch (*(char*)pointer)
    {
            case 'h': printf_underline('-', "Commands");
            blc_command_display_help();
            break;
            case 'r':
            sem_wait(channel->sem_block);
            sem_wait(channel->sem_prot);
            printf("%s\n", channel->mem.data);
            if (channel->sem_ack) sem_post(channel->sem_ack);
            else SYSTEM_ERROR_CHECK(sem_post(channel->sem_prot), -1, "");
            break;
            case 'w':
            sem_wait(channel->sem_ack);
            sem_wait(channel->sem_prot);
            memcpy(channel->mem.data, mem->data, strlen(mem->data));
            sem_post(channel->sem_prot);
            sem_post(channel->sem_block);
            break;
            case 'q': printf("Quitting\n");
            exit(0);
            break;
    }
}


int main(int argc, char **argv)
{
    // avoid "unused variable" warning
    (void)argc;
    (void)argv;
    
    console_ansi_detect();
    
    printf("\n");
    printf_underline('=', "blc_channel_example");
    printf("\n");
    
    blc_command_add("h", command_callback, NULL, "Display help.", "h");
    blc_command_add("q", command_callback, NULL, "Quit the application.", "q");
    blc_command_add("r", command_callback, NULL, "Read to read from the channel.", "r");
    blc_command_add("w", command_callback, "Text to write to the channel.", "Write texte.", "w");

    
    channel = create_blc_channel("/blc_channel_example", LINE_MAX);
    
    while(1)
    {
        blc_command_display_help();
        blc_command_interpret();
        printf("\n");
    }
    return EXIT_SUCCESS;
}