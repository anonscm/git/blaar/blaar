#include "blc.h"
#include "blc_channel.h"
#include "string.h"
#include <sys/mman.h>


#define TOTO_SIZE 64

blc_channel *toto = NULL;

static void *display_data(blc_mem *mem, void*)
{
    (void)mem;
    printf("%.64s\n", toto->data);
    return NULL;
}

static void *log_display(blc_mem *mem, void*)
{
    void *buf;
    size_t size;
    (void)mem;
    
    if (blc_log_file== NULL) printf("No log yet\n");
    else
    {
        size = ftell(blc_log_file);
        
        buf = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fileno(blc_log_file), 0 );
        if( buf == MAP_FAILED) EXIT_ON_SYSTEM_ERROR("mmap");
        if (fwrite(buf,  size, 1, stdout)!=1) EXIT_ON_SYSTEM_ERROR("Displaying log");
        munmap(buf, size);
    }
    return NULL;
}


static void *send(blc_mem *mem, void *arg)
{
    blc_channel *channel = (blc_channel*)arg;

    memcpy(channel->data, mem->data, mem->size);
    channel->send();
    printf("send\n");
    return NULL;
}

static void *quit(blc_mem *mem, void*)
{
    (void) mem;
    
    printf("Quitting\n");
    
    exit(0);
    return NULL;
}

void* display_help(blc_mem *mem, void*)
{
    (void)mem;
    printf_underline('-', "Commands");
    blc_command_display_help();
    return NULL;
}

void on_receive_data(blc_mem *mems, int mems_nb, void *arg)
{
    printf("receive %.*s bytes\n", 64, mems->data);
}

int main(int argc, char **argv)
{
    sockaddr_in addr;
    ssize_t ret;
    blc_mem var;
/*
    blc_network network(inet_addr("127.0.0.1"), 3333, BLC_UDP4);
    blc_server server(3333, BLC_UDP4, on_receive_data, NULL, 1, 10);
 //   start_channel_server();
    
 //   toto = new blc_channel("toto", TOTO_SIZE, "127.0.0.1");
    
    printf("\n");
    printf_underline('=', "blc network");
    printf("\n");
    
    blc_command_add("d", display_data, NULL, "Display current data.", NULL);
    blc_command_add("h", display_help, NULL, "Display help.", NULL);
    blc_command_add("l", log_display, NULL, "Display log data.", NULL);
    blc_command_add("s", send, "Text to send", "Send the following text.", toto);
    blc_command_add("q", quit, NULL, "Quit the application.", NULL);
    
    display_help(NULL, NULL);
    var.data = (char*)"arg";
    var.size = 5;
    
    network.message.msg_iov = (struct iovec*)&var;
    network.message.msg_iovlen = 1;
    
  //  set_sockaddr_in(&addr, AF_INET, inet_addr("127.0.0.1"), 3333);
    while(1)
    {
        blc_command_interpret();
  //      SYSTEM_ERROR_CHECK(ret = sendmsg(network.socket_fd, &network.message,  0), -1, "sending data" );
        SYSTEM_ERROR_CHECK(ret = sendto(network.socket_fd, (void*)"but", 4, 0,  (struct sockaddr*)&network.socket_address, sizeof(network.socket_address)), -1, "sending data" );

        printf("%ld", ret);
    }*/
    return EXIT_SUCCESS;
}