#include <getopt.h>
#include "blc.h"

#define MESSAGE_MAX 64
#define PORT "35000"

char const *is_server=NULL;
char const *is_client=NULL;
char const *target_host = NULL;

void on_server_receive(char *data, size_t size, void *arg)
{
    (void)arg;
    color_printf(BLC_BRIGHT_GREEN, "Server receiving size: %ld, text:'%s'\n", size, data);
}

int main(int argc, char **argv)
{
    blc_network client;
    blc_server server;
    ssize_t message_size, sent_size;
    size_t allocated_memory = 0;
    
    console_ansi_detect();
    program_option_add(&is_client, 'c', "client", no_argument, "set as a client");
    program_option_add(&target_host, 'h', "hostname", required_argument, "<ip> address of the target if client.");
    program_option_add(&is_server, 's', "server", no_argument, "set as a server");
    program_option_interpret(&argc, &argv);

    if (argc != 1)
    {
        printf("usage: \n");
        program_option_display_help();
        EXIT_ON_ERROR("Wrong arguments '%s'", argv[1]);
    }
    
    if (is_server==NULL && is_client==NULL) is_client=is_server="1"; // By default it si client and server.
    if (is_client && target_host == NULL) target_host = "127.0.0.1";
    if (!is_client && target_host!=NULL) EXIT_ON_ERROR("It is meaningless to give a target address to a server. the server is always local.");
    
    printf(" \n");
    printf_underline('=', "blc network example");
    
    if (is_server)
    {
        printf_underline('-', "Server");
        printf(" Starting the server on port '%s'\n\n", PORT);
        blc_server_allocate_mem_and_start(&server, PORT, BLC_UDP4, on_server_receive, NULL, MESSAGE_MAX);
        if (!is_client)
        {
            printf("Press enter to quit\n\n");
            getchar();
        }
    }
  
    if (is_client)
    {
        printf_underline('-', "Client");
        printf(" Sending to '%s' on port '%s'\n\n", target_host, PORT);
        blc_network_init(&client, target_host, PORT, BLC_UDP4);
        
        do
        {
            printf("Message to send or 'quit':\n");
            SYSTEM_ERROR_CHECK(message_size = getline(&client.mem.data, &allocated_memory, stdin), -1, NULL);
            client.mem.data[message_size-1] = 0; //removing the last '\n'
            client.mem.size = message_size; // We send only what is useful. allocated_memory and size is different which is not conventional.
            SYSTEM_ERROR_CHECK(sent_size = sendmsg(client.socket_fd, &client.msghdr, 0), -1, NULL);
            printf("Sending %ld bytes.\n", sent_size);
        }while(strcmp(client.mem.data, "quit") != 0);
    }
    
    printf("Quiting 'blc_network_example'\n");
    return 0;
}

