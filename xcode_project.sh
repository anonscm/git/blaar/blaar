#!/bin/sh

current_dir=`pwd`
parent_dir=`dirname $current_dir`
build_dir="$parent_dir/`basename $current_dir`_build"

if (( $# != 1 )) 
then
echo "You need a project directory as argument." 
echo "Usage : xcode_project <project_directory>"
exit 1
fi

name=`basename $1`

developer_tools/create_project.sh $1 $build_dir xcode

if (( $? == 0 )) 
then 
open $build_dir/xcode/$1/$name.xcodeproj
fi