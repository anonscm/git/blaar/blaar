#!/bin/sh

blar_build_dir="${PWD}_build"

./compile.sh $1 "debug" $1 || {
	echo "fail compiling '$1'"
	exit 1
}

bin_dir="$blar_build_dir/debug_bin"

program=`basename $1`
shift      #remove $1
echo >&2
echo "execute: $bin_dir/$program $*" >&2
echo "'run' to start" >&2
echo >&2

command -v lld && lldb -- $bin_dir/$program $* || gdb  $bin_dir/$program $*




