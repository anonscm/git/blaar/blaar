Blaar works with any cmake from version 2.6. ( [cmake documentation](https://cmake.org/cmake/help/cmake2.6docs.html) )

The cmake builds are in `blaar_build/<project>`. You usually do not need to access here.

The makefiles produced are not human readable but you can use `make help` to see the usable targets. The important settings are stored in CMakeCache.txt. It is a good place to check where the lib are found or where different outputs will be.

When you think that your changes have no effect, you can remove this file and relaunch compilation. It is also safe to remove the whole `blaar_build/` directory. However, you should not need it, the important files to changes are **CMakeLists.txt** in the source root of each project.

CMake configuarion
==================

We use conventions which should make it easy to not use cmake but cmake make it easier. It manage dependencies and it is also good when using an integrated development editor ( eclipse, Xcode, ...). It will be also very useful to test the code and to produce bundles or packages.