//
//  Created by Arnaud Blanchard on 22/12/15.
//  Copyright ETIS 2015. All rights reserved.
//

#include "blcl.h"
#include "blc.h"

blc_channel *channel=NULL;
int pipe_mode=1;
struct blcl_target cpu_target;
char const *display, *expr_arg;
char *expr;
int width, height;

void display_mask()
{
    int i, j;
    
    fprintf(stderr, "\n");
    FOR(j,height)
    {
        FOR (i, width) fprintf(stderr, "%12.3f ", ((float*)channel->data)[i+j*channel->lengths[0]]);
        fprintf(stderr, "\n");
    }
}

void create_mask_cb(char const *argument, void *user_data)
{
    cl_int error_id;
    char *source_code=NULL;
    cl_mem mask;
    cl_kernel kernel;
    size_t work_group_sizes[2]={1, 1};
    size_t items_nbs[2];
    (void)user_data;
    
    if (strlen(argument)==0) {
        fprintf(stderr, "%s\n", expr);
        return;
    }
    free(expr);
    expr=strdup(argument);
    
    items_nbs[0]=width;
    items_nbs[1]=height;
    
    BLCL_ERROR_CHECK(mask = clCreateBuffer(cpu_target.context,  CL_MEM_USE_HOST_PTR , channel->size, channel->data, &error_id), NULL, error_id, NULL);
    asprintf(&source_code,"#define WIDTH %d\n#define HEIGHT %d\nkernel void create_filter(global float *mask){\nint i, j; float x, y; i=get_global_id(0); j=get_global_id(1);x=i-WIDTH/2.f+0.5f; y=j-HEIGHT/2.f+0.5f; mask[i+j*WIDTH]=%s;}", channel->lengths[0], channel->lengths[1], argument);
    kernel=BLCL_CREATE_KERNEL_1P(&cpu_target, "create_filter", source_code, mask);
    FREE(source_code);
    BLCL_CHECK(clEnqueueNDRangeKernel(cpu_target.command_queue, kernel, 2, NULL, items_nbs, work_group_sizes, 0, NULL, NULL), "Failed to execute kernel!");
    BLCL_CHECK(clFinish(cpu_target.command_queue), "finishing");
    
    if (pipe_mode) {
        printf(".");
        fflush(stdout);
    }
    clReleaseKernel(kernel);
    clReleaseProgram(cpu_target.program);
    
    if (display) display_mask();
}

void quit()
{
    blcl_release(&cpu_target);
    fprintf(stderr, "Quitting %s\n", blc_program_name);
    exit(0);
}

int main(int argc, char **argv)
{
    char const *interactive_mode, *format_string, *help, *size_string, *type_string, *channel_name;
    int  dims_nb, *lenghts;
    uint32_t type, format;
    
    program_option_add(&display, 'd', "display", 0, "print mask", NULL);
    program_option_add(&expr_arg, 'e', "expr", "expression", "c99 expression function of x and y.", "0"); //m|M are usualy for min and Max
    program_option_add(&format_string, 'f', "format", "TEXT|Y800", "set prefered format", "NDEF");
    program_option_add(&help, 'h', "help", 0, "display help", NULL);
    program_option_add(&interactive_mode, 'i', "interactive", 0, "set interactive mode", NULL);
    program_option_add(&size_string, 's', "size", "(integer)x(integer)", "size of the mask", "3x3");
    program_option_add(&type_string, 't', "type", "UIN8|FL32", "type of the value of the mask", "FL32");
    program_option_add(&channel_name, 'o', "output", "blc_channel", "output channel name", NULL);
    program_option_interpret(&argc, &argv);
    
    if (help) program_option_display_help();
    else{
        expr=strdup(expr_arg);
        
        type = STRING_TO_UINT32(type_string);
        format = STRING_TO_UINT32(format_string);
        if (channel_name==NULL) {
            asprintf((char**)&channel_name, "/image%d", getpid());
            printf("%s\n", channel_name);
            fflush(stdout);
        }
        else pipe_mode=0;
        
        dims_nb=blc_sscan_dims(&lenghts, size_string);
        switch (dims_nb){
            case 1: width=lenghts[0];
                height=1;
                channel=create_or_open_blc_channel(channel_name, "p", type, format, NULL, 1, width);
                break;
            case 2: width=lenghts[0];
                height=lenghts[1];
                channel=create_or_open_blc_channel(channel_name, "p", type, format, NULL, 2, width, height);
                break;
            default: EXIT_ON_ERROR("Dim nb of %d is not managed.", dims_nb);
        }
        
        underline_fprintf('=', stderr, "%s", blc_program_name);
        
        blcl_init(&cpu_target, CL_DEVICE_TYPE_CPU);
        create_mask_cb(expr, NULL);
        
        blc_command_add("expr=", create_mask_cb, "expression", "c99 expression function of x and y.", NULL);
        blc_command_add("d", (type_blc_command_cb)display_mask, NULL, "display mask", NULL);
        blc_command_add("q", (type_blc_command_cb)quit, NULL, "quit", NULL);
        blc_command_interpret_loop("h");
    }
    return 0;
}
