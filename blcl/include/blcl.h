//
//  hello.h
//  i_CL_QTKit_camera
//
//  Created by Arnaud Blanchard on 16/12/2015.
//
//

#ifndef BLCL_H
#define BLCL_H

#include "blc_tools.h"
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#define BLCL_CREATE_KERNEL_0P(context, name, source_code, param) blcl_create_kernel(context, name, source_code, (size_t)0 )
#define BLCL_CREATE_KERNEL_1P(context, name, source_code, param) blcl_create_kernel(context, name, source_code, sizeof(param), &(param), (size_t)0 )
#define BLCL_CREATE_KERNEL_2P(context, name, source_code, param0, param1) blcl_create_kernel(context, name, source_code, sizeof(param0), &param0, sizeof(param1), &param1, (size_t)0  )
#define BLCL_CREATE_KERNEL_3P(context, name, source_code, param0, param1, param2) blcl_create_kernel(context, name, source_code, sizeof(param0), &param0, sizeof(param1), &param1, sizeof(param2), &param2, (size_t)0  )
#define BLCL_CREATE_KERNEL_4P(context, name, source_code, param0, param1, param2, param3) blcl_create_kernel(context, name, source_code, sizeof(param0), &param0, sizeof(param1), &param1, sizeof(param2), &param2, sizeof(param3), &param3, (size_t)0 )
#define BLCL_CREATE_KERNEL_5P(context, name, source_code, param0, param1, param2, param3, param4) blcl_create_kernel(context, name, source_code, sizeof(param0), &param0, sizeof(param1), &param1, sizeof(param2), &param2, sizeof(param3), &param3, sizeof(param4), &param4, (size_t)0 )
#define BLCL_CREATE_KERNEL_6P(context, name, source_code, param0, param1, param2, param3, param4, param5) blcl_create_kernel(context, name, source_code, sizeof(param0), &param0, sizeof(param1), &param1, sizeof(param2), &param2, sizeof(param3), &param3, sizeof(param4), &param4, sizeof(param5), &param5, (size_t)0)

#define BLCL_ERROR_CHECK(command, result_error, error_id, ...) if ((command)==result_error) blcl_check(__FILE__, __FUNCTION__, __LINE__, error_id, STRINGIFY(command), __VA_ARGS__)
#define BLCL_CHECK(command, ...) blcl_check(__FILE__, __FUNCTION__, __LINE__, (command), STRINGIFY(command), __VA_ARGS__)

struct blcl_target{
    cl_int device_type;
    cl_context context;
    cl_device_id device_id;
    cl_command_queue command_queue;
    cl_program program;
};


START_EXTERN_C
    void blcl_check(const char *name_of_file, const char* name_of_function, int numero_of_line, cl_int error_id, char const *command, const char *message, ...);
    void blcl_fprint_device_info(FILE *file, cl_device_id device_id);
    void blcl_init(struct blcl_target *target, cl_int device_type_id);
    cl_kernel blcl_create_kernel(struct blcl_target *target, char const *name, char const *source_code, ...);
    void blcl_release(struct blcl_target *target);
END_EXTERN_C
#endif
