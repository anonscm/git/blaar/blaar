#include "blcl.h"

#include "blc.h"


#include <signal.h>

#define SET_ERROR(error_define) {error_define, #error_define}

#define BLCL_DEVICE_INFO_TEXT(file, device, info_id) blcl_print_device_info_text(file, device, info_id, #info_id)
#define BLCL_DEVICE_INFO_UINT(file, device, info_id) blcl_print_device_info_uint(file, device, info_id, #info_id)
#define BLCL_DEVICE_INFO_SIZE_T(file, device, info_id) blcl_print_device_info_size_t(file, device, info_id, #info_id)
#define BLCL_DEVICE_INFO_3SIZE_T(file, device, info_id) blcl_print_device_info_3size_t(file, device, info_id, #info_id)

struct error {
    cl_int id;
    const char *text;
};

static struct error errors[]=
{
    SET_ERROR(CL_SUCCESS),
    SET_ERROR(CL_DEVICE_NOT_FOUND),
    SET_ERROR(CL_DEVICE_NOT_AVAILABLE),
    SET_ERROR(CL_COMPILER_NOT_AVAILABLE),
    SET_ERROR(CL_MEM_OBJECT_ALLOCATION_FAILURE),
    SET_ERROR(CL_OUT_OF_RESOURCES),
    SET_ERROR(CL_OUT_OF_HOST_MEMORY),
    SET_ERROR(CL_PROFILING_INFO_NOT_AVAILABLE),
    SET_ERROR(CL_MEM_COPY_OVERLAP),
    SET_ERROR(CL_IMAGE_FORMAT_MISMATCH),
    SET_ERROR(CL_IMAGE_FORMAT_NOT_SUPPORTED),
    SET_ERROR(CL_BUILD_PROGRAM_FAILURE),
    SET_ERROR(CL_MAP_FAILURE),
    SET_ERROR(CL_MISALIGNED_SUB_BUFFER_OFFSET),
    SET_ERROR(CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST),
    SET_ERROR(CL_COMPILE_PROGRAM_FAILURE),
    SET_ERROR(CL_LINKER_NOT_AVAILABLE),
    SET_ERROR(CL_LINK_PROGRAM_FAILURE),
    SET_ERROR(CL_DEVICE_PARTITION_FAILED),
    SET_ERROR(CL_KERNEL_ARG_INFO_NOT_AVAILABLE),
    
    SET_ERROR(CL_INVALID_VALUE),
    SET_ERROR(CL_INVALID_DEVICE_TYPE),
    SET_ERROR(CL_INVALID_PLATFORM),
    SET_ERROR(CL_INVALID_DEVICE),
    SET_ERROR(CL_INVALID_CONTEXT),
    SET_ERROR(CL_INVALID_QUEUE_PROPERTIES),
    SET_ERROR(CL_INVALID_COMMAND_QUEUE),
    SET_ERROR(CL_INVALID_HOST_PTR),
    SET_ERROR(CL_INVALID_MEM_OBJECT),
    SET_ERROR(CL_INVALID_IMAGE_FORMAT_DESCRIPTOR),
    SET_ERROR(CL_INVALID_IMAGE_SIZE),
    SET_ERROR(CL_INVALID_SAMPLER),
    SET_ERROR(CL_INVALID_BINARY),
    SET_ERROR(CL_INVALID_BUILD_OPTIONS),
    SET_ERROR(CL_INVALID_PROGRAM),
    SET_ERROR(CL_INVALID_PROGRAM_EXECUTABLE),
    SET_ERROR(CL_INVALID_KERNEL_NAME),
    SET_ERROR(CL_INVALID_KERNEL_DEFINITION),
    SET_ERROR(CL_INVALID_KERNEL),
    SET_ERROR(CL_INVALID_ARG_INDEX),
    SET_ERROR(CL_INVALID_ARG_VALUE),
    SET_ERROR(CL_INVALID_ARG_SIZE),
    SET_ERROR(CL_INVALID_KERNEL_ARGS ),
    SET_ERROR(CL_INVALID_WORK_DIMENSION ),
    SET_ERROR(CL_INVALID_WORK_GROUP_SIZE ),
    SET_ERROR(CL_INVALID_WORK_ITEM_SIZE),
    SET_ERROR(CL_INVALID_GLOBAL_OFFSET),
    SET_ERROR(CL_INVALID_EVENT_WAIT_LIST),
    SET_ERROR(CL_INVALID_EVENT),
    SET_ERROR(CL_INVALID_OPERATION),
    SET_ERROR(CL_INVALID_GL_OBJECT),
    SET_ERROR(CL_INVALID_BUFFER_SIZE),
    SET_ERROR(CL_INVALID_MIP_LEVEL),
    SET_ERROR(CL_INVALID_GLOBAL_WORK_SIZE ),
    SET_ERROR(CL_INVALID_PROPERTY ),
    SET_ERROR(CL_INVALID_IMAGE_DESCRIPTOR),
    SET_ERROR(CL_INVALID_COMPILER_OPTIONS),
    SET_ERROR(CL_INVALID_LINKER_OPTIONS),
    SET_ERROR(CL_INVALID_DEVICE_PARTITION_COUNT)};

void blcl_check(const char *name_of_file, const char* name_of_function, int numero_of_line, cl_int error_id, char const *command, const char *message, ...)
{
    int i;
    va_list arguments;
    
    if (error_id!=CL_SUCCESS)
    {
        va_start(arguments, message);
        color_fprintf(BLC_BRIGHT_RED, stderr, "\n%s: %s \t %i:%s\n", blc_program_name, name_of_file, numero_of_line, name_of_function);
        fprintf(stderr, "Executing: %s\nOpenCL error '%d' :", command, error_id);
        
        FOR_INV(i, sizeof(errors)/sizeof(struct error)) if (error_id == errors[i].id) break;
        if (i==-1) color_fprintf(BLC_BRIGHT_RED, stderr, "Unknonw error\n");
        else color_fprintf(BLC_BRIGHT_RED, stderr, "%s .\n", errors[i].text+3);

        color_vfprintf(BLC_BRIGHT_RED, stderr, message, arguments);
        va_end(arguments);
        fprintf(stderr, "\n\n");
        fflush(stderr);
        raise(SIGABRT);
        exit(EXIT_FAILURE);
    }
}

cl_kernel blcl_create_kernel(struct blcl_target *target, char const *name, char const *source_code, ...)
{
    int i;
    cl_int error_id;
    size_t len, param_size;
    char buffer[1<<16];
    va_list arguments;
    void const *param_pt;
    cl_kernel kernel;
    
    // Create the compute program from the source buffer
    target->program = clCreateProgramWithSource(target->context, 1, (const char **)&source_code, NULL, &error_id);
    if (target->program==NULL)
    {
        clGetContextInfo(target->context, CL_CONTEXT_PROPERTIES,  sizeof(buffer), buffer, &len);
        
        FOR_INV(i, sizeof(errors)/sizeof(struct error)) if (error_id == errors[i].id) break;
        if (i==-1) color_fprintf(BLC_BRIGHT_RED, stderr, "Unknonw error\n");
        else color_fprintf(BLC_BRIGHT_RED, stderr, "%s .\n", errors[i].text);
        
        return NULL;
    }
    
    // Build the program executable
    if (clBuildProgram(target->program, 0, NULL, NULL, NULL, NULL) != CL_SUCCESS)
    {
        clGetProgramBuildInfo(target->program, target->device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        EXIT_ON_ERROR("Failed to build program executable : %*s\n", len, buffer);
    }
    
    clGetProgramBuildInfo(target->program, target->device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
    if (strlen(buffer)) color_fprintf(BLC_YELLOW, stderr, "Warning: %s\n", buffer);
    
    // Create the compute kernel in the program we wish to run
    BLCL_ERROR_CHECK(kernel=clCreateKernel(target->program, name, &error_id), NULL, error_id, "Fail creating kernel.");
    
    va_start(arguments, source_code);
    for (i=0; (param_size = va_arg(arguments, size_t)); i++)
    {
        param_pt=(void const *)va_arg(arguments, void const *);
        BLCL_CHECK(clSetKernelArg(kernel, i, param_size, param_pt), "Failed to set kernel argument '%d' of size %lu!", i, param_size);
    }
    va_end(arguments);
        return kernel;
}

void blcl_print_device_info_text(FILE *file, cl_device_id device_id, cl_device_info info_id, char const *title)
{
    char info[NAME_MAX+1];
    size_t real_size;
    
    clGetDeviceInfo(device_id, info_id, NAME_MAX, info, &real_size);
    if (real_size > NAME_MAX) EXIT_ON_ERROR("Device info too long %*s", NAME_MAX, info);
    fprintf(file, "%s: %s, ", title+strlen("CL_DEVICE_"), info);
}



cl_uint blcl_print_device_info_uint(FILE *file, cl_device_id device_id, cl_device_info info_id, char const *title)
{
    cl_uint value;
    size_t real_size;
    
    clGetDeviceInfo(device_id, info_id, sizeof(value), &value, &real_size);
    if (real_size != sizeof(value)) EXIT_ON_ERROR("Device value size is %llu instead of %llu in %s", real_size, sizeof(value), title);
    fprintf(file, "%s: %u, ", title+strlen("CL_DEVICE_"), value);
    return value;
}

size_t blcl_print_device_info_size_t(FILE *file, cl_device_id device_id, cl_device_info info_id, char const *title)
{
    size_t value;
    size_t real_size;
    
    clGetDeviceInfo(device_id, info_id, sizeof(value), &value, &real_size);
    if (real_size != sizeof(value)) EXIT_ON_ERROR("Device value size is %llu instead of %llu in %s", real_size, sizeof(value), title);
    fprintf(file, "%s: %lu, ", title+strlen("CL_DEVICE_"), value);
    return value;
}

void blcl_print_device_info_3size_t(FILE *file, cl_device_id device_id, cl_device_info info_id, char const *title)
{
    size_t value[3];
    size_t real_size;
    
    clGetDeviceInfo(device_id, info_id, sizeof(value), value, &real_size);
    if (real_size != sizeof(value)) EXIT_ON_ERROR("Device value size is %llu instead of %llu in %s", real_size, sizeof(value), title);
    fprintf(file, "%s: %lu,%lu,%lu, ", title+strlen("CL_DEVICE_"), value[0], value[1], value[2]);
}

void blcl_fprint_device_info(FILE *file, cl_device_id device_id)
{
    BLCL_DEVICE_INFO_TEXT(file, device_id, CL_DEVICE_NAME);
    BLCL_DEVICE_INFO_TEXT(file, device_id, CL_DEVICE_VERSION);
    BLCL_DEVICE_INFO_TEXT(file, device_id, CL_DEVICE_OPENCL_C_VERSION);
    BLCL_DEVICE_INFO_UINT(file, device_id, CL_DEVICE_MAX_COMPUTE_UNITS);
    BLCL_DEVICE_INFO_UINT(file, device_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS);
    BLCL_DEVICE_INFO_SIZE_T(file, device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE);
    BLCL_DEVICE_INFO_3SIZE_T(file, device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES);
    fprintf(stderr, "\n");
}

void blcl_init(struct blcl_target *target, cl_int device_type_id)
{
    cl_int error_id;

    target->device_type=device_type_id;
    BLCL_CHECK(clGetDeviceIDs(NULL, device_type_id, 1,  &target->device_id, NULL), "Creating device group");
    if (target->device_id==NULL) EXIT_ON_ERROR("Device not found");
    BLCL_ERROR_CHECK( target->context = clCreateContext(0, 1, &target->device_id, NULL, NULL, &error_id),   NULL, error_id,  "Creating the context");
    BLCL_ERROR_CHECK( target->command_queue = clCreateCommandQueue(target->context,  target->device_id, 0, &error_id), NULL, error_id, NULL);
}

void blcl_release(struct blcl_target *target)
{
    BLCL_CHECK(clReleaseCommandQueue(target->command_queue), NULL);
    BLCL_CHECK(clReleaseDevice(target->device_id), NULL);
    BLCL_CHECK(clReleaseContext(target->context), NULL);
  
    target->command_queue=NULL;
    target->device_id=NULL;
    target->context=NULL;
}



/*
void init_CL(char const *source_code, int width, int height)
{
    int err;                            // error code returned from api calls
    // Fill our data set with random float values
    size = height*width*2;
    
    
    // print device name
   
    
    // Create the input and output arrays in device memory for our calculation
    cl_input0 = clCreateBuffer(context,  CL_MEM_READ_ONLY,  sizeof(char) * size, NULL, NULL);
    if (cl_input0==NULL) EXIT_ON_ERROR("Fail to allocate cl_input0");
    cl_inputs[0]=cl_input0;
    
    cl_input1 = clCreateBuffer(context,  CL_MEM_READ_ONLY,  sizeof(char) * size, NULL, NULL);
    if (cl_input1==NULL) EXIT_ON_ERROR("Fail to allocate cl_input1");
    cl_inputs[1]=cl_input1;
    
    cl_output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(char) * size, NULL, NULL);
    if(cl_output==NULL)  EXIT_ON_ERROR("Fail to allocate cl_output");
    
    kernel_1=BLCL_CREATE_KERNEL_5P(context, "square", source_code, cl_output, cl_inputs[0], cl_inputs[1], size, frame_id);
    
    // Get the maximum work group size for executing the kernel on the device
    BLCL_CHECK(clGetKernelWorkGroupInfo(kernel_1, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(work_group_size), &work_group_size, NULL), "Failed to retrieve kernel work group info! %d");
}*/
/*
void execute_CL(char *output, char const *input){
    
    // Set the arguments to our compute kernel
    BLCL_CHECK(clEnqueueWriteBuffer(commands, cl_inputs[frame_id], CL_TRUE, 0, size, input, 0, NULL, NULL), "Failed copy input");
    BLCL_CHECK(clSetKernelArg(kernel_1, 4, sizeof(frame_id), &frame_id), "Failed to set kernel arguments!");
    
    // Execute the kernel over the entire range of our 1d input data set
    // using the maximum number of work group items for this device
    BLCL_CHECK(clEnqueueNDRangeKernel(commands, kernel_1, 1, NULL, &items_nb, &work_group_size, 0, NULL, NULL), "Failed to execute kernel!");
    
    // Wait for the command commands to get serviced before reading back results
    BLCL_CHECK(clFinish(commands), "finishing");
    
    // Read back the results from the device to verify the output
    BLCL_CHECK(clEnqueueReadBuffer(commands, cl_output, CL_TRUE, 0, size, output, 0, NULL, NULL ), "Failed to read output array!");
}*/


