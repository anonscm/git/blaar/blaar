#include <stdio.h>
#include "blc_channel.h"
#include <string.h>
#include <stdint.h> //uint32_t
#include <dirent.h>
#include <promihm.hh>
#include <unistd.h>
#include <sys/stat.h>
#include <limits.h>

type_experience **experiences = NULL;
int experiences_nb = 0;

int variables_nb = 0;
type_variable **variables = NULL;

static char const *global_start_filter_name = NULL; // Nobody else can touch it

/** Return 1 if the file_property is regular and start with the global_start_filter_name */
int start_filter(struct dirent const *file_property)
{
   if (file_property->d_type == DT_DIR || file_property->d_type == DT_UNKNOWN) return 0;
   return (strncmp(file_property->d_name, global_start_filter_name, strlen(global_start_filter_name)) == 0);
}

/** Call the graphical creation of the experience */
type_experience::experience(const char *name) :
      name(name), display(NULL)
{
}

/** Create a new channel. If it is included in an experiment which does not exist yet, the experiment is created.
 * Otherwise it just does a link to the existing one.
 * */
type_variable::variable(const char *full_name, size_t size, int type, char display_type) :
      blc_channel(full_name, size), type(type), display_type(display_type),  record_file(NULL), display(NULL)
{
   char const *experience_name_end, *experience_name;
   int i, name_length;
   size_t experience_name_length;

   /* The name is suppose to be <global_start_filter_name><experience_name>.<variable name> */
   experience_name_end = strchr(full_name + strlen(global_start_filter_name), '.');// On supprime l'entete ( ex "prom.")

   /* We are looking for an existing experience */
   if (experience_name_end == NULL) experience_name_length = 0;
   else experience_name_length = experience_name_end - full_name;

   FOR(i, experiences_nb)
      if ((strlen(experiences[i]->name) == experience_name_length) && strncmp(experiences[i]->name, full_name, experience_name_length) == 0) break;

   if (i != experiences_nb) experience = experiences[i]; /* We found it */
   else /* We create a new experience */
   {
      MANY_REALLOCATIONS(&experiences, experiences_nb + 1);
      experience_name = strndup(full_name, experience_name_length);
      experience = new type_experience(experience_name);
      experiences[experiences_nb++] = experience;
   }

   /*The variable name start just one dot after the experience name */
   if (experience_name_end == NULL) basename = strdup(full_name);
   else basename = strdup(experience_name_end + 1);

   name_length = strstr(basename, "-t") - basename; // It finishes by -t or end of line
   if (name_length > 0) basename[name_length] = 0; // We stop it at -t
}

void type_variable::create_display()
{
   switch (type)
   {
      case FLOAT_VARIABLE:
         ((type_float_variable*)this)->create_display();
         break;
      case BYTE_VARIABLE:
          if (display_type == IMAGE_DISPLAY) ((type_image_variable*)this)->create_display();
         break;
   }
}

/* Call the graphic creation of a float channel */
float_variable::float_variable(const char *name, size_t size, char const *params) :
      type_variable(name, size, FLOAT_VARIABLE, params[0]), values_nb(size / sizeof(float))
{}

/*
 * Detect the format of the image with the FourCC format (GREY, RGBA, ...). Interpret the parameters (width, ...)
 * Then call the graphic creation of a image */
image_variable::image_variable(const char *name, size_t size, char const *params) :
      type_variable(name, size, BYTE_VARIABLE, IMAGE_DISPLAY)
{
   if (sscanf(params, "%4s", (char*)&pixel_format) == 1)
   {
      params += 4;
      if (sscanf(params, "w%dh%d", &width, &height) == 2);
      else PRINT_WARNING("Missing width information in '%s", params);
   }
   else PRINT_WARNING("Not a valid image format : '%s'.\n\tIt should be fourCC.", params);
}

/** Check all the check memory starting by start_filter_name.
 * Due to the use of a global variable, this function is not thread safe.
 */

void update_channels(char const *start_filter_name) //Cannot be threaded (global var)
{
    FILE *blc_channels_list_file = NULL;
   char variable_type, *variable_name;
   char *type_option, *params = NULL;
   char const directory_name[] = "/dev/shm";
   char path[PATH_MAX];
    size_t variable_name_length;
   struct dirent **namelist=NULL;
    struct dirent *tmp_dirent = NULL;
   struct stat stat_data;
    size_t *sizes = NULL;
   int i, removed_variables_nb = 0;
    type_variable *variable;

   global_start_filter_name = start_filter_name;
    
    variables_nb = 0;
#ifdef __APPLE__
    char *line;
    size_t line_length;
    
    blc_channels_list_file = fopen("/tmp/blc_channels.list", "r");
    if (blc_channels_list_file == NULL) variables_nb = 0;
    else
    {
        do
        {
            tmp_dirent=ALLOCATION(struct dirent);
            MANY_REALLOCATIONS(&sizes, variables_nb+1);
            FSCANF(1, blc_channels_list_file, "%lu ", &sizes[variables_nb]);
            variable_name = fgetln(blc_channels_list_file, &variable_name_length);
        
            if (variable_name_length >= FILENAME_MAX) EXIT_ON_ERROR("The variable name length (%ld) is too long in %s", variable_name_length, variable_name);
            else if (variable_name_length==0) EXIT_ON_ERROR("Variable name is missing");
            
            tmp_dirent->d_namlen = variable_name_length-1;
            strncpy(tmp_dirent->d_name, variable_name, tmp_dirent->d_namlen);
            tmp_dirent->d_name[variable_name_length]=0;
            APPEND_ITEM(&namelist, &variables_nb, &tmp_dirent);
        }while(!feof(blc_channels_list_file));
        fclose(blc_channels_list_file);
    }
#else
   variables_nb = scandir(directory_name, &namelist, start_filter, alphasort);
   if (variables_nb < 0) EXIT_ON_SYSTEM_ERROR("scandir");
#endif

   variables = MANY_ALLOCATIONS(variables_nb, type_variable*);

   FOR(i, variables_nb)
   {
      type_option = strstr(namelist[i]->d_name, "-t");
      if (type_option != NULL)
      {
         type_option += 2;
         variable_type = type_option[0];
         params = &type_option[1];
      }
      else variable_type = 0;

      sprintf(path, "%s/%s", directory_name, namelist[i]->d_name);
       
#if __APPLE__
       stat_data.st_size = sizes[i];
#else
      stat(path, &stat_data);
      if (((stat_data.st_mode & S_IRGRP) == 0) || ((stat_data.st_mode & S_IRGRP) == 0))
      {
         if (getuid() != stat_data.st_uid)
         {
            removed_variables_nb++;
            continue;
         }
      }
#endif

      switch (variable_type)
      {
      case FLOAT_VARIABLE:
         variable = new type_float_variable(namelist[i]->d_name, stat_data.st_size, params);
         break;
      case BYTE_VARIABLE:
         if (params[0] == 'i')  variable = new type_image_variable(namelist[i]->d_name, stat_data.st_size, params+1);
         else PRINT_WARNING("Display %c for bytes not managed", params[0]);
         break;
      case INT16_VARIABLE:
        variable = new type_variable(namelist[i]->d_name, stat_data.st_size, variable_type,  params[0]);
        break;
      default:
         variable = new type_variable(namelist[i]->d_name, stat_data.st_size, variable_type, UNDEFINED_DISPLAY);
      }
      variables[i - removed_variables_nb] = variable;
   }
   FOR(i, variables_nb) free(namelist[i]);
   free(namelist);
   variables_nb -= removed_variables_nb;
   MANY_REALLOCATIONS(&variables, variables_nb); // S'il y en a eu des supprimés, on libère la mémoire en trop
}

