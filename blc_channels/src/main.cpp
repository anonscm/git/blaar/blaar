#include "blc.h"
#include <sys/fcntl.h>
#include <sys/stat.h> // mode S_... constants
#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>

blc_channel *channels=NULL;
int channels_nb=0;

/**
 * The first optional argument is the name of the experience. Otherwise all the existing shared memory are used.
 * */
int get_channel_index_by_name_or_id(char const *name)
{
   int i, id;
   char *end;

   if (name[0]=='/')
   {
      FOR(i, channels_nb) if (strcmp(channels[i].name, name)==0) return i;
   }
   else
   {
      id = strtol(name, &end, 10);
      if (end!=name && end[0]==0)
      {
         FOR(i, channels_nb) if (channels[i].id == id) return i;
      }else color_fprintf(BLC_RED, stderr, "You have to give either an id or a channel name stating by '/' instead of '%s'.\n", name);
   }
   return -1;
}

void display_content(char const *name)
{
   blc_channel *channel;
   int index;
   uint32_t type_str;


   index = get_channel_index_by_name_or_id(name);

   if (index == -1) color_fprintf(BLC_RED, stderr, "Channel '%s' does not exist\n");
   else
   {
      channel=&channels[index];
      switch(channel->type)
      {
      default: EXIT_ON_ERROR("You cannot display %.4s", UINT32_TO_STRING(type_str, channel->type));
      }
   }
}

void remove_channel(char const *name)
{
   char buffer[NAME_MAX+1];
   int index;

   index = get_channel_index_by_name_or_id(name);
   if (index == -1)
   {
      if(name[0]=='/')
      {
         do
         {
            color_printf(BLC_RED, "'%s' is not a known channel.\nTry to unlink the shared memory file anyway (y/n).\n", name);
            fgets(buffer, NAME_MAX, stdin);

            if (strcmp(buffer, "y\n")==0)
            {
               if (shm_unlink(name) == -1)
               {
                  if (errno == ENOENT) printf("The shared memory '%s' does not exist.\n", name);
                  else printf("Unlinking '%s'.\nSystem error:%s", name, strerror(errno));
               }
               printf("\nSuccess unlinking shared memory '%s'\n\n", name);
               break;
            }
         }while(strcmp(buffer, "n\n") != 0);
      }
   }
   else
   {
      channels[index].unlink();
      REMOVE_ITEM_ID(&channels, &channels_nb, index);
   }
}

void remove_semaphore(char const *name)
{

   int ret;

   ret=sem_unlink(name);

   if (ret==-1)
   {
      if (errno == ENOENT) color_printf(BLC_RED, "The semaphore '%s' does not exist/n", name);
      else EXIT_ON_SYSTEM_ERROR("Unlinking semaphore '%s'.", name);
   }
   else color_printf(BLC_GREEN, "The semaphore '%s' has been unlinked.\n", name);
}

void display_channels()
{
   char sem_status[4]="---";
   int i, width;
   blc_channel *channel;
   uint32_t type_str, format_str;

   underline_printf('-', "%6s | sem | type |format| %-32s | %-32s | %-32s ", "id", "dims", "parameter", "name");
   FOR(i, channels_nb)
   {
      //  channel = channels_addresses[i];
      channel=&channels[i];

      if (channel->sem_flags & BLC_SEM_ACK){
         if (sem_is_locked(channel->sem_ack)) sem_status[0]='A';
         else sem_status[0]='a';
      }
      if (channel->sem_flags & BLC_SEM_BLOCK){
         if (sem_is_locked(channel->sem_block)) sem_status[1]='B';
         else sem_status[1]='b';
      }
      if (channel->sem_flags & BLC_SEM_PROTECT){
         if (sem_is_locked(channel->sem_protect)) sem_status[2]='P';
         else sem_status[2]='p';
      }
      printf("%6d | %3s | %.4s | %.4s | ", channel->id, sem_status, UINT32_TO_STRING(type_str, channel->type),  UINT32_TO_STRING(format_str, channel->format));
      width=channel->fprint_dims(stdout);
      printf("%*c", 32-width, ' ');
      printf(" | %-32s | %-32s\n", channel->parameter, channel->name);
   }
   printf("\n");
}

void command_callback(char const *command, char const *argument, void *)
{
   int  cancel=0;
   char tmp_buffer[NAME_MAX];
   size_t size;
   char *end;
   blc_channel *channel;
   char tmp_sem_abp[4];
   blc_channel info;
   int fd, sem_flags, index;

   switch (*command)
   {
   case 'a':case 'b':case 'p':case 'A':case 'B':case 'P':
      index = get_channel_index_by_name_or_id(argument);
      if (index!=-1)
      {
         channel=&channels[index];
         switch (*command)
         {
         case 'a':SYSTEM_ERROR_CHECK(sem_post(channel->sem_ack), -1, "");break;
         case 'A':SYSTEM_ERROR_CHECK(sem_wait(channel->sem_ack), -1, "");break;
         case 'b':SYSTEM_ERROR_CHECK(sem_post(channel->sem_block), -1, "");break;
         case 'B':SYSTEM_ERROR_CHECK(sem_wait(channel->sem_block), -1, "");break;
         case 'p':SYSTEM_ERROR_CHECK(sem_post(channel->sem_protect), -1, "");break;
         case 'P':SYSTEM_ERROR_CHECK(sem_wait(channel->sem_protect), -1, "");break;
         }
      }
      else printf("unknown blc_channel '%s'", argument);
      break;
   case 'c':
      if (argument[0] != '/') printf("The channel name must start with '/'");
      else do
      {
         if (blc_channel_get_info_with_name(&info, argument) !=-1)
         {
            color_printf(BLC_RED, "The blc_channel '%s' already exist.",info.name);
            printf("\nDo you want to recreate it ? (y/n)");
            do {
               fgets(tmp_buffer, NAME_MAX, stdin);
               if (strcmp(tmp_buffer, "y\n")==0)
               {
                  remove_channel(argument);
                  break;
               }
               else cancel=1;
            }while(strcmp(tmp_buffer, "n\n") != 0);
         }

         fd=shm_open(argument, O_RDWR, S_IRWXU);
         if (fd != -1) {color_printf(BLC_RED, "A shared memory of name '%s' already exists.\nDo you want to remove it before creating a new blc_channel ? (y/n)", argument);
         do {
            fgets(tmp_buffer, NAME_MAX, stdin);
            if (strcmp(tmp_buffer, "y\n")==0)
            {
               shm_unlink(argument);
               break;
            }
            cancel=1;
         }while(strcmp(tmp_buffer, "n\n") != 0);
         }
         if (cancel) break;

         printf("Size of blc channel '%s' ('c' to cancel)?", argument);
         fgets(tmp_buffer, NAME_MAX, stdin);
         size = strtol(tmp_buffer, &end, 10);
         if (end != tmp_buffer)
         {
            printf("abp (ack, block, protect) ?");
            scanf("%3[apb]", tmp_sem_abp);
            sem_flags=0;
            if (strchr(tmp_sem_abp, 'a')) sem_flags|=BLC_SEM_ACK;
            if (strchr(tmp_sem_abp, 'b')) sem_flags|=BLC_SEM_BLOCK;
            if (strchr(tmp_sem_abp, 'p')) sem_flags|=BLC_SEM_PROTECT;

            channel = new blc_channel(argument, size, sem_flags);
            APPEND_ITEM(&channels, &channels_nb, channel);
            break;
         }
      }
      while(tmp_buffer[0] != 'c');
      break;
      /*          case 'd':display_content(argument);
            break;*/
   case 'l':display_channels();
   break;
   case 'q':exit(0);
   break;
   case 'u':remove_channel(argument);
   break;
   case 'U':remove_semaphore(argument);
   break;

   }
}

static int open_sems(blc_channel *channel)
{
   char sem_name[SEM_NAME_LEN+1];
   char tmp_buffer[NAME_MAX];
   int success=1;

   if (channel->sem_flags & BLC_SEM_ACK){
      sprintf(sem_name, "blc_channel_%d-ack", channel->id);
      channel->sem_ack = sem_open(sem_name,  O_RDWR);
      if (channel->sem_ack == SEM_FAILED){
         PRINT_SYSTEM_ERROR("Opening semaphore '%s' for channel '%s'.", sem_name, channel->name);
         success=0;
      }
   }

   if (channel->sem_flags & BLC_SEM_BLOCK){
      sprintf(sem_name, "blc_channel_%d-block", channel->id);
      channel->sem_block = sem_open(sem_name,  O_RDWR);
      if (channel->sem_block == SEM_FAILED) {
         PRINT_SYSTEM_ERROR("Opening semaphore '%s' for channel '%s'.", sem_name, channel->name);
         success=0;
      }
   }

   if (channel->sem_flags & BLC_SEM_PROTECT){
      sprintf(sem_name, "blc_channel_%d-protect", channel->id);
      channel->sem_protect = sem_open(sem_name,  O_RDWR);
      if (channel->sem_protect == SEM_FAILED) {
         PRINT_SYSTEM_ERROR("Opening semaphore '%s' for channel '%s'.", sem_name, channel->name);
         success=0;
      }
   }

   if (success==0)
   {
      do{
         fprintf(stderr, "Creating missing semaphores ('c') or destroying the channel ('d') ?");
         fgets(tmp_buffer, NAME_MAX, stdin);
         if (strcmp(tmp_buffer, "c\n")==0){
            channel->open_or_create_semaphores();
            success=1;
            break;
         }
         else if (strcmp(tmp_buffer, "d\n")==0)  {
            channel->destroy();
            break;
         }
      }
      while(1);
   }

   return success;
}


void main_interactive_mode(const char *start_filter_name)
{
   int i;

   blc_command_add("c", command_callback, "name", "create a new blc_channel", NULL);
   //    blc_command_add("d", command_callback, "channel", "display the content", (void*)"d");
   blc_command_add("l", command_callback, NULL, "list the blc_channels", NULL);
   blc_command_add("u", command_callback, "channel", "unlink a channel", NULL);
   blc_command_add("U", command_callback, "name", "unlink a semaphore", NULL);
   blc_command_add("q", command_callback, NULL, "quit", NULL);
   blc_command_add("a", command_callback, "channel", "send ack (sem_post)", NULL);
   blc_command_add("A", command_callback, "channel", "wait for ack (sem_wait)", NULL);
   blc_command_add("b", command_callback, "channel", "unblock the channel (sem_post)", NULL);
   blc_command_add("B", command_callback, "channel", "block a channel (sem_wait)", NULL);
   blc_command_add("p", command_callback, "channel", "unprotect a channel (sem_post)", NULL);
   blc_command_add("P", command_callback, "channel", "protect a channel (sem_wait)", NULL);

   channels_nb=blc_channel_get_all_infos_with_filter(&channels, start_filter_name);
   FOR(i, channels_nb)  if ( open_sems(&channels[i])==0) {
      REMOVE_ITEM_ID(&channels, &channels_nb, i);
      i--;
   }

   while (1)
   {
      display_channels();
      blc_command_display_help();
      blc_command_interpret();
      printf("\n");
   }
}

int main(int argc, char **argv)
{
   char const   *unlink_name, *unlink_sem_name, *force;
   char const *start_filter_name = "";
   blc_channel channel_info;

   blc_program_option_add(&unlink_name, 'u', "unlink", "blc_channel", "unlink the channel", NULL);
   blc_program_option_add(&unlink_sem_name, 'U', "unlink_sem", "sem_name", "unlink the semaphore", NULL);
   blc_program_option_add(&force, 'f', "force", NULL, "force the action", NULL);
   blc_program_add_parameter(&start_filter_name, "name_filter", 0, "filter the beginning og the blc_channel name");
   blc_program_init(&argc, &argv, NULL);

   if (unlink_name || unlink_sem_name)
   {
      if (unlink_name)
      {
         if (blc_channel_get_info_with_name(&channel_info, unlink_name) !=-1)
         {
            channel_info.unlink();
         }
         else
         {
            color_fprintf(BLC_RED, stderr, "Channel '%s' does not exist.\n", unlink_name);
            if (force)
            {
               fprintf(stderr, "We try to unlink the shared memory\n");
               if (shm_unlink(unlink_name) == -1) color_fprintf( BLC_RED, stderr, "Impossible to remove shared memory '%s'.\n%s\n", unlink_name, strerror(errno));
               else color_fprintf(BLC_GREEN, stderr,  "'%s' has been removed.");

            }
            else fprintf(stderr, "You can try to remove the shared memory anyway using the --force option.\n");
         }
      }
      if (unlink_sem_name)
      {
         remove_semaphore(unlink_sem_name);
      }
   }
   else main_interactive_mode(start_filter_name);
   return 0;
}

