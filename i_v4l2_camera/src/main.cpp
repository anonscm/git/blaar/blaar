//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include "blc.h"
#include "blv4l2.h"
#include <string.h>
#include <limits.h>

enum status {
   STOP, PAUSE, RUN
};

blc_channel output;
int status = RUN;
blv4l2_device camera;
int is_zoom=0;

void quit_callback(){

}

void blc_display_text_image(blc_narray *image, int width, int height)
{
    int component_offset, offset, step1, step2;
    uint32_t format_str;

    switch (image->format)
    {
        case 'yuv2':
            component_offset=1;
            if (is_zoom)
            {
                step1=2;
                step2=width*2;
                offset=component_offset+(int)((image->lengths[1]-width/2)/2)*2+((int)(image->lengths[2]-height)/2)*step2;
            }
            else
            {
                step1 =(int)(image->lengths[1]/(width/2))*2;
                step2=(int)(image->lengths[2]/height)*image->lengths[1]*2;
                offset=component_offset;
            }
            fprint_3Dmatrix(stderr, image, offset, 2, 1, step1, width/2, step2, height, 1);
            break;
        default: fprintf(stderr, "format '%.4s' cannot be displayed.", UINT32_TO_STRING(format_str, image->format));
    }
}

void callback(blc_mem *image_mem, void *)
{
   blc_program_loop_start();
   output.protect();

   if (image_mem->size > output.size) EXIT_ON_ERROR("The input image size  (%ubytes) is bigger than output size (%ubytes).", image_mem->size, output.size);

   memcpy(output.data, image_mem->data, image_mem->size);
   output.protect(0);

   if (blc_output_terminal==0){
      printf(".");
      fflush(stdout);
   }

   if (status == STOP) exit(0);
   blc_program_loop_end();
}

void display_information()
{

   fprintf(stderr, "image size %u: %dx%d\n", camera.format.fmt.pix.sizeimage, camera.format.fmt.pix.width, camera.format.fmt.pix.height);
   fprintf(stderr, "colorspace: ");
   switch (camera.format.fmt.pix.colorspace)
   {
   case V4L2_COLORSPACE_SMPTE170M:
      fprintf(stderr, "ITU-R 601 -- broadcast NTSC/PAL");
      break;
   case V4L2_COLORSPACE_SMPTE240M:
      fprintf(stderr, " 1125-Line (US) HDTV");
      break;
   case V4L2_COLORSPACE_REC709:
      fprintf(stderr, "HD and modern captures.");
      break;
   case V4L2_COLORSPACE_BT878:
      fprintf(stderr, "broken BT878 extents (601, luma range 16-253 instead of 16-235)");
      break;
   case V4L2_COLORSPACE_470_SYSTEM_M:
      fprintf(stderr, "470_SYSTEM_M");
      break;
   case V4L2_COLORSPACE_470_SYSTEM_BG:
      fprintf(stderr, "470_SYSTEM_BG");
      break;
   case V4L2_COLORSPACE_JPEG:
      fprintf(stderr, "JPEG (Y'CbCr)");
      break;
   case V4L2_COLORSPACE_SRGB:
      fprintf(stderr, "SRGB (RGB)");
      break;
   default:
      fprintf(stderr, "Unknown colorspace %d", camera.format.fmt.pix.colorspace);
   }
   fprintf(stderr, "\n");
   fprintf(stderr, "pixel format '%.4s'\n", (char*) &camera.format.fmt.pix.pixelformat);
}

int main(int argc, char **argv)
{
   char const *device_name;

   blc_program_set_description("Acquire webcam images on Linux computer (with video4linux2).");
   blc_program_option_add(&device_name, 'd', "device", "path", "device path name", "/dev/video0");
   blc_program_add_channel_option(&output, 'o', "output", "output channel name", "/image");
   blc_program_init(&argc, &argv, quit_callback);

   camera.init(device_name);
   output.init_image('UIN8', ntohl(camera.format.fmt.pix.pixelformat), camera.format.fmt.pix.width, camera.format.fmt.pix.height);
   output.create_or_open(output.name, BLC_SEM_PROTECT, NULL);

   printf("%s\n", output.name);
   fflush(stdout);

   blc_command_add("i", (type_blc_command_cb) display_information, NULL, "display image information", NULL);
   blc_program_loop_init();
   camera.start(callback, NULL);

   return 0;
}
