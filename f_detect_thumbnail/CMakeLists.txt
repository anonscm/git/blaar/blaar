# Set the minimum version of cmake required to build this project
cmake_minimum_required(VERSION 2.6)

# Set the name of the project as the directory basename
get_filename_component(PROJECT_NAME ${CMAKE_SOURCE_DIR} NAME)
project(${PROJECT_NAME})

add_definitions(-Wextra -Wall -pthread)

get_filename_component(blaa_dir  ${CMAKE_SOURCE_DIR} PATH)
get_filename_component(blaa_build_dir ${CMAKE_BINARY_DIR} PATH)

add_subdirectory(${blaa_dir}/blc ${blaa_build_dir}/blc_build )
add_subdirectory(${blaa_dir}/blcl ${blaa_build_dir}/blcl_build )

set(EXECUTABLE_OUTPUT_PATH  ${blaa_build_dir}/bin)
find_path(OPENCL_INCLUDE_DIR NAMES CL/cl.h OpenCL/cl.h PATHS /usr/include)	
FIND_LIBRARY(OPENCL_LIBRARY NAMES OpenCL)
 
include_directories(${blaa_dir}/blc/include ${blaa_dir}/blcl/include ${OPENCL_INCLUDE_DIR})
add_executable(${PROJECT_NAME} src/main.cpp)
target_link_libraries(${PROJECT_NAME} static_blcl static_blc ${OPENCL_LIBRARY})
if(UNIX AND NOT APPLE)
target_link_libraries(${PROJECT_NAME} rt pthread)
endif()



