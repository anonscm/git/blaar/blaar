#include <fcntl.h> // O_RDONLY ...
#include <stdio.h>
#include <gtk/gtk.h>
#include <string.h>
#include <stdint.h> //uint32_t
#include <jpeglib.h>
#include "blc.h"

#include "content_display.hpp"


//#include "record.hh"

// Pour les view metre / text boxes */

const float value_max = 1;
const float value_min = 0;



GtkWidget **channels_display;

void getting_range(GtkRange *range, float *value)
{
  *value = gtk_range_get_value(range);
}

void getting_spin_value(GtkSpinButton *button, float *value)
{
  *value = gtk_spin_button_get_value(button);
}

void getting_check_value(GtkToggleButton *button, float *value)
{
  if (gtk_toggle_button_get_active(button)) *value = 1.0;
  else *value = 0.0;
}

gboolean update_range(GtkRange *range, GdkFrameClock *frame_clock, float *value)
{
  (void) frame_clock;

  gtk_range_set_value(range, *value);
  return G_SOURCE_CONTINUE;
}

gboolean update_spin_value(GtkSpinButton *button, GdkFrameClock *frame_clock, float *value)
{
  (void) frame_clock;

  gtk_spin_button_set_value(button, *value);

  return G_SOURCE_CONTINUE;
}

gboolean update_check_value(GtkToggleButton *button, GdkFrameClock *frame_clock, float *value)
{
  (void) frame_clock;
  gtk_toggle_button_set_active(button, (*value > 0.5));
  return G_SOURCE_CONTINUE;
}

/*
void type_experience::create_display()
{
  GtkWidget *grid;
  GtkWidget *scrolled_window;

  display = gtk_frame_new(name);
  scrolled_window = gtk_scrolled_window_new(NULL, NULL);

  grid = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  gtk_container_add(GTK_CONTAINER(display), scrolled_window);
  gtk_container_add(GTK_CONTAINER(scrolled_window), grid);
  gtk_widget_set_vexpand(scrolled_window, TRUE);
  gtk_widget_set_hexpand(scrolled_window, TRUE);
}*/
 

/// Create a widget displaying the controls accordingly to the type of control
GtkWidget *create_tensor_display(blc_channel *channel)
{
  char label_text[NAME_MAX + 1];
  float *values = (float*) channel->data;
  GtkWidget *widget = NULL, *grid;
  GtkWidget *display;
  int i;

  display = gtk_frame_new(channel->name);
  grid = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_container_add(GTK_CONTAINER(display), grid);

    /*
  if (channel->type != )
  {
    FOR(i, values_nb)
    {
      switch (display_type)
      {
      case TEXT_DISPLAY:
        widget = gtk_spin_button_new_with_range(value_min, value_max, 0.001);
        gtk_widget_add_tick_callback(widget, (GtkTickCallback) update_spin_value, &values[i], NULL);
        g_signal_connect(G_OBJECT(widget), "value-changed", G_CALLBACK(getting_spin_value), &values[i]);
        break;
      case VIEW_METER_DISPLAY:
        widget = gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, value_min, value_max, 0.001);
        gtk_range_set_inverted(GTK_RANGE(widget), TRUE);
        gtk_widget_add_tick_callback(widget, (GtkTickCallback) update_range, &values[i], NULL);
        g_signal_connect(G_OBJECT(widget), "value-changed", G_CALLBACK(getting_range), &values[i]);
        gtk_widget_set_size_request(widget, 40, 128);
        break;
      case CHECKBOX_DISPLAY:
        widget = gtk_check_button_new();
        gtk_widget_add_tick_callback(widget, (GtkTickCallback) update_check_value, &values[i], NULL);
        g_signal_connect(G_OBJECT(widget), "toggled", G_CALLBACK(getting_check_value), &values[i]);
        break;
        break;
      }
      gtk_widget_set_vexpand(widget, TRUE);
      gtk_widget_set_hexpand(widget, TRUE);
      gtk_container_add(GTK_CONTAINER(grid), widget);
    }
  }
  else
  {
    snprintf(label_text, NAME_MAX, "Unknown display for %d float values.", values_nb);
    widget = gtk_label_new(label_text);
    gtk_container_add(GTK_CONTAINER(grid), widget);
  }
     */
}

/*
/// Create a widget displaying an image accordingly to the type of the image
void image_variable::create_display()
{
  GdkPixbuf *pixbuf;
  GtkWidget *widget = NULL;
  char *image_buffer;
  char label_text[NAME_MAX + 1];

  sprintf(label_text, "%s %.4s", name, (char*) &pixel_format);
  display = gtk_frame_new(label_text);

  if (pixel_format == TO_UINT32("GREY") )
  {
    image_buffer = MANY_ALLOCATIONS(size * 3, char); //In order to be sure that the rowstride will be 'gui.width * 3'
    pixbuf = gdk_pixbuf_new_from_data((uchar*) image_buffer, GDK_COLORSPACE_RGB, 0, 8, width, height, width * 3, NULL, NULL);
    widget = gtk_image_new_from_pixbuf(pixbuf);
    gtk_widget_add_tick_callback(widget, (GtkTickCallback) update_GREY_image, this, NULL);
  }
  else if (pixel_format == TO_UINT32("RGB3") )
  {
    pixbuf = gdk_pixbuf_new_from_data((uchar*) data, GDK_COLORSPACE_RGB, 0, 8, width, height, width * 3, NULL, NULL);
    widget = gtk_image_new_from_pixbuf(pixbuf);
    gtk_widget_add_tick_callback(widget, (GtkTickCallback) update_RGB3_image, pixbuf, NULL);
  }
  else if (pixel_format == TO_UINT32("JPEG") )
  {
    image_buffer = MANY_ALLOCATIONS(size * 3, char); //In order to be sure that the rowstride will be 'gui.width * 3'
    pixbuf = gdk_pixbuf_new_from_data((uchar*) image_buffer, GDK_COLORSPACE_RGB, 0, 8, width, height, width * 3, NULL, NULL);
    widget = gtk_image_new_from_pixbuf(pixbuf);
    gtk_widget_add_tick_callback(widget, (GtkTickCallback) update_JPEG_image, this, NULL);
  }
  else if ((pixel_format == TO_UINT32("YUYV") )|| (pixel_format == TO_UINT32("YUV2"))){
  image_buffer = MANY_ALLOCATIONS(size * 3, char); //In order to be sure that the rowstride will be 'gui.width * 3'
  pixbuf = gdk_pixbuf_new_from_data((uchar*)image_buffer, GDK_COLORSPACE_RGB, 0, 8, width, height, width * 3, NULL, NULL);
  widget = gtk_image_new_from_pixbuf(pixbuf);

  pthread_create(&init_table_thread, NULL, create_RGB3_from_YUYV, widget);
  gtk_widget_add_tick_callback(GTK_WIDGET(widget), (GtkTickCallback) update_YUYV_image, this, NULL);

}
else widget = gtk_label_new("Unknown format image");

  gtk_widget_set_size_request(GTK_WIDGET(display), 32, 32);
  gtk_container_add(GTK_CONTAINER(display), widget);
}
*/

GtkWidget *create_image_display(blc_channel *channel)
{
    GdkPixbuf *pixbuf;
    GtkWidget *widget = NULL;
    uchar *image_buffer;
    char label_text[NAME_MAX + 1];
    GtkWidget *display;
    blc_image_info image(channel->parameter);
    
    sprintf(label_text, "%s %.4s", channel->name, UINT32_TO_STRING(image.format));
    display = gtk_frame_new(label_text);
        
    if (image.format == STRING_TO_UINT32("Y800") )
    {
            image_buffer = MANY_ALLOCATIONS(image.pixels_nb * 3, uchar); //In order to be sure that the rowstride will be 'gui.width * 3'
            pixbuf = gdk_pixbuf_new_from_data( image_buffer, GDK_COLORSPACE_RGB, 0, 8, image.width, image.height, image.width * 3, NULL, NULL);
            widget = gtk_image_new_from_pixbuf(pixbuf);
            gtk_widget_add_tick_callback(widget, (GtkTickCallback) update_GREY_image, channel, NULL);
        }
        else if (image.format == STRING_TO_UINT32("RGB3") )
        {
            pixbuf = gdk_pixbuf_new_from_data((uchar*)channel->data, GDK_COLORSPACE_RGB, 0, 8, image.width, image.height, image.width * 3, NULL, NULL);
            widget = gtk_image_new_from_pixbuf(pixbuf);
            gtk_widget_add_tick_callback(widget, (GtkTickCallback) update_RGB3_image, pixbuf, NULL);
        }
        else if (image.format == STRING_TO_UINT32("JPEG") )
        {
            image_buffer = MANY_ALLOCATIONS(image.pixels_nb * 3, uchar); //In order to be sure that the rowstride will be 'gui.width * 3'
            pixbuf = gdk_pixbuf_new_from_data( image_buffer, GDK_COLORSPACE_RGB, 0, 8, image.width, image.height, image.width * 3, NULL, NULL);
            widget = gtk_image_new_from_pixbuf(pixbuf);
            gtk_widget_add_tick_callback(widget, (GtkTickCallback) update_JPEG_image, channel, NULL);
        }
        else if ((image.format == STRING_TO_UINT32("YUYV"))|| (image.format == STRING_TO_UINT32("yuv2"))){
            image_buffer = MANY_ALLOCATIONS(image.pixels_nb * 3, uchar); //In order to be sure that the rowstride will be 'gui.width * 3'
            pixbuf = gdk_pixbuf_new_from_data(image_buffer, GDK_COLORSPACE_RGB, 0, 8, image.width, image.height, image.width * 3, NULL, NULL);
            widget = gtk_image_new_from_pixbuf(pixbuf);
            
            pthread_create(&init_table_thread, NULL, create_RGB3_from_YUYV, widget);
            gtk_widget_add_tick_callback(GTK_WIDGET(widget), (GtkTickCallback) update_YUYV_image, channel, NULL);
            
        }
        else widget = gtk_label_new("Unknown format image");
        
        gtk_widget_set_size_request(GTK_WIDGET(display), 32, 32);
        gtk_container_add(GTK_CONTAINER(display), widget);
    return display;
}


/*
 channels_display = MANY_ALLOCATIONS(channels_nb, GtkWidget*);
 FOR(i, channels_nb)
 {
 if (channels[i]->type == STRING_TO_UINT32("UIN8"))  channels_display[i] = create_image_display(channels[i]);
 else if (channels[i]->type == STRING_TO_UINT32("FL32"))  channels_display[i] = create_image_display(channels[i]);
 
 else channels_display[i] = gtk_label_new("Unknonwn channel type");
 gtk_container_add(GTK_CONTAINER(grid), GTK_WIDGET(channels_display[i]));
 }
 
 pthread_join(init_table_thread, NULL);*/



