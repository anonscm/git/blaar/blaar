//
//  Created by Arnaud Blanchard on 06/11/14.
//Copyright ETIS 2014. All rights reserved.
//
#include <OpenCL/opencl.h>
#include "blcl.h"
#include "silent.hpp"
#include "main.hpp"
#include "blQTKit.h"

#include "blc.h"
#include <sys/signal.h>
#include <stdlib.h>

/**
 @main
 Exemple of using the console to display the camera
 */

static int initialised = 0;
static int width, height = 0;
static uint32_t input_format;
static struct timeval global_time;
size_t image_size=0;

char const *Y0_code="y0";
char const *U_code="u";
char const *Y1_code="y1";
char const *V_code="v";
char *source_code=NULL;

int refresh_filter=1;
cl_command_queue commands;
cl_context context;
cl_kernel filter_kernel=NULL;
cl_mem cl_input, cl_output;
cl_device_id device_id;


float filter[16];
char *activation=(char*)"convert_uchar_sat(p)";

// Simple compute kernel which computes the square of an input array
const char *start_source_code = "kernel void square(global unsigned char* output,  global unsigned char *input0, global unsigned char *input1, unsigned int count, int index){\n" \
"   unsigned char y0, u, y1, v;\n"\
"   unsigned char Y0, U, Y1, V;\n"\
"   unsigned char y01, u1, y11, v1;\n"\
"   local uchar tr[512]; \n"\
"   unsigned char tmp0, tmp1, tmp2, tmp3;\n"\
"   unsigned int i = get_global_id(0)*4;\n"\
"   unsigned int j = get_local_id(0);\n"\
"   global unsigned char *input;"\
"   global unsigned char  *past;"\
"   if (index) { input=input1;past=input0;}"\
"   else {input=input0;past=input1;}"\
"   if(i < count){\n"\
"      y0=input[i+1]; u=input[i]; y1=input[i+3]; v=input[i+2];\n"\
"      y01=past[i+1]; u1=past[i]; y11=past[i+3]; v1=past[i+2];\n"\

"   tr[j] = y0;\n"\
"   tr[j+1] = y1;\n";

char const* end_source_code="output[i+1] = Y0; output[i] = U; output[i+3] = Y1; output[i+2] = V;\n" \
"   }\n" \
"}\n";


char const *update_source_code()
{
    free(source_code);
    switch (output_format)
    {
        case 'yuv2':
            break;
    }
    asprintf(&source_code, "%s\nY0=%s;\nU=%s;\nY1=%s;\nV=%s;\n%s\n", start_source_code, Y0_code, U_code, Y1_code, V_code, end_source_code);
    return source_code;
}

static void command_callback(char const *command, char const *, void *)
{
    uint32_t input_format_string, output_format_string;
    
    switch (*command)
    {
        case   0:if (status==RUN){
            status=PAUSE;
            fprintf(stderr, "%d iterations, average period: %.3lf ms\n", iteration, period/1000);
            iteration=0;
            blc_command_display_help();
            fprintf(stderr, "--- pause ---\n");
            fflush(stderr);
            blc_command_interpret();
        } else {
            status=RUN;
            fprintf(stderr, "--- continue ---\n");
        }
            
            break;
        case 'i':
            input_format_string=htonl(input_format);
            output_format_string=htonl(output_format);
            fprintf(stderr, "input:%.4s output:%.4s\n", (char*)&input_format_string, (char*)&output_format_string);
            printf("i");
            fflush(stdout);break;
        case 'q': status=STOP;break;
    }
}

static void edit_callback(char const *, char const *argument, void *user_data)
{
    struct timeval time={0, 0};
    if (strlen(argument)==0)  fprintf(stderr, "=%s\n", *((char**)user_data));
    else {
        //    free(user_data);
        *((char**)user_data)=strdup(argument);
        us_time_diff(&time);
        //  BLCL_CREATE_KERNEL_4P("square", update_source_code(), cl_output, cl_inputs, size, frame);
        fprintf(stderr, "Recompilation time %ld µs\n", us_time_diff(&time));
    }
}

static void quit_silent_mode()
{
    printf("q");
    fflush(stdout);
    //   if (channel) channel->unlink();
    fprintf(stderr, "\nQuitting QTKit_camera.\n");
}


void silent_mode_callback(CVImageBufferRef videoFrame, void *user_data)
{
    blc_mem mem;
    int bytes_per_pixel;
    static size_t items_nbs[2];
    static size_t work_group_sizes[2];
    
    (void)user_data;
    
    if (!initialised)
    {
        input_format = ntohl(CVPixelBufferGetPixelFormatType(videoFrame));
        if (output_format==0) output_format = input_format;
        
        width= CVPixelBufferGetWidth(videoFrame);
        height=  CVPixelBufferGetHeight(videoFrame);
        bytes_per_pixel=blc_get_bytes_per_pixel(output_format);
        input_image.size=blc_get_bytes_per_pixel(input_format)*width*height;
        if (bytes_per_pixel==1) channel = create_or_open_blc_channel(channel_name, "p", 'UIN8', output_format, NULL, 2, width-4, height-4);
        else  channel = create_or_open_blc_channel(channel_name, "p", 'UIN8', output_format, NULL, 3, bytes_per_pixel, width, height);
        if (synchronous_mode) printf("%s\n", channel->name);
        fflush(stdout);
        initialised = 1;
        
        switch (output_format)
        {
            case 'yuv2':
                blc_command_add("Y0=", edit_callback, "expression", "function of y0, u, y1, v", (void*)&Y0_code);
                blc_command_add("U=", edit_callback, "expression", "function of y0, u, y1, v", (void*)&U_code);
                blc_command_add("Y1=", edit_callback, "expression", "function of y0, u, y1, v", (void*)&Y1_code);
                blc_command_add("V=", edit_callback, "expression", "function of y0, u, y1, v", (void*)&V_code);
                break;
            case 'Y800':
                blc_command_add("Y=", edit_callback, "expression", "function of y0, u, y1, v", (void*)&Y0_code);
                break;
            default:EXIT_ON_ERROR("output format not managed '%.4s' ", &output_format );
        }
        CLEAR(global_time);
        fprintf(stderr, "--- start ---\n");
        fflush(stderr);
        items_nbs[0]=width-4;
        items_nbs[1]=height-4;
        work_group_sizes[0]=1;
        work_group_sizes[1]=1;
    }
    
    if (refresh_filter) {
        update_source_code_filtering(activation, filter, 1, 1);
        refresh_filter=0;
    }
    
    CVPixelBufferLockBaseAddress(videoFrame, 0);
    input_image.data = (char*)CVPixelBufferGetBaseAddress(videoFrame);
    us_time_diff(&global_time);
    
    BLCL_CHECK(clEnqueueWriteBuffer(commands, cl_input, CL_TRUE, 0, sizeof(char)*width*2*height, input_image.data, 0, NULL, NULL ), "Failed to write input array!");
    CVPixelBufferUnlockBaseAddress(videoFrame, 0);
    BLCL_CHECK(clEnqueueNDRangeKernel(commands, filter_kernel, 2, NULL, items_nbs, work_group_sizes, 0, NULL, NULL), "Failed to execute kernel! items_nb '%lu', work_group_size '%lu'", items_nbs[0], work_group_sizes[0]);
    BLCL_CHECK(clFinish(commands), "finishing");
    channel->protect();
    BLCL_CHECK(clEnqueueReadBuffer(commands, cl_output, CL_TRUE, 0, sizeof(char)*(width-4)*(height-4), channel->data, 0, NULL, NULL ), "Failed to read output array!");
    channel->protect(0);
    iteration++;
    period+=(us_time_diff(&global_time)-period)/iteration;
    blc_command_try_to_interpret();
    if (synchronous_mode)  printf(".");
    SYSTEM_SUCCESS_CHECK(fflush(stdout), 0, "flushing stdout");
    if (status == STOP) exit(0);
}

void filter_command_cb(char const *, char const *argument, void *user_data)
{
    cl_int err;
    char *source_code=NULL;
    int i,j;
    cl_mem cl_filter;
    cl_kernel kernel;
    size_t items_nb=16, work_group_size=16;
    (void)user_data;
    
    cl_filter = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float) * 16, NULL, &err);
    if (err!=CL_SUCCESS) EXIT_ON_ERROR("cl_error: %d", err);
    
    asprintf(&source_code,"kernel void create_filter(global float *filter){int i; float x; float y; i=get_global_id(0);x=i%%4-1.5f; y=i/4-1.5f; filter[i]=%s;}", argument);
    kernel=BLCL_CREATE_KERNEL_1P(context, "create_filter", source_code, cl_filter);
    FREE(source_code);
    BLCL_CHECK(clEnqueueNDRangeKernel(commands, kernel, 1, NULL, &items_nb, &work_group_size, 0, NULL, NULL), "Failed to execute kernel! items_nb '%lu', work_group_size '%lu'", items_nb, work_group_size);
    BLCL_CHECK(clFinish(commands), "finishing");
    BLCL_CHECK(clEnqueueReadBuffer(commands, cl_filter, CL_TRUE, 0, 16*sizeof(float), filter, 0, NULL, NULL ), "Failed to read output array!");
    
    clReleaseKernel(kernel);
    refresh_filter=1;
    
    FOR(j,4)
    {
        FOR(i,4)
        {
            fprintf(stderr, "%.3f ", filter[i+j*4]);
        }
        fprintf(stderr, "\n");
    }
    
}

void update_activation_cb(char const *, char const *argument, void *)
{
    if (strlen(argument)==0) fprintf(stderr, "%s\n", activation);
    else {
        activation=(char*)argument;
        refresh_filter=1;
    }
}

void start_silent_mode()
{
    cl_int error_id;
    if (output_format==0 ) output_format=input_format;
    
    blc_command_add("", command_callback, NULL, "pause", NULL);
    blc_command_add("filter=", filter_command_cb, "expression", "change the filter", NULL);
    blc_command_add("activation=", update_activation_cb, "expression", "change the activation function", NULL);
    blc_command_add("i", command_callback, NULL, "info", NULL);
    blc_command_add("q", command_callback, NULL, "quit", NULL);
    
    atexit(quit_silent_mode);
    
    BLCL_CHECK(clGetDeviceIDs(NULL, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL), "Creating device group");
    BLCL_ERROR_CHECK(context = clCreateContext(0, 1, &device_id, NULL, NULL, &error_id), NULL,  error_id, "Initialisation");
    // Create a command commands
    BLCL_ERROR_CHECK(commands = clCreateCommandQueue(context, device_id, 0, &error_id), NULL, error_id, "Initialisation");
    
    init_capture(silent_mode_callback, NULL);
}


