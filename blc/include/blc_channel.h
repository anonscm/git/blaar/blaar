/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)

 Author: Aranud Blanchard

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

/**
 *
 *  @date Oct 10, 2014
 *  @author Arnaud Blanchard
 @defgroup blc_channel channels
 @{
 */

#ifndef BLC_CHANNEL_H
#define BLC_CHANNEL_H

#include "blc_network.h"
#include "blc_tools.h"
#include "blc_mem.h"
#include <netinet/in.h>
#include <semaphore.h>

#define SEM_NAME_LEN 31 // At least on OSX
#define BLC_PARAMETER_MAX 31
#define BLC_DIMS_MAX 8
#define BLC_SEM_NONE 0
#define BLC_SEM_ACK (1<<0)
#define BLC_SEM_ACK_LOCKED (1<<1)
#define BLC_SEM_BLOCK (1<<2)
#define BLC_SEM_BLOCK_LOCKED (1<<3)
#define BLC_SEM_PROTECT (1<<4)
#define BLC_SEM_PROTECT_LOCKED (1<<5)


typedef struct blc_channel
#ifdef __cplusplus
:blc_narray {
    blc_channel(int dims_nb=0);
    
    int fscan_info(FILE *file);
    void fprint_info(FILE *file);

	///Create or open a channel in shared memory.
    blc_channel(char const *name);
    ///Channel with only one dimension
    blc_channel(char const *name, int length, int sem_flags, uint32_t type='NDEF', uint32_t format='NDEF', char const *parameter=NULL);

    blc_channel(char const *name, int sem_flags, uint32_t type, uint32_t format, char const *new_parameter, int dims_nb, int length0, ...); // Protected, Block, Ack
  //  blc_channel(char const *name, int sem_flags, char const *type_string, char const *format_string, char const *new_parameter, int dims_nb, int length0, ...); // Protected, Block, Ack

    blc_channel(char const *name, int sem_flags, uint32_t type, uint32_t format, char const *parameter, int dims_nb, int length0, va_list arguments);
    blc_channel(char const *name, int sem_flags, char const *type_string, char const *format_string, char const *parameter, int dims_nb, int length0, va_list arguments);

    blc_channel(char const *name, size_t size, int sem_flags, const char *hostname);
    ///Remove the channel. This free the memory of this process and close the file localy. It does not remove the shared memory file on the system.
 //  ~blc_channel();
  
    void open_or_create_semaphores();
    void vcreate(int sem_flags, uint32_t type, uint32_t format, char const *parameter, int dims_nb, int length, va_list arguments);
    void vcreate_or_open(int sem_abp, uint32_t type, uint32_t format,  char const *parameter, int dims_nb, int length0, va_list arguments);
    void create(int sem_abp, uint32_t type, uint32_t format,  char const *parameter, int dims_nb, int length0, ...);
    void create(char const *name, int sem_flags, char const *parameter);
    void open(const char *name=NULL);
    void create_or_open(char const *name, int sem_flags, char const *parameter);
    void create_or_open(int sem_abp, uint32_t type, uint32_t format,  char const *parameter, int dims_nb, int length0, ...);
  //  void open_or_create(char const *name);
    
    void protect(int value=1);
    void set_namef(char const *name, ...);
    void send();
    void unlink(); //Remove the memory from the system unless another process retain it.
    
#else
{
    blc_narray narray;// Not beautiful but makes it easy to convert C++ heritage of "class"  in C struct inclusion.
#endif
    int sem_flags;
    char name[NAME_MAX+1];
    char parameter[BLC_PARAMETER_MAX+1];
    int id;
    int fd; ///< shared memory file descriptor
   int socket_fd; ///< network (UDP) socket file descriptor
   sem_t  *sem_ack,  *sem_block, *sem_protect; ///<semaphores for respectively acknowledge data, waiting data, protecting data.
   uint32_t index;
   struct sockaddr_in target;
}blc_channel;

extern int blc_channel_port;

void start_channel_server(int port);
    
START_EXTERN_C
/*Create a channel with the name, the kind of semaphore "abp" a:ack, b:block, p:protect, the type of data UIN8, INT8, UI16, IN16, FL32, FL64. ( default 0:UIN8),
The format are four chars user defined but usually describe the image format 'RGB3', 'YUV2' ,..., parameter is an user defined text.
Dims_nb is the number of dimenssions of the data ( 1 for a vector). 
The following value are the the length of each dims. You must have the same number of length than dim.
*/
blc_channel *new_blc_channel_create(const char *name, char const *new_sem_abp, uint32_t type, uint32_t format, char const *parameter, int dims_nb, int length0, ...);

/*Open an exisiting channel 'name' */
blc_channel *new_blc_channel_open(const char *name);
    
    
void blc_channel_create_or_open(blc_channel *channel, char const *name, int sem_flags, char const *parameter);

/* Create or reopen a channel, in the latter case the exisiting channel must have the same characteristics */
blc_channel *new_blc_channel_create_or_open(const char *name, int sem_flags, uint32_t type, uint32_t format, char const *parameter, int dims_nb, int length0, ...);

/* Free the local memory of the channel but not the reference (another process can still open it). Otherwise use unlink.*/
void destroy_blc_channel(blc_channel *channel);

/** Unlink the shared memory and destroy the channel. No new process can use it.*/
int blc_channel_unlink( blc_channel * channel);
    
/** Allocate and give the array of existing shared memory files on the system.*/
int blc_channel_get_all_infos(blc_channel **channels_infos);
    
int blc_channel_get_all_infos_with_filter(blc_channel **channels_infos, char const *start_name_filter);

///Retrieve the informations about a channel if it is known and return the id. Otherwise, return -1
int blc_channel_get_info_with_name(blc_channel *info, char const *name);
    
/** Remove the channels
 * \param channels Address of a pointer to an array containing a list of channel. This array may have been created by blc_channel_get_availables.
 * \param channels_nb number of channels in the array.*/
void destroy_channels(blc_channel ***channels, int channels_nb);
    
void blc_channel_protect(blc_channel *channel, int value);

END_EXTERN_C
///@}
    
#endif
