/*
 Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)

 Author: Arnaud Blanchard

 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.*/

/**
 @defgroup blc_text Text_tools
 @{
 @brief Provide tools to manipulate text on files or on the terminal (color, formating, ...).
 It alsomanage arguments of programs or interactive command line input.
 Consider using ncurses ( https://www.gnu.org/software/ncurses ) for more interactive tools with the terminal.
*/

#ifndef BLC_TEXT_H
#define BLC_TEXT_H

#include <stdio.h>
#include <stdarg.h>
#include "blc_tools.h"
#include "blc_mem.h"

#define MOVE_N_UP_CURSOR "%dA"
#define MOVE_BEGIN_PREVIOUS_N_LINES "%dF"
#define MOVE_N_DOWN_CURSOR "%dB"
#define DEL_END_SCREEN "0J"
#define DEL_BEGIN_SCREEN "1J"
#define DEL_END_LINE "0K"
#define DEL_BEGIN_LINE "1K"
#define DEL_ALL_LINE "2K"
///Number of gradual colors we can use.
#define BLC_BAR_COLORS_NB 12
#define BLC_BRIGHT 0x8

///Code to be used in text coloring function (color_id).
enum BLC_COLORS{
  BLC_BLACK = 0,
  BLC_RED = 1,
  BLC_GREEN = 2,
  BLC_YELLOW = 3,
  BLC_BLUE = 4,
  BLC_MAGENTA = 5,
  BLC_CYAN = 6,
  BLC_WHITE = 7,
  BLC_GREY = 8,
  BLC_BRIGHT_RED = 9,
  BLC_BRIGHT_GREEN = 10,
  BLC_BRIGHT_YELLOW = 11,
  BLC_BRIGHT_BLUE = 12,
  BLC_BRIGHT_MAGENTA = 13,
  BLC_BRIGHT_CYAN = 14,
  BLC_BRIGHT_WHITE = 15,
  BLC_COLORS_NB
};

///Start a new color for the standart output.
#define  print_start_color(color_id) fprint_start_color(stdout, color_id)
///Reset the default color for the standart output.
#define print_stop_color() fprint_stop_color(stdout)

#define color_printf(color_id, ...) color_fprintf(color_id, stdout, __VA_ARGS__)
///apply on stdout
#define underline_printf(char_pattern, ...) underline_fprintf( char_pattern, stdout, __VA_ARGS__)
///apply on stdout
#define print_human_size(size) fprint_human_size(stdout, size)
///apply on stdout
#define print_tsv_floats(values, values_nb) fprint_tsv_floats(stdout, values, values_nb);
///apply on stdout
#define scan_tsv_floats(values, values_nb) fscan_tsv_floats(stdin, values, values_nb);


///Variable setting wether we can use ANSI escape codes. This value is true by default, use terminal_ansi_detect() to set its value.
extern int blc_stdout_ansi;
///Variable setting wether we can use ANSI escape codes. This value is true by default, use terminal_ansi_detect() to set its value.
extern int blc_stderr_ansi;
///Contain the color_id going gradually from dark_blue to dark_red corresponding to @ref BLC_BAR_COLORS_NB scales.
extern uchar blc_bar_colors[BLC_BAR_COLORS_NB];

START_EXTERN_C
///Try to get the position of the cursor on the terminal.
int blc_fterminal_try_to_get_cursor_position(FILE *file, int *x, int *y);
///Test wether the terminal is ANSI. color etc ..
int blc_stdout_ansi_detect();
///
int blc_stderr_ansi_detect();
///Get the size of the terminal windows in characters.
void blc_terminal_get_size(int *columns, int *lines);
///Set a callback to be called each time the terminal window is resized.
void blc_terminal_set_resize_callback(void (*callback)(int columns_nb, int rows_nb));

///Send an escape command on the terminal.
void fprint_escape_command(FILE *file, char const *format, ...);
///Set a new color for the terminal.
void fprint_start_color(FILE *file, int color_id);
///Reset the default color for the terminal
void fprint_stop_color(FILE *file);
///Change the backgroungd color.
void fprint_backgournd_start_color(FILE *file, int color_id);
///Set back the background color to default.
void fprint_backgournd_stop_color(FILE *file);
///Print a text in color.
void color_fprintf(int color_id, FILE *file, char const *format, ...);
///Put a color text in a char string
int color_sprintf(int color_id, char *string, const char *format, ...);
///Print a text in color.
void color_vfprintf(int color_id, FILE *file, char const *format, va_list arguments);
///Put a color text in a char string
int color_vsprintf(int color_id, char *string, char const *format, va_list arguments);
///Underline with the pattern char the text given in parameter.
void underline_fprintf(char pattern, FILE * file, char const *format, ...);
///Print to the file a human readable format of the size ( Kb, Mb, Gb, ...).
void fprint_human_size(FILE *file, size_t size);
///Print in tab separated values the array of values.
void fprint_tsv_floats(FILE *file, float *values, int values_nb);
///Read tab separated values in the files and set values with the result.
void fscan_tsv_floats(FILE *file, float *values, int values_nb);

int fprint_fourcc(FILE *file, uint32_t value);
char* sprint_fourcc(char *string, uint32_t value);
int blc_sscan_dims(int **lengths, char const *str);

END_EXTERN_C
///@}
#endif
