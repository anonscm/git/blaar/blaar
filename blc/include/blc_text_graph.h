/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)

 Author: A. Blanchard

 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */



/**
@defgroup blc_text_graph Text graph tools
@{
Provide tools to display textual graphs on files or on the teminal.
*/
#ifndef BLC_TEXT_GRAPH_H
#define BLC_TEXT_GRAPH_H

#include <stdio.h>
//#include <stdarg.h>
#include "blc_tools.h"
#include "blc_mem.h"

/*
#ifdef __cplusplus
#define printf_graph(mem, title, height, max, min) fprint_graphs(stdout, mem, title, 1, height, max, min)
void fprint_graphs(FILE *file, blc_mem const *mems,  char const* const* title,  int graphs_nb = 1, int height = 16, int max = 256, int min = 0, char const *abscissa_name = "x", char const *ordinate_name = "y");
void fprint_matrix_graph(FILE *file, blc_mem *mem, int dim_size = 0, char const *title = "untitled");
#else
*/
#define print_graph(mem, title, height, max, min, abscissa_name, ordonate_name) fprint_graphs(stdout, mem, &(title), 1, height, max, min, abscissa_name, ordonate_name)
START_EXTERN_C
void fprint_graphs(FILE *file,  blc_mem const *mems, char const* const* title, int graphs_nb, int height, int max, int min,  char const* abscissa_name,  char const* ordonate_name );
void fprint_matrix_graph(FILE *file, blc_mem *mem, int step, char const *title, int width, int height);
void fprint_matrix_graph_decimal(FILE *file, blc_mem *mem, int step, char const *title, int width, int height, int ansi_terminal);
void fprint_matrix_graph_hex(FILE *file, blc_mem *mem, int step, char const *title, int width, int height);
void fprint_matrix3D_graph(FILE *file, blc_mem *mem, int step, int length, int offset, int width, int height);
void fprint_3Dmatrix(FILE *file, blc_mem *mem, int offset, int step0, int length0, int step1, int length1, int step2,  int length2, int ansi_terminal);
END_EXTERN_C
//#endif
///@}
#endif
