/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)
 
 Author: Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

/*
 The usage formating follows the format of argparse in Python ( https://docs.python.org/3/library/argparse.html ).
Otherwise, we can be inspired from http://docopt.org
 */

/**
@defgroup blc_text_app text app
Few functions helping for pseudo realtime applications.
@{*/

#ifndef BLC_TEXTAPP_H
#define BLC_TEXTAPP_H

#include "blc_tools.h"
#include "blc_channel.h"

#define BLC_PROGRAM_FOR_LOOP() for(blc_program_loop_init(); blc_program_loop_start;blc_program_loop_end)


typedef  void (*type_blc_command_cb)(char const *command, char const *arguement, void *user_data);

typedef struct blc_optional_argument
{
    char const *shortname;
    char const *longname;
    char const *help;
    char const *value;
    void  *display;
}blc_optional_argument;

typedef struct blc_positional_argument
{
    char const *name;
    char const *help;
    char const *value;
}blc_positional_argument;


START_EXTERN_C
char *blc_get_help(char const *path);
char const *blc_parse_help(char const *path, blc_optional_argument **optional_arguments, int *optional_arguments_nb, blc_positional_argument **positional_arguments, int *positional_arguments_nb, char **epilog);
int blc_parse_optional_arguments(struct blc_optional_argument **results, char const **pos, char const *optional_arguments_description);
int blc_parse_positional_arguments(struct blc_positional_argument **results, char const **pos, char const *positional_arguments_description);


///Add a command in the list of possible command lines from the user.
void blc_command_add(const char *command_name, void (*callback)(char const* command, char const *argument, void *user_data), const char *prompt, const char *help, void *user_data);
///Wait a input from the user (this id blocking) and interprets it depending on the blc_command list.
void blc_command_interpret();
///Like blc_command_interpret but does not block if there is nothing to read (return 0 in this case).
int blc_command_try_to_interpret();

int blc_loop();


void *blc_command_interpret_loop(char const *option);
///Start a thread listinning to the user entry without blocking.
void blc_command_interpret_thread(char const *option);
///Display the list of blc_command with help.
void blc_command_display_help();


void add_arg( int *argc, char ***argv, char const *arg, char const *value);
///Allocate and fill a command line with the array of argument. The array must be terminated by a NULL argument and must the returned command line me be freed after use.
///It does not handle spaces in argument !!!
char *create_command_line_from_argv(char const *const *argv);
///Allocate and fill an array of argument by spiting the command_line. The array is terminated by a NULL argument and it must be freed after used.
///It does not handle spaces in argument !!!
char * const*create_argv_from_command_line(char const *command_line);

///Set the desciption of the program used for help. A copy of desciption is done (strdup).
void blc_program_set_description(char const *description);
/// Add parameter required or not after the program name
void blc_program_add_parameter(char const **result, char const *name, int required_nb, char const *help);
///Add a possible option to the program. The argument following the option, if any, it will be put in result otherwise "1" will be put in result.

void blc_program_add_channel_parameter(blc_channel *channel, int required_nb, char const *help);
void blc_program_add_channel_option(blc_channel *channel, char letter, char const *long_option, char const *help, char const* default_value);
void blc_program_add_input_pipe_option(FILE *file, char letter, char const *long_option, char const *help, char const* default_value);
void blc_program_add_output_pipe_option(FILE *file, char letter, char const *long_option, char const *help, char const* default_value);
void blc_program_add_channel_parameter(blc_channel *channel, int required_nb, char const *help);


void blc_program_option_add( char const**result, char letter, char const *long_option, char const *parameter_type, const char *help, char const* default_value);
///Interpret the program arguments and update the results values as it should.
void blc_program_option_interpret(int *argc, char **argv[]);
///Do program_option_interpret but print the name of the program (argv[0]) underlined with '='.
void blc_program_option_interpret_and_print_title(int *argc, char ***argv);
///Display the different possible argument for the  program.
void blc_program_option_display_help();
/** Start a textual program.
 * This has to be called after defining the parameters of the program
* - Parse the arguments of the programs
* - display the title
* - check the color possibility of the terminal stderr
* - associate the blc_quit function at exit and eventually your 'exit_cb' if it not NULL.
* - set internal variables setting if the input and output terminal are tty*/
void blc_program_init(int *argc, char ***argv, void(*exit_cb)(void));

void blc_program_loop_init();
int blc_program_loop_start();
void blc_program_loop_end();

/** Stop a textual program
 * - Send a quitting message with the name of the app on stderr.
 * - Send 'q' and flush on stdout if it is a piped output
 * - quit with no error (0).
 */
void blc_quit();

void blc_close_pipe(int *pipe);

END_EXTERN_C
#endif
///@}
