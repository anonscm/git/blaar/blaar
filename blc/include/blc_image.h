/*
 Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)
 
 Author:  Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. *
 */

#ifndef BLC_IMAGE_H
#define BLC_IMAGE_H

#include "blc_mem.h"
#include "stdint.h" //uint32_t

/*
typedef struct blc_image_info
{
    uint32_t format;
    int width, height, bytes_per_pixel, pixels_nb;
}blc_image_info;*/

START_EXTERN_C
///Return the number of bytes for a pixel of the format. -1 is returned if this data is undefined (i.e. JPEG)
int blc_get_bytes_per_pixel(blc_narray const *image);


///Allocate the narray with jpeg data
void blc_narray_jpeg_init(blc_narray *image, blc_narray const *mem);

void blc_narray_init_image(blc_narray *narray, uint32_t type, uint32_t format, size_t width, size_t height);

/// Copy image. Make the conversion if needed. Be carefull, few checks are done if the memory is not big enough
void blc_image_copy(blc_narray *output, blc_narray const *input);
END_EXTERN_C
#endif
