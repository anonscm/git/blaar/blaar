/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)

 Author: A. Blanchard

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

/**
@date Apr 28, 2014
@author Arnaud Blanchard
@defgroup blc_mem Memory strucuture with a pointer and a size.
@{
This kind of functionnalities are more complete with the Standard Template Library (STL) but it much simpler here.
*/

#ifndef BLC_MEM_H
#define BLC_MEM_H

#include "blc_tools.h"
#include <stdio.h>

typedef struct blc_mem {
   char *data;
   size_t size;

#ifdef __cplusplus
   /** Create and allocate memory of size size */
   blc_mem(size_t size = 0);
    void allocate();
   /** Change the size of memory. The content is not preserved. Use size = 0 to free the memory. */
   void allocate(size_t size);
   /// Allocate more memory if needed but does not free memory 
   void allocate_min(size_t size);
   /** Change the size of memory. But keep the content.*/
   void reallocate(size_t size);
   /** Increases the memory of size and adds the new content.*/
   void append(char const *new_data, size_t size);
   /** Replace the memory with the new data and size*/
   void replace(char const *new_data, size_t size);
   /** Reset all the value to 'value'. It will be converted in unsigned char. */
   void reset(int value=0);
#endif
}blc_mem;

typedef struct blc_narray
#ifdef __cplusplus
:blc_mem{
    blc_narray(int dims_nb=0); //dim 0
    blc_narray(int dims_nb, int length, ...);
    void destroy();
    
    void add_dim(int length, int step);
    void add_dim(int length);

    void init_image(uint32_t type, uint32_t format, int width, int height);

    void set_dims(int dims_nb, int length, ...);
    int sprint_dims(char *string, int size);
    int fprint_dims(FILE *file);
    int get_type_size();
    size_t get_minimum_size();
    
#else
    {
    blc_mem mem;
#endif
    size_t *steps; // check how useful
    int *lengths;
    int dims_nb;
    uint32_t type, format;
}blc_narray;


START_EXTERN_C
/// Allocate or reallocate memory as needed but lose the content.
void blc_mem_allocate(blc_mem *mem, size_t size);
/// Reallocate memory and keep the content.
void blc_mem_reallocate(blc_mem *mem, size_t size);
/// Adapt the size and replace the content with the new one
void blc_mem_replace(blc_mem *mem, char const* data, size_t size);
/// Increase the size and append the data at the end.
void blc_mem_append(blc_mem *mem, char const* data, size_t size);

///Return the number of bytes for a pixel of the format. -1 is returned if this data is undefined (i.e. JPEG)
int blc_get_bytes_per_pixel(uint32_t format);

void    blc_narray_destroy(blc_narray *narray);
END_EXTERN_C
///@}
#endif

