/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)

 Author: A. Blanchard

 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
Created on: Apr 28, 2011
*/

#ifndef BLC_TOOLS_H
#define BLC_TOOLS_H

/**
 @defgroup blc_tools Basic tools (memory, error) ...
 @{*/
 

///Start a C function definition block. The function inside this block will be able to be call by C programs even if it is compiled in C++.
#ifdef __cplusplus
#define START_EXTERN_C extern "C" {
///End a C definition block.
#define END_EXTERN_C }
#else
#define START_EXTERN_C
///End a C definition block.
#define END_EXTERN_C
#endif

#include <sys/param.h>
#include <stdio.h> /* FILE* */
#include <stdlib.h> /* size_t */
#include <stdarg.h> /* variable arguments ... */
#include <string.h> /*memset*/
#include <stdint.h> /* uint32_t */



/// Set zeros to any structures. These structures have to be static otherwise the macro cannot determine the size of the structure.
#define CLEAR(structure) memset(&(structure), 0, sizeof(structure))

/// Clip the value on the number between 0 and 255. Usefull to convert any number to a uchar.
#define CLIP_UCHAR(x) ((x) < 0 ? 0 : ((x) >= 256 ? 255 : (x)))

/// Free the content of the pointer and set it to NULL. Useful to be sure that a freed pointer will not be used again.
#define FREE(pointer) do{ free((void*)pointer); pointer=NULL; }while(0)

/// Do a modulo even with negative numbers.
#define MOD(a,b) ((((a)%(b))+(abs(b)))%(b))

#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX _POSIX_HOST_NAME_MAX //Darwin (APPLE) does not have HOST_NAME_MAX definition
#endif

///definition of qsort_r in the with the BSD syntax even on gnuC. Be careful the compar function passed in argument has to be adapted as well. The void *arg is expected as a third argument on gnuC and it is as a first on Darwin
#ifdef BSD
#define QSORT_R qsort_r
#else
#define QSORT_R(base, elements_nb, width, arg, compar) (base, elements_nb, width, compar, arg);
#endif

///Convert a define in string e.g. STRINGIFY(EXIT_FAILURE) -> "EXIT_FAILURE"
#define STRINGIFY(string) #string

///Convert the content of a definition in string e.g. STRINGIFY_CONTENT(EXIT_FAILURE) -> "1"  Utile pour afficher les parametres de compilations.
#define STRINGIFY_CONTENT(variable) STRINGIFY(variable)

///Convert a string of 4 chars to a uint32_t number. Usefull for FOURCC pixel format. e.g. you can campare pixel_format with 'YUV2' or 'JPEG' or 'RGB3'. 
#define STRING_TO_UINT32(x) ntohl(*(uint32_t*)x)

//
#define UINT32_TO_STRING(uint32_print, x) ((char*)&(uint32_print=htonl(x)))

///Shorcut to use in formated string to define a maximal size of the receiving string.
#define SCAN_CONV(size_max, format)  "%" STRINGIFY(size_max)format

///Print a red error message with the file, line and position of the caller with the message in parameter with printf format. Then raise SIGABRT signal (usefull for debugging) and exit with code EXIT_FAILURE.
#define EXIT_ON_ERROR(...) fatal_error(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)
///Print a orange waring message with the file, line and position of the caller with the message in parameter with printf format. Then return.
#define PRINT_WARNING(...) print_warning(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)
///Like @ref EXIT_ON_ERROR butprints also the system error message corresponding to the last system error (errno).
#define EXIT_ON_SYSTEM_ERROR(...) fatal_system_error(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)
///Like @ref PRINT_WARNING but print also the system error message corresponding to the last system error (errno).
#define PRINT_SYSTEM_ERROR(...) print_system_error(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)

// Put NULL as last argument if you do not want to bother. The do while is usefule if it is included in a if then else.
#define SYSTEM_ERROR_CHECK(command, bad_value, ...) do{if ((command) == bad_value) fatal_command_system_error(__FILE__, __FUNCTION__, __LINE__, STRINGIFY(command),__VA_ARGS__);}while(0)
#define SYSTEM_SUCCESS_CHECK(command, good_value, ...) do{if ((command) != good_value) fatal_command_system_error(__FILE__, __FUNCTION__, __LINE__, STRINGIFY(command),__VA_ARGS__);}while(0)


/**Like @ref SYSTEM_ERROR_CHECK but the command is reexecuted if the errno is errno_accepted (Usually in cas od  interruption EINTR).*/
#define SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(command, bad_value, errno_accepted, ...) while((command) == bad_value) { if (errno!=(errno_accepted)) fatal_command_system_error(__FILE__, __FUNCTION__, __LINE__, STRINGIFY(command),__VA_ARGS__);}


///Do a malloc big enough to contain 'type', checks if it succeed and return the memory casted as 'type*'.
#define ALLOCATION(type) (type*)secure_malloc(__FILE__, __FUNCTION__,__LINE__, sizeof(type))
///Like @ref ALLOCATION but allocates 'numbers' elements of 'type'.
#define MANY_ALLOCATIONS(numbers, type) (type*)secure_malloc(__FILE__,  __FUNCTION__, __LINE__, (numbers)*sizeof(type))
///Like @ref ALLOCATION but reallocates 'pointer' to contain 'numbers' element of 'type'. The content of pointer may be changed if needed. If pointer is NULL, this macro has the same effect than MANY_ALLOACTIONS. @return Nothing as the 'pointer' is directly modified.
#define MANY_REALLOCATIONS(array_pt, numbers) secure_realloc(__FILE__,  __FUNCTION__, __LINE__, (void**)array_pt, (numbers)*sizeof(**(array_pt)))
///Append the content of the object pointed by new_element to the array pointed by pointer. The size of the array will increase and the pointeris changed acordingly. The number of element is incremented. The new element is returned. The size of the element is determined by the type of pointer. 
#define APPEND_ITEM(array_pt, numbers_pt, new_element_pt) append_item(__FILE__,  __FUNCTION__, __LINE__, (void**)array_pt, sizeof(**(array_pt)), numbers_pt, sizeof(*(new_element_pt)), (void*)new_element_pt)
///Insert the content of the object pointed by new_element to the array pointed by pointer. The size of the array will increase and the pointer will be changed acordingly. To insert the data, a part of the array is copied, which may not be optimal. The number of elements is incremented. The size of the element is determined by the type of pointer.
#define INSERT_ITEM(array_pt, numbers_pt, new_element_pt, position) insert_item(__FILE__,  __FUNCTION__, __LINE__, (void**)array_pt, sizeof(**(array_pt)), numbers_pt, sizeof(*(new_element_pt)), (void*)new_element_pt, position)
///Remove item by replacing the item at position by the last item and liberating the memory. This change the order of the element in the array. If you use a loop, do not forget to reiterate the current position as it as now a new item ( the one that was at teh end of the array ).
#define REMOVE_ITEM_ID(array_pt, numbers_pt, position) remove_item_id(__FILE__,  __FUNCTION__, __LINE__, (void**)array_pt, sizeof(**(array_pt)), numbers_pt, position)

#define GET_ITEM_POSITION(array, numbers, searched_item_pt) get_item_position(__FILE__,  __FUNCTION__, __LINE__, (void**)array, sizeof(*(array)), numbers, sizeof(*(searched_item_pt)), (void*)searched_item_pt)

/** Log the data in the file log_file. log_file has to be managed (open/close), by the caller. You can have many different log file depending on the caller. It can also be stdout or stderr. In this case declare log_file = stdout/stderr. It is not yet stabilized.*/
#define BLC_LOG(...) printf_log("blc", __VA_ARGS__)

///like sprintf but check that the buffer is big enough to contain the string. It only works with static buffer. It does not return the string size.
#define SPRINTF(buffer, ...) do{ if (snprintf(buffer, sizeof(buffer), __VA_ARGS__) >= (int)sizeof(buffer))  EXIT_ON_ERROR("The string is too long for the buffer of size '%ld'.", sizeof(buffer));}while(0) //tricks to not have problem in a if

///like strcpy but check that the buffer is big enough to contain the string. It only works with static buffer.
#define STRCPY(buffer, source) do {if (strlen(source)+1 > (int)sizeof(buffer)) EXIT_ON_ERROR("The length ('%d') of the your source ('%s') is longer than the receiving buffer: '%ld'.", strlen(source)+1, source, sizeof(buffer)); else strcpy(buffer, source);}while(0) //tricks to not have problem in a if


/// Do a fscanf checking than the number of interpreted arguments corresponds to the first parameter.
#define FSCANF(fields_nb, file, ... ) if (fscanf(file,  __VA_ARGS__) != fields_nb) EXIT_ON_ERROR("Failed reading %d fields.", fields_nb)

/// Do a fscanf checking than the number of interpreted arguments corresponds to the first parameter.
#define SSCANF(fields_nb, string, ... ) if (sscanf(string,  __VA_ARGS__) != fields_nb) EXIT_ON_ERROR("Failed reading %d fields in '%s'.", fields_nb, string)

/// Like FSCANF but generates only a warning in case of error.
#define TRY_FSCANF(fields_nb, file, ... ) if (fscanf(file, __VA_ARGS__) != fields_nb) PRINT_WARNING("Failed trying reading %d fields in '%s'.", fields_nb)

///Shorcut to do a loop from 0 to max-1 element. If it is not break, the last iterator after the loop is max.
#define FOR(iterator, max) for (iterator = 0; iterator != max; ++iterator)

///Shorcut to do a loop from max-1 elements to 0. This is a bit faster. If it is not broken, the last iterator after the loop is -1.
#define FOR_INV(iterator, max) for (iterator = max; iterator--;)

///Set the iterator fom the fist lement of the array 
#define FOR_EACH(iterator, array, length) for(iterator=array; iterator!=(array)+(length); iterator++) // May not be optimal
#define FOR_EACH_INV(iterator, array, length) for (iterator=(array)+(length); array!=iterator--;)


#ifndef BSD
#define HTONL(x) x=htonl(x)
#define NTOHL(x) x=ntohl(x)
#endif

// If it has been initialise it points toward argv[0]. Useful to debug different program in parallel.
extern char const *blc_program_name;
extern FILE *blc_log_file;
typedef unsigned char uchar;


START_EXTERN_C
size_t blc_get_type_size(uint32_t type);
void print_warning(const char *name_of_file, const char* name_of_function, int numero_of_line, const char *message, ...);
void print_system_error(const char *name_of_file, const char* name_of_function, int numero_of_line, const char *message, ...);

void fatal_error(const char *name_of_file, const char* name_of_function, int numero_of_line, const char *message, ...);
void fatal_system_error(const char *name_of_file, const char* name_of_function, int numero_of_line, const char *message, ...);
void fatal_command_system_error(const char *name_of_file, const char* name_of_function, int numero_of_line, char const *command, const char *message, ...);
void printf_log(const char* log_name, ...);
void *secure_malloc(const char *file , const char *function,  int line, size_t size);
void secure_realloc(char const *file , char const *function, int line, void** pointer, size_t size);
void *append_item(const char *file, const char *function, int line, void** pointer, size_t pointer_content_size, int *items_nb, size_t item_size, void* new_item);
void *insert_item(const char *file, const char *function, int line, void** pointer, size_t pointer_content_size, int *items_nb, size_t item_size, void* new_item, int position);
void remove_item_id(const char *file, const char *function, int line, void** pointer, size_t pointer_content_size, int *items_nb, int position);
int get_item_position(const char *file, const char *function, int line, void const* const* array_pt, size_t pointer_content_size, int items_nb, size_t item_size, void* searched_item_pt);

/** return a random char. May be converted to uint8_t */
char rand_char();

//int set_iovec(struct iovec *iovec, void *data, size_t size, ...);
END_EXTERN_C
///@}
#endif /* BLC_TOOLS_H */

