//
//  blc_network.h
//  network
//
//  Created by Arnaud Blanchard on 30/05/2015.
//
//

/**
 @defgroup blc_network network
 Few functions helping for pseudo realtime applications.
 @{*/
#ifndef BLC_NETWORK_H
#define BLC_NETWORK_H

#include <stdio.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include "blc_mem.h"

enum {BLC_UDP4};
/* To be used with readv/writev, sendmsg/recvmsg. */
#define SET_IOVEC(iovec, variable)  set_iovec(iovec, &(variable), sizeof(variable), NULL)
#define SET_IOVEC2(iovec, variable1, variable2)  set_iovec(iovec, &(variable1), sizeof(variable1), &(variable2), sizeof(variable2), NULL)
#define SET_IOVEC3(iovec, variable1, variable2, variable3)  set_iovec(iovec, &(variable1), sizeof(variable1),&(variable2), sizeof(variable2),&(variable3), sizeof(variable3), NULL)

typedef void (*type_blc_network_callback)(char *, size_t size, void *arg);

typedef struct blc_network
{
    blc_mem mem;
    int socket_fd;
    int socket_address_len;
    struct msghdr msghdr;
    struct sockaddr_storage socket_address;
    
#ifdef __cplusplus
    void init(char const *address_name, char const *port_name, int mode);
#endif
}blc_network;

typedef struct blc_server
#ifdef __cplusplus
:blc_network {
#else
{ blc_network network;
#endif
    
    int running;
    pthread_t thread;
    type_blc_network_callback callback;
    void *callback_arg;
    
#ifdef __cplusplus
    void start(char const *port_name, int mode, type_blc_network_callback on_receive_data, void *arg);
    void allocate_mem_and_start(char const *port_name, int mode, type_blc_network_callback on_receive_data, void *arg, int size);
#endif
}blc_server;
    
    
START_EXTERN_C
void blc_network_init(blc_network *network, char const *address_name, char const *port_name, int mode);
void blc_server_start(blc_server *server, char const *port_name, int mode, type_blc_network_callback on_receive_data, void *);
void blc_server_allocate_mem_and_start(blc_server *server, char const *port_name, int mode, type_blc_network_callback on_receive_data, void *, int size);
int set_iovec(struct iovec *iov, void *data, size_t size, ...);

END_EXTERN_C

#endif
///@}