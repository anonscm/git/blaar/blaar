/*
 Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)
 
 Author:  Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. *
  
 * 
 *
 *  Created on: Oct 9, 2014
 *      Author: Arnaud Blanchard
 */

#include "blc_channel.h"
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <pthread.h>
#include <fcntl.h> // O_... constants
#include <semaphore.h>
#include <sys/mman.h>
#include <sys/stat.h> // mode S_ ... constants
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include "blc.h"

#define BLC_CHANNEL_DEFAULT_PORT 3333
#define PAQUET_SIZE 1024 // Should be less than MTU - header_size. Usually between in [1280, 1500] - 28
#define TMP_BUFFER_SIZE 4096
#define BLC_CHANNELS_LIST_PATH "/tmp/blc_channels.txt"

typedef struct
{
    uint32_t index, position;
}type_channel_header;

enum {HEADER_MEM=0, DATA_MEM};

blc_channel **net_channels = NULL;
int net_channels_nb = 0;
int channel_server_socket = 0;
int blc_channel_port = BLC_CHANNEL_DEFAULT_PORT;
int blc_channel_id_max = 0;
sem_t *blc_channels_sems[2]={NULL, NULL};

int sem_id=-1;


/* We suppose sizeof(int) >= 4 !!! */
void listen_data(blc_mem *mems, int mems_nb, void *arg)
{
    int i;
    blc_channel *channel = (blc_channel*)arg;
    div_t div_result;
    type_channel_header header;
    int *destination, *source;
    (void) mems_nb;
    
    header = *(type_channel_header*)mems[HEADER_MEM].data;
    /* We do not use memcpy to be sure that the copy are 4 by 4. The memory has also to be aligned. TODO check if we can do int by int*/
    destination = (int*)channel->data + header.position*PAQUET_SIZE;
    source = (int*)mems[DATA_MEM].data;
    div_result = div(mems[DATA_MEM].size, sizeof(int));
    
    FOR_INV(i, div_result.quot) *destination++ = *source++;
    
    /* If sizeof(int) = 8, a chunk of 4 can be forgotten */
    if (div_result.rem == 4) *(int32_t*)destination = *(int32_t*)source;
    else  EXIT_ON_ERROR("Data receive is not a multiple of 4 : %d", mems[DATA_MEM].size);
}

void blc_channel::fprint_info(FILE *file){
    char text[4];
    int width;
    uint32_t net_type = htonl(type);
    uint32_t net_format = htonl(format);
    
    if (type==0) EXIT_ON_ERROR("The type should not be null for '%s'. Use NDEF by default.", name);
    if (format==0) EXIT_ON_ERROR("The format should not be null for '%s'. Use NDEF by default.", name);
    if (parameter[0]==0) EXIT_ON_ERROR("The parameter should not be null for '%s'. Use NDEF by default.", name);
    
    if (sem_flags&BLC_SEM_ACK_LOCKED) text[0]='A';
    else if (sem_flags&BLC_SEM_ACK) text[0]='a';
    else text[0]='-';
    
    if (sem_flags&BLC_SEM_BLOCK_LOCKED) text[1]='B';
    else if (sem_flags&BLC_SEM_BLOCK) text[1]='b';
    else text[1]='-';
    
    if (sem_flags&BLC_SEM_PROTECT_LOCKED) text[2]='P';
    else if (sem_flags&BLC_SEM_PROTECT) text[2]='p';
    else text[2]='-';
    fprintf(file, "%6d %.3s %.4s %.4s %-31s ", id, text, (char*)&net_type, (char*)&net_format, parameter);
    width=fprint_dims(file);
    fprintf(file, "%*c", 32-width, ' ');
    if (strlen(name)==0) EXIT_ON_ERROR("name is empty");
    fprintf(file, " %s\n", name);
}

int blc_channel::fscan_info(FILE *file){
    char sem_str[3];
    int ret, length;
    
    dims_nb=0;
    
    ret = fscanf(file, "%d %3c %4c %4c %31s %d", &id, sem_str, (char*)&type, (char*)&format, parameter, &length);
    if (ret == EOF) return -1;
    else if (ret!=6) EXIT_ON_ERROR("Only %d parameters have been read instead of 6 in " BLC_CHANNELS_LIST_PATH" id '%d'.", ret, id);
  
    sem_flags=0;
    if (memchr(sem_str, 'a', 3)) sem_flags|=BLC_SEM_ACK;
    if (memchr(sem_str, 'b', 3)) sem_flags|=BLC_SEM_BLOCK;
    if (memchr(sem_str, 'p', 3)) sem_flags|=BLC_SEM_PROTECT;
    
    NTOHL(type);
    NTOHL(format);
    do
    {
        APPEND_ITEM(&lengths, &dims_nb, &length);
    }while(fscanf(file, "x%d", &length)==1);
    
    if (fscanf(file, " %"STRINGIFY_CONTENT(NAME_MAX)"[^\n]s\n", name) != 1) EXIT_ON_ERROR("Impossible to read channel's name of channel id '%d'.", id);
    if (id > blc_channel_id_max) blc_channel_id_max=id;
    return id;
}


blc_channel::blc_channel(int dims_nb):sem_ack(NULL), sem_block(NULL), sem_protect(NULL){
}


blc_channel::blc_channel(char const *name):sem_ack(NULL), sem_block(NULL), sem_protect(NULL)
{
    set_namef(name);
    open();
}

blc_channel::blc_channel(char const *name, int length, int sem_flags, uint32_t type, uint32_t format, char const *parameter)
{
    set_namef(name);
    create(sem_flags, type, format, parameter, 1, length);
}


blc_channel::blc_channel(char const *name, int sem_flags, uint32_t type, uint32_t format, char const *parameter, int dims_nb, int length, ...)
{
    va_list arguments;
    
    set_namef(name);
    va_start(arguments, length);
    vcreate(sem_flags, type, format, parameter, dims_nb, length, arguments);
    va_end(arguments);
}
/*
 blc_channel::blc_channel(char const *name, int sem_flags, const char *type_string, const char *format_string, char const *parameter, int dims_nb, int length, ...)
 {
 va_list arguments;
 
 set_name(name);
 va_start(arguments, length);
 vcreate(sem_flags, STRING_TO_UINT32(type_string), STRING_TO_UINT32(format_string), parameter, dims_nb, length, arguments);
 va_end(arguments);
 }*/

blc_channel::blc_channel(char const *name, int sem_flags, uint32_t type, uint32_t format, char const *parameter, int dims_nb, int length, va_list arguments)
{
    set_namef(name);
    vcreate(sem_flags, type, format, parameter, dims_nb, length, arguments );
}

blc_channel::blc_channel(char const *name, int sem_flags, const char *type_string, const char *format_string, char const *parameter, int dims_nb, int length, va_list arguments)
{
    set_namef(name);
    vcreate(sem_flags, STRING_TO_UINT32(type_string), STRING_TO_UINT32(format_string), parameter, dims_nb, length, arguments );
}

void blc_channel::set_namef(char const *format, ...)
{
    va_list arguments;
    va_start(arguments, format);
    if (vsnprintf(this->name, sizeof(this->name), format, arguments) >= (int)sizeof(this->name)) EXIT_ON_ERROR("Channel name '%s' has been cropped, it is more than '%d' chars", name, sizeof(name));
    else if (name[0]!='/') EXIT_ON_ERROR("Shared memory names have to start with '/' but it is not the case in '%s'.", name);
    va_end(arguments);
}

void blc_channels_event()
{
    if (sem_id==-1)
    {
        SYSTEM_ERROR_CHECK(blc_channels_sems[0] = sem_open("blc_channels_sem1", O_CREAT | O_RDWR, S_IRWXU, 0), NULL, "Creating blc_channels_sem");
        SYSTEM_ERROR_CHECK(blc_channels_sems[1] = sem_open("blc_channels_sem2", O_CREAT | O_RDWR, S_IRWXU, 0), NULL, "Creating blc_channels_sem");
        sem_id=0;
    }
    
    SYSTEM_ERROR_CHECK(sem_post(blc_channels_sems[0]), -1, NULL);
    SYSTEM_ERROR_CHECK(sem_post(blc_channels_sems[1]), -1, NULL);
}


void blc_channel::open_or_create_semaphores()
{
   char sem_name[SEM_NAME_LEN+1];
   int sem_value;

   if (sem_flags & BLC_SEM_ACK){
         if (sem_flags & BLC_SEM_ACK_LOCKED) sem_value=0;
         else sem_value=1;
         sprintf(sem_name, "blc_channel_%d-ack", id);
         SYSTEM_ERROR_CHECK(sem_ack = sem_open(sem_name, O_CREAT | O_EXCL | O_RDWR, S_IRWXU, sem_value), SEM_FAILED, "Creating ACK semaphore '%s' for channel '%s'.", sem_name, name);
     }

     if (sem_flags & BLC_SEM_BLOCK){
         if (sem_flags&BLC_SEM_BLOCK_LOCKED) sem_value=0;
         else sem_value=1;
         sprintf(sem_name, "blc_channel_%d-block", id);
         SYSTEM_ERROR_CHECK(sem_block = sem_open(sem_name, O_CREAT | O_EXCL  | O_RDWR, S_IRWXU, sem_value), SEM_FAILED, "Creating BLOCK semaphore '%s' for channel '%s'.", sem_name, name);
     }
     if (sem_flags & BLC_SEM_PROTECT){
         if (sem_flags & BLC_SEM_PROTECT_LOCKED) sem_value=0;
         else sem_value=1;

         sprintf(sem_name, "blc_channel_%d-protect", id);
         SYSTEM_ERROR_CHECK(sem_protect = sem_open(sem_name, O_CREAT | O_EXCL | O_RDWR, S_IRWXU, sem_value), SEM_FAILED, "Creating PROTECT semaphore '%s' for channel '%s'.", sem_name, name);
     }
}

void blc_channel::create(char const *name, int sem_flags, char const *parameter){
    FILE *file;
    int sem_value;
    
    id = blc_channel_get_info_with_name(this, name);
    if (id!=-1) EXIT_ON_ERROR("The channel '%s' is already referenced in '" BLC_CHANNELS_LIST_PATH"'.\nUnlink it before.", name);
    blc_channel_id_max++;
    id = blc_channel_id_max;
    
    STRCPY(this->name, name);
    this->sem_flags=sem_flags;
    
    if ((parameter==NULL) || (strlen(parameter)==0)) strcpy(this->parameter, "NDEF");
    else STRCPY(this->parameter, parameter);
    
    SYSTEM_ERROR_CHECK(file=fopen(BLC_CHANNELS_LIST_PATH, "a+"), NULL, "Openning the file '" BLC_CHANNELS_LIST_PATH"' in order to reference the channel '%s'.", name);
    
    fd = shm_open(name, O_CREAT | O_EXCL | O_RDWR, S_IRWXU); // (O_EXCL first) is important in order to avoid a race condition.  We create it at the same time we check it does not exist. Otherwise someone else could create it in between.
    if (fd==-1)
    {
        fclose(file);
        if (errno == EEXIST) EXIT_ON_ERROR("The shared memory '%s' already exists, you should destroy it before.", name);
        else EXIT_ON_SYSTEM_ERROR("Creating shared memory '%s'.", name);
    }
    else
    {
        fprint_info(file);
        fclose(file);
    }
    
    SYSTEM_ERROR_CHECK(ftruncate(fd,  size), -1, "fd:%d, size '%ld'.",fd, size);
    SYSTEM_ERROR_CHECK(data = (char*)mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0), MAP_FAILED,  "Mapping memory of %s (size %ld bytes), fd(%d).", name, size, fd);
    
    open_or_create_semaphores();
    
    blc_channels_event();
}



///Create a blc channel
void blc_channel::vcreate(int sem_flags, uint32_t type, uint32_t format, char const *parameter, int dims_nb, int length, va_list arguments){
    int dim;
    
    id = blc_channel_get_info_with_name(this, name);
    if (id!=-1) EXIT_ON_ERROR("The channel '%s' is already referenced in '"BLC_CHANNELS_LIST_PATH"'.\nUnlink it before.", name);
    
    this->dims_nb=dims_nb;
    this->type=type;
    this->format=format;
    lengths=MANY_ALLOCATIONS(dims_nb, int); //need to be freed
    
    size=get_type_size()*length;
    lengths[0] = length;
    for(dim=1; dim != dims_nb; dim++) {
        lengths[dim]=va_arg(arguments, int);
        size*=lengths[dim];
    }
    create(name, sem_flags, parameter);
}


///Create a blc channel
void blc_channel::vcreate_or_open(int sem_flags, uint32_t type, uint32_t format, char const *parameter, int dims_nb, int length, va_list arguments){
    int dim;
    uint32_t type_str, new_type_str;
    
    id = blc_channel_get_info_with_name(this, name);
    if (id==-1) vcreate(sem_flags, type,  format, parameter, dims_nb, length, arguments);
    else{
        if (this->dims_nb!=dims_nb) EXIT_ON_ERROR("Reopening a blc_channel of dim '%d' different from the new '%d'.", this->dims_nb, dims_nb);
        if (this->type!=type) EXIT_ON_ERROR("Reopening blc_channel of type '%.4s' instead of '%.4s'.", UINT32_TO_STRING(type_str, this->type),  UINT32_TO_STRING(new_type_str, type));
        if (this->format!=format) EXIT_ON_ERROR("Reopening blc_channel of format '%.4s' instead of '%.4s'.", UINT32_TO_STRING(type_str, this->format),  UINT32_TO_STRING(new_type_str, format));
        if (((parameter==NULL) && strcmp(this->parameter, "NDEF")!=0) || ((parameter!=NULL) && strcmp(this->parameter, parameter)!=0)) EXIT_ON_ERROR("Reopening with parameter '%s' instead of '%s'.", this->parameter,  parameter);
        if (lengths[0] != length) EXIT_ON_ERROR("The reopening first dimmension length '%d' of '%s' differ from the existing one '%d'.", length, name, lengths[0]);
        for(dim=1; dim != dims_nb; dim++) {
            if (lengths[dim]!=va_arg(arguments, int))  EXIT_ON_ERROR("The reopening dimmension %d  length '%d' of '%s' differ from the existing one '%d'.", dim, length, name, lengths[dim]);
        }
        open();
    }
}

/**
 @param new_name of the channel. You need '/' at the begining. It is referenced in "/tmp/blc_channels.lst." The files also appears in /dev/shm/ on Linux.
 @param new_size, size of the requested channel. 0 or current size if the channel already exists.
 @param sem_abp a string defining the semaphores to create with it. "a" for ack, "b" for block and "p" for protect. The ack semaphore is used to block until someone read the value. Block is used to check if there is new values. Protect is used to be sure that nothingis reading or writing at the same time. It is conventions, you have to manage (sem_post, sem_wait) the semafores yourself.
 */
void blc_channel::create(int semaphores, uint32_t type, uint32_t format,  char const *parameter, int dims_nb, int length0, ...)
{
    va_list arguments;
    
    va_start(arguments, length0);
    vcreate(semaphores, type, format, parameter, dims_nb, length0, arguments);
    va_end(arguments);
}

/**
 @param new_name of the channel. You need '/' at the begining. It is referenced in "/tmp/blc_channels.lst." The files also appears in /dev/shm/ on Linux.
 @param new_size, size of the requested channel. 0 or current size if the channel already exists.
 @param sem_abp a string defining the semaphores to create with it. "a" for ack, "b" for block and "p" for protect. The ack semaphore is used to block until someone read the value. Block is used to check if there is new values. Protect is used to be sure that nothingis reading or writing at the same time. It is conventions, you have to manage (sem_post, sem_wait) the semafores yourself.
 */
void blc_channel::create_or_open(int sem_flags, uint32_t type, uint32_t format,  char const *parameter, int dims_nb, int length0, ...)
{
    va_list arguments;
        
    va_start(arguments, length0);
    vcreate_or_open(sem_flags, type, format, parameter, dims_nb, length0, arguments);
    va_end(arguments);
}

void blc_channel::create_or_open(char const *name, int sem_flags, char const *parameter)
{
    int dim;
    uint32_t new_type_str, type_str;
    blc_channel info;
    
    id = blc_channel_get_info_with_name(&info, name);
    if (id==-1) create(name, sem_flags, parameter);
    else {
        if (dims_nb!=info.dims_nb) EXIT_ON_ERROR("Reopening a blc_channel of dim '%d' different from the new '%d'.", info.dims_nb, dims_nb);
        if (type!=info.type) EXIT_ON_ERROR("Reopening blc_channel of type '%.4s' instead of '%.4s'.", UINT32_TO_STRING(type_str, info.type),  UINT32_TO_STRING(new_type_str, type));
        if (format!=info.format) EXIT_ON_ERROR("Reopening blc_channel of format '%.4s' instead of '%.4s'.", UINT32_TO_STRING(type_str, info.format),  UINT32_TO_STRING(new_type_str, format));
        if (((parameter==NULL) && strcmp(info.parameter, "NDEF")!=0) || ((parameter!=NULL) && strcmp(info.parameter, parameter)!=0)) EXIT_ON_ERROR("Reopening with parameter '%s' instead of '%s'.", info.parameter,  parameter);
        for(dim=0; dim != dims_nb; dim++) {
            if (info.lengths[dim]!=lengths[dim])  EXIT_ON_ERROR("The reopening dimmension %d  length '%d' of '%s' differ from the existing one '%d'.", dim, lengths[dim], name, info.lengths[dim]);
        }
        set_namef(name);
        open();
    }
}


///Open an existing channel
void blc_channel::open(const char *new_name)
{
    char sem_name[SEM_NAME_LEN+1];
    
    if (new_name) set_namef(new_name);
    
    id = blc_channel_get_info_with_name(this, name);
    if (id==-1) EXIT_ON_ERROR("The blc_channel '%s' does not exist. See '" BLC_CHANNELS_LIST_PATH"'.", name);
    size=get_minimum_size();
    
    if (sem_flags & BLC_SEM_ACK)
    {
        SPRINTF(sem_name, "blc_channel_%d-ack", id);
        SYSTEM_ERROR_CHECK(sem_ack = sem_open(sem_name, O_RDWR, S_IRWXU), SEM_FAILED, "sem name: '%s' for channel '%s'.", sem_name,name);
    }
    
    if (sem_flags & BLC_SEM_BLOCK)
    {
        SPRINTF(sem_name, "blc_channel_%d-block", id);
        SYSTEM_ERROR_CHECK(sem_block = sem_open(sem_name, O_RDWR, S_IRWXU), SEM_FAILED, "sem name: '%s' for channel '%s'.", sem_name, name);
    }
    
    if (sem_flags & BLC_SEM_PROTECT)
    {
        SPRINTF(sem_name, "blc_channel_%d-protect", id);
        SYSTEM_ERROR_CHECK(sem_protect = sem_open(sem_name, O_RDWR, S_IRWXU), SEM_FAILED, "sem name: '%s' for channel '%s'.", sem_name, name);
    }
    
    SYSTEM_ERROR_CHECK(fd = shm_open(name, O_RDWR , S_IRWXU), -1, "Impossible to open shared memory '%s'.", name);
    SYSTEM_ERROR_CHECK(data = (char*)mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0), MAP_FAILED,  "Mapping memory of %s (%ld bytes)", name, size);
}



/*
 void *waiting_on_peer_change(void *arg)
 {
 blc_channel *channel = (blc_channel*)arg;
 sockaddr_in socket_addr;
 socklen_t socket_addr_len;
 ssize_t recv_size;
 char name[FILENAME_MAX];
 
 while(1)
 {
 recv_size = recvfrom(channel->socket_fd, name, FILENAME_MAX, 0, (sockaddr*)&socket_addr, &socket_addr_len);
 if (recv_size==-1)
 {
 EXIT_ON_SYSTEM_ERROR("recvfrom port %d", ntohs(socket_addr.sin_port));
 }
 
 if (strncmp(name, channel->name, FILENAME_MAX) == 0)
 {
 if (bind(channel->socket_fd, (sockaddr*)&socket_addr, socket_addr_len) !=0) EXIT_ON_SYSTEM_ERROR("Binding");
 BLC_LOG("We receive change in the server for channel: %s", name);
 }
 else EXIT_ON_ERROR("%s try to receive data from %s, size %ld.", name, channel->name, recv_size);
 }
 return NULL;
 };*/

blc_channel::blc_channel(char const *new_name, size_t new_size, int sem_flags, const char *hostname): index(0)
{
    struct iovec size_and_name[2];
    struct msghdr message;
    (void)hostname;
    //   pthread_t thread;
    
    
    if (strlen(new_name) >= FILENAME_MAX) EXIT_ON_ERROR("Channel name %s is too long more than %d.", new_name, FILENAME_MAX);
    else strcpy(name, new_name);
    
    //  set_sockaddr_in(&target, AF_INET, inet_addr(hostname), blc_channel_port);
    
    CLEAR(message);
    message.msg_name = &target;
    message.msg_namelen = sizeof(target);
    message.msg_iov = size_and_name;
    message.msg_iovlen = set_iovec(size_and_name, (void*)&size, sizeof(size), (void*)new_name, strlen(new_name), NULL);
    
    socket_fd = socket(PF_INET, SOCK_DGRAM, 0);
    if (socket_fd == -1) EXIT_ON_SYSTEM_ERROR("Creating channel socket");
    /*  if (connect(socket_fd, (struct sockaddr *)&target,  sizeof(struct sockaddr_in)) == -1) {
     if (errno == ENETUNREACH) PRINT_WARNING("Receiver not ready.");
     else EXIT_ON_SYSTEM_ERROR("Connect port %d error%d", BLC_CHANNEL_PORT, errno);
     }*/
    //    if(bind(socket_fd, (const struct sockaddr*)&target, sizeof(target))==-1) EXIT_ON_SYSTEM_ERROR("binding");
    if (sendmsg(socket_fd, &message, 0)==-1) EXIT_ON_SYSTEM_ERROR("Sending channel '%s' of size %ld", name, size);
    BLC_LOG("Sending size %d and name %s", size, name);
    //   if (pthread_create(&thread, NULL, waiting_on_peer_change, this)) EXIT_ON_SYSTEM_ERROR("pthread_create");
}

/*
blc_channel::~blc_channel()
{
    if (data) munmap(data, size);
    if (sem_ack) sem_close(sem_ack);
    if (sem_block) sem_close(sem_block);
    if (sem_protect) sem_close(sem_protect);
    if (fd==-1) close(fd);
    if (id==blc_channel_id_max) blc_channel_id_max--;
}
 */

void blc_channel::unlink()
{
    FILE *list_file;
    blc_channel info;
    char tmp_name[NAME_MAX];
    fpos_t write_position, read_position;
    char *buffer[TMP_BUFFER_SIZE];
    size_t data_read;
    off_t offset;
    
    SYSTEM_ERROR_CHECK(shm_unlink(name), -1, "unlinking blc_channel '%s'", name);
    
    if (sem_flags&(BLC_SEM_ACK|BLC_SEM_ACK_LOCKED))
    {
        sprintf(tmp_name, "blc_channel_%d-ack", id);
        SYSTEM_ERROR_CHECK(sem_unlink(tmp_name), -1, "unlinking blc_channel '%s' semaphore '%s'", name, tmp_name);
    }
    if (sem_flags&(BLC_SEM_BLOCK|BLC_SEM_BLOCK_LOCKED))
    {
        sprintf(tmp_name, "blc_channel_%d-block", id);
        SYSTEM_ERROR_CHECK(sem_unlink(tmp_name), -1, "unlinking blc_channel '%s' semaphore '%s'", name, tmp_name);
    }
    if (sem_flags & (BLC_SEM_PROTECT|BLC_SEM_PROTECT_LOCKED))
    {
        sprintf(tmp_name, "blc_channel_%d-protect", id);
        SYSTEM_ERROR_CHECK(sem_unlink(tmp_name), -1, "unlinking blc_channel '%s' semaphore '%s'", name, tmp_name);
    }
    
    SYSTEM_ERROR_CHECK(list_file=fopen(BLC_CHANNELS_LIST_PATH, "r+"), NULL, "impossible to open " BLC_CHANNELS_LIST_PATH);
    
    while(!feof(list_file))
    {
        fgetpos(list_file, &write_position);
        info.fscan_info(list_file);
        if (strcmp(info.name, name) == 0)
        {
            if (info.id != id) EXIT_ON_ERROR("The referenced id '%d' is not equal to the current unlinking channel '%d'.", info.id, id);
            fgetpos(list_file, &read_position); //we remove the line by copying the end of the file here.
            do
            {
                fsetpos(list_file, &read_position);
                data_read = fread(buffer, 1, TMP_BUFFER_SIZE, list_file);
                fgetpos(list_file, &read_position);
                if (data_read==0) if (!feof(list_file)) EXIT_ON_ERROR("Reading file" BLC_CHANNELS_LIST_PATH);
                fsetpos(list_file, &write_position);
                SYSTEM_SUCCESS_CHECK(fwrite(buffer, 1, data_read, list_file), data_read, "");
                fgetpos(list_file, &write_position);
            }while(data_read);
            
            SYSTEM_ERROR_CHECK(offset = ftell(list_file), -1, "");
            SYSTEM_ERROR_CHECK(ftruncate(fileno(list_file), offset), -1, "");
            SYSTEM_ERROR_CHECK(fclose(list_file), -1, "");
            break;
        }
    }
    blc_channels_event();
}


void blc_channel::protect(int value)
{
    if (value)
    {
        SYSTEM_ERROR_CHECK(sem_wait(sem_protect), -1,"pointer %p, channel '%s'.", sem_protect, name);
    }
    else SYSTEM_ERROR_CHECK(sem_post(sem_protect), -1,"pointer %p, channel '%s'.", sem_protect, name);
}

void blc_channel::send()
{
    struct iovec index_position_and_buffer[3];
    struct msghdr message;
    uint32_t position;
    //   type_channel_header header;
    ssize_t data_sent;
    
    CLEAR(message);
    message.msg_iov = index_position_and_buffer;
    message.msg_iovlen = SET_IOVEC2(index_position_and_buffer, index, position)+1;
    message.msg_name = &target;
    message.msg_namelen = sizeof(target);
    
    FOR_INV(position, (size-1)/PAQUET_SIZE+1) // (size-1) for the case size=PAQUET_SIZE should still be 1
    {
        index_position_and_buffer[2].iov_base = data+position*PAQUET_SIZE;
        index_position_and_buffer[2].iov_len = MIN(PAQUET_SIZE, size-position*PAQUET_SIZE);
        SYSTEM_ERROR_CHECK(data_sent = sendmsg(socket_fd, &message, 0), -1, "Error while sending data of size %ld position %d index %d.", size, position, index);
    }
    index++;
}

void server_recv_callback(blc_mem *mems, int mems_nb, void* user_data)
{
    int i;
    blc_channel *channel=NULL;
    const char *channel_name;
    //    blc_server *server;
    (void)mems_nb;
    (void) user_data;
    
    channel_name = mems[1].data;
    
    BLC_LOG("Receive channel name %s", mems[1].data);
    
    FOR_INV(i, net_channels_nb) if(strncmp(net_channels[i]->name, channel_name, FILENAME_MAX) == 0)
    {
        channel = net_channels[i];
        BLC_LOG( "The channel %s is known",  channel_name);
        break;
    }
    
    if (channel == NULL)
    {
        /*    channel = new blc_channel(mems[1].data, (uint64_t)mems[0].data);
         if (channel->size <= PAQUET_SIZE)  server = new blc_server(BLC_UDP4, 0, NULL, NULL, channel, 1);
         else server = new blc_server(0, BLC_UDP4, listen_data, (void*)channel, 3, sizeof(uint32_t), sizeof(uint32_t), PAQUET_SIZE);
         
         APPEND_ITEM(net_channels, &net_channels_nb, &channel)
         */
    }
    send(channel->socket_fd, channel_name, strlen(channel_name), 0); //Envoi de son nom pour dire où on attend la reponse.
}

void start_channel_server(int port)
{
    (void)port;
    //    blc_server server(port, BLC_UDP4, server_recv_callback, NULL, 2, sizeof(uint64_t), FILENAME_MAX);
}


blc_channel *new_blc_channel_create(const char *name, int sem_flags, uint32_t type, uint32_t format, char const *parameter, int dims_nb, int length0, ...)
{
    blc_channel *channel;
    va_list arguments;
    
    va_start(arguments, length0);
    channel = new blc_channel(name, sem_flags, type, format, parameter, dims_nb, length0, arguments);
    va_end(arguments);
    return channel;
}

blc_channel *new_blc_channel_open(const char *name)
{
    blc_channel *channel;
    channel = new blc_channel(name);
    return channel;
}

blc_channel *new_blc_channel_create_or_open(const char *name, int sem_flags, uint32_t type, uint32_t format, char const *parameter, int dims_nb, int length, ...)
{
    blc_channel info;
    blc_channel *channel;
    int id, dim;
    va_list arguments;
    uint32_t int_string, requested_int_string;
    
    id = blc_channel_get_info_with_name(&info, name);
    if (id == -1)
    {
        va_start(arguments, length);
        channel = new blc_channel(name, sem_flags, type, format, parameter, dims_nb, length, arguments);
        va_end(arguments);
    }
    else
    {
        if (parameter==NULL) parameter="NDEF";
        if (sem_flags!=info.sem_flags) EXIT_ON_ERROR("You try to open a blc_channel  with different semaphores. ");
        else if (info.type!=type) {
            EXIT_ON_ERROR("You try to open a blc_channel  with different types. You ask '%.4s' instead of '%.4s'.", UINT32_TO_STRING(requested_int_string, type), UINT32_TO_STRING(int_string,  info.type));
        }else if (info.format!=format) {
            EXIT_ON_ERROR("You try to open a blc_channel  with different formats. You ask '%.4s' instead of '%.4s'.", UINT32_TO_STRING(requested_int_string, type), UINT32_TO_STRING(int_string,  info.type));
        }else if (strcmp(parameter, info.parameter)!=0) EXIT_ON_ERROR("You try to open a blc_channel  with different parameters. You ask '%s' instead of '%s'.", parameter, info.parameter);
        else if (dims_nb != info.dims_nb) EXIT_ON_ERROR("You try to open a blc_channel  with different dims. You ask '%d' instead of '%d'.", dims_nb, info.dims_nb);
        else
        {
            va_start(arguments, length);
            FOR(dim, dims_nb)
            {
                if (length != info.lengths[dim]) EXIT_ON_ERROR("You try to open a blc_channel  with different length on dim '%d'. You ask '%d' instead of '%d'.", dim, length, info.lengths[dim]);
                length=va_arg(arguments, int);
            }
            va_end(arguments);
        }
        channel=new blc_channel(name);
    }
    return channel;
}

void blc_channel_create_or_open(blc_channel *channel, char const *name, int sem_flags, char const *parameter){
    channel->create_or_open(name, sem_flags, parameter);
}

void destroy_blc_channel(blc_channel *channel)
{
    delete channel;
}

void blc_channel_protect(blc_channel *channel, int value)
{
    channel->protect(value);
}

int blc_channel_get_info_with_name(blc_channel *info, char const *name)
{
    FILE *file;
    blc_channel tmp_info;
    int tmp_id=-1;
    
    file = fopen(BLC_CHANNELS_LIST_PATH, "r");
    if (file == NULL)
    {
        if (errno==ENOENT) return -1;
        else EXIT_ON_SYSTEM_ERROR("opening '" BLC_CHANNELS_LIST_PATH"'.");
    }
    
    while(fscanf(file, "%*[ \t\n]")!=EOF)
    {
        tmp_info.fscan_info(file);
        if ((tmp_info.id != -1) && (strcmp(tmp_info.name, name)==0))
        {
            *info=tmp_info;
            tmp_id=info->id;
            break;
        }
    }
    fclose(file);
    return tmp_id;
}

int blc_channel_get_all_infos(struct blc_channel **channels_infos)
{
    blc_channel channel_info;
    FILE *file;
    int channels_nb=0;
    
    file = fopen(BLC_CHANNELS_LIST_PATH, "r");
    if (file == NULL) return 0;
    
    while(fscanf(file, "%*[ \t\n]s")!=EOF)
    {
        channel_info.fscan_info(file);
        APPEND_ITEM(channels_infos, &channels_nb, &channel_info);
    }
    fclose(file);
    return channels_nb;
}

int blc_channel_get_all_infos_with_filter(struct blc_channel **channels_infos, char const *filter)
{
    blc_channel channel_info;
    FILE *file;
    int channels_nb=0;
    
    file = fopen(BLC_CHANNELS_LIST_PATH, "r");
    if (file == NULL) return 0;
    while(fscanf(file, "%*[ \t\n]")!=EOF)
    {
        if (channel_info.fscan_info(file) !=-1)
        {
            if (strncmp(filter, channel_info.name, strlen(filter))==0) APPEND_ITEM(channels_infos, &channels_nb, &channel_info);
        }
    }
    fclose(file);
    return channels_nb;
}

void destroy_channels(blc_channel ***channels, int channels_nb)
{
    int i;
    FOR_INV(i, channels_nb)  delete (*channels)[i];
    free(*channels);
    channels = NULL;
}

int blc_channel_unlink(blc_channel * channel)
{
    if(channel == NULL) EXIT_ON_ERROR("The channel is NULL");
    else channel->unlink();
    return 1;
}




