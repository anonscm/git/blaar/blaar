//
//  blc_network.cpp
//  network
//
//  Created by Arnaud Blanchard on 30/05/2015.
//
//

#include "blc_network.h"

#include <pthread.h>
#include <netdb.h>
#include <sys/types.h>
#include "blc.h"

static void* server_manager(void *arg)
{
    blc_server *server=(blc_server*)arg;
    ssize_t received_data_size;
    
    while(server->running)
    {
        SYSTEM_ERROR_CHECK(received_data_size = recvmsg(server->socket_fd, &server->msghdr, 0), -1, "Error receiving data.", "toto");
        server->callback(server->mem.data, received_data_size, server->callback_arg);
    }
    return NULL;
}

void blc_network::init(char const *address_name, char const *port_name, int mode)
{
    struct addrinfo hints, *results;
    int ret;

    CLEAR(hints);
    hints.ai_flags = AI_NUMERICHOST | AI_NUMERICSERV | AI_PASSIVE; //if address name is NULL it will be passive .

    switch (mode)
    {
            case BLC_UDP4:
            hints.ai_family = PF_INET;
            hints.ai_socktype = SOCK_DGRAM;
            socket_address_len = sizeof(struct sockaddr_in);
            break;
        default:
            EXIT_ON_ERROR("Unknown mode %d. The possibility is 'BLC_UDP4'.", mode);
            return; // To avoid warning;
    }
    ret=getaddrinfo(address_name, port_name, &hints, &results);
    if (ret !=0) EXIT_ON_ERROR(gai_strerror(ret)); //, 0, "setting address: %s", address_name));
    if(results->ai_next != NULL) EXIT_ON_ERROR("There is more than one possible address. It is not yet implemented.");
    SYSTEM_ERROR_CHECK(socket_fd = socket(results->ai_family, results->ai_socktype, results->ai_protocol), -1, NULL); //We suppose there is only one result
    memcpy(&socket_address, results->ai_addr, results->ai_addrlen);
    freeaddrinfo(results);
    
    CLEAR(msghdr);
    msghdr.msg_name = &socket_address;
    msghdr.msg_namelen = socket_address_len;
    msghdr.msg_iov = (struct iovec*)&mem;
    msghdr.msg_iovlen = 1;
}

void blc_server::start(char const *port_name, int mode, type_blc_network_callback on_receive_data, void *arg)
{
    callback = on_receive_data;
    callback_arg = arg;
    
    init(NULL, port_name, mode); 
    SYSTEM_ERROR_CHECK(bind(socket_fd, (struct sockaddr*)&socket_address, socket_address_len), -1, "Socket %d", socket_fd);
    running=1;
    SYSTEM_SUCCESS_CHECK(pthread_create(&thread, NULL, server_manager, (void*)this), 0, "Starting server");
}

void blc_server::allocate_mem_and_start(char const *port_name, int mode, type_blc_network_callback on_receive_data, void *arg, int size)
{
    mem.allocate(size);
    start(port_name, mode, on_receive_data, arg);
}

int set_iovec(struct iovec *iov, void *data, size_t size, ...)
{
    int len;
    va_list arguments;
    
    va_start(arguments, size);
    
    for(len=0; data != NULL; len++)
    {
        iov[len].iov_base = data;
        iov[len].iov_len = size;
        data = va_arg(arguments, void*);
        size = va_arg(arguments, size_t);
    }
    va_end(arguments);
    return len;
}


/* Network */
START_EXTERN_C
void blc_network_init(blc_network *network, char const *address_name, char const *port_name, int mode)
{
    network->init(address_name, port_name, mode);
}

void blc_server_start(blc_server *server, char const *port_name, int mode, type_blc_network_callback on_receive_data, void *arg)
{
    server->start(port_name, mode, on_receive_data, arg);
}

void blc_server_allocate_mem_and_start(blc_server *server, char const *port_name, int mode, type_blc_network_callback on_receive_data, void *arg, int size)
{
    server->allocate_mem_and_start(port_name, mode, on_receive_data, arg, size);
}
END_EXTERN_C



