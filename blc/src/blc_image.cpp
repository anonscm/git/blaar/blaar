/*
 Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)
 
 Author:  Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. *
 
 *  Created on: August 1, 2015
 *      Author: Arnaud Blanchard
 */

#include "blc_image.h"
#include "blc.h"
#include "tools/jpeg_tools.h"

typedef struct {
    uchar r,g,b;
}type_RGB;

static type_RGB *RGB_from_YUYV = NULL;

static void* init_RGB_from_YUYV()
{
    int Y, Cb, Cr;
    int i, j;
    float G_tmp;
    static uchar  R[256], B;
    
    if (RGB_from_YUYV == NULL)
    {
        RGB_from_YUYV= MANY_ALLOCATIONS(256*256*256, type_RGB);
        FOR_INV(Y, 256)
        {
            FOR_INV(j,256) R[j]= CLIP_UCHAR(Y+1.13983*(j-128)); //It does not depend on Cb
            FOR_INV(Cb, 256)
            {
                B =  CLIP_UCHAR(Y+2.03211*(Cb-128)); // It does not depend on Cr
                G_tmp = - 0.58060*(Cb-128);
                
                FOR_INV(Cr, 256)
                {
                    i = Y + (Cb << 8) + (Cr << 16);
                    
                    // Version Wikipedia
                    RGB_from_YUYV[i].r = R[Cr];
                    RGB_from_YUYV[i].b = (uchar)CLIP_UCHAR(Y-0.39465*(Cr-128) + G_tmp);
                    RGB_from_YUYV[i].b = B;
                }
            }
        }
    }
    return NULL;
}

void blc_YUYV_to_RGB3(blc_narray *dest, blc_narray const *src)
{
    int i, j;
    uchar *data = (uchar*)src->data;
    type_RGB *dest_data;
    int Y, Cb = 128, Cr = 128;
    
    init_RGB_from_YUYV();
    dest_data = (type_RGB*)dest->data;
    if (dest->size%2) EXIT_ON_ERROR("The output size must be multiple of two but it is %ld ", dest->size);
    for (i=0; i != (int) dest->size;dest_data+=2)
    {
        Y = data[i++];
        Cb = data[i++];
        j = Y + (Cb << 8) + (Cr << 16);
        dest_data[0]=RGB_from_YUYV[j];
        
        Y = data[i++];
        Cr = data[i++];
        j = Y + (Cb << 8) + (Cr << 16);
        dest_data[1]=RGB_from_YUYV[j];
    }
}

void blc_yuv2_to_RGB3(blc_narray *dest, blc_narray const *src)
{
    int i, j;
    uchar *data = (uchar*)src->data;
    int Y, Cb = 128, Cr = 128;
    type_RGB *dest_data= (type_RGB*)dest->data;
    
    init_RGB_from_YUYV();
    if (dest->size%2) EXIT_ON_ERROR("The output size must be multiple of two but it is %ld ", dest->size );
    for (i=0; i != (int) dest->size; dest_data+=2)
    {
        Cb = data[i++];
        Y = data[i++];
        
        j = Y + (Cb << 8) + (Cr << 16);
        dest_data[0]=RGB_from_YUYV[j];
        
        Cr = data[i++];
        Y = data[i++];
        
        j = Y + (Cb << 8) + (Cr << 16);
        dest_data[1]=RGB_from_YUYV[j];
    }
}





/*
 blc_image_info::blc_image_info(uint32_t format, int width, int height):format(format), width(width), height(height)
 {
 bytes_per_pixel = blc_get_bytes_per_pixel(format);
 pixels_nb=width*height;
 }*/

void blc_image_copy(blc_narray *output, blc_narray const *input)
{
    int i;
    uint32_t output_format_str, input_format_str;
    
    if (input->format == output->format) memcpy(output->data,  input->data, output->size);
    else
    {
        switch(input->format){
            case 'yuv2':
                switch (output->format){
                    case 'Y800': FOR_INV(i, output->size) output->data[i]=input->data[i*2+1];
                        break;
                    case 'RGB3':blc_yuv2_to_RGB3(output, input);
                        break;
                    default:
                        EXIT_ON_ERROR("Output format '%.4s' is not managed with input format '%.4s'.", UINT32_TO_STRING(output_format_str, output->format), UINT32_TO_STRING(input_format_str, input->format));
                }
                break;
            case 'YUYV':
                switch (output->format){
                    case 'Y800': FOR_INV(i, output->size) output->data[i]=input->data[i*2];
                        break;
                    case 'RGB3':blc_YUYV_to_RGB3(output, input);
                        break;
                    default:
                        EXIT_ON_ERROR("Output format '%.4s' is not managed with input format '%.4s'.", UINT32_TO_STRING(output_format_str, output->format), UINT32_TO_STRING(input_format_str, input->format));
                }
                break;
            case 'Y800':
                switch (output->format){
                        case 'JPEG': blc_jpeg_write(output, input);
                        break;
                        
                    default:
                        EXIT_ON_ERROR("Output format '%.4s' is not managed with input format '%.4s'.", UINT32_TO_STRING(output_format_str, output->format), UINT32_TO_STRING(input_format_str, input->format));
                }
                break;
            case 'JPEG':
                switch (output->format){
                    case 'Y800':blc_jpeg_read(output, input);
                        break;
                    case 'RGB3':blc_jpeg_read(output, input);
                        break;
                    case 'RGB4':blc_jpeg_to_RGB4(output, input);
                        break;
                    default:
                        EXIT_ON_ERROR("Output format '%.4s' is not managed with input format '%.4s'.", UINT32_TO_STRING(output_format_str, output->format), UINT32_TO_STRING(input_format_str, input->format));
                }
                break;
            default:  EXIT_ON_ERROR("Input format '%.4s' is not managed", UINT32_TO_STRING(input_format_str, input->format));
        }
    }
}




