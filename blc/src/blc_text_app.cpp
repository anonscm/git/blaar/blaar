/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)
 
 Author:  Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

#include "blc_text_app.h"

#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <termios.h>
#include <sys/select.h>
#include <getopt.h>
#include <signal.h>
#include <errno.h> //errno and EINT
#include <libgen.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <getopt.h>

#include "blc.h"

//Identical to argparse in Python
#define POSITIONAL_ARGUMENTS_TITLE "\npositional arguments:\n"
#define OPTIONAL_ARGUMENTS_TITLE "\noptional arguments:\n"

enum {RUN, PAUSE};

typedef void*(*type_pthread_cb)(void*);

typedef struct
{
    const char *name;
    type_blc_command_cb callback;
    const char *prompt;
    const char *help;
    void *user_data;
}blc_command;

//typedef type_program_option_result;
enum {STRING, BLC_CHANNEL, INPUT_PIPE, OUTPUT_PIPE};

// This is a bit redundant with struct option but has we need to send an array of struct option to getopt_long, we need two independant arrays: one with struct option and one with type_program_option.
typedef struct {
    int type;
    union{
        blc_channel *channel;
        FILE *pipe;
        char const **string_pt;
    };
    char letter;
    char const *name;
    char const *parameter;
    char const *help;
    char const *default_value;
}type_program_option;

struct program_parameter{
    int type;
    union{
        blc_channel *channel;
        FILE *pipe;
        char const **string_pt;
    };
    char const *name;
    int required_nb;
    char const *help;
};


// Static means it is only existing in this file
static char const *program_description=NULL;
static blc_command *blc_commands = NULL;
static int blc_commands_nb = 0;
static type_program_option *blc_program_options=NULL;
static int blc_program_options_nb=0;
static struct program_parameter *blc_program_parameters=NULL;
static int blc_program_parameters_nb;
static struct timeval timer;
static int iterations_nb=0;
static uint blc_period=0;
static uint blc_duration=0;
static int input_terminal, output_terminal;
static int status=RUN;
static long blc_tmp_duration;


START_EXTERN_C
void blc_remove_spaces(char const **text)
{
    while ((*text)[0]==' ') (*text)++;
}

static void free_optional_argument(struct blc_optional_argument *argument)
{
    if (argument->shortname) FREE(argument->shortname);
    if (argument->longname) FREE(argument->longname);
    if (argument->help) FREE(argument->help);
}


char *blc_parse_description(char const *str, char const **optional_argument_description, char const **positional_argument_description)
{
    char const *pt;
    char *description;
    char const *end_description;
    
    pt=strstr(str, POSITIONAL_ARGUMENTS_TITLE);
    if (pt){
        end_description=pt;
        if (positional_argument_description) *positional_argument_description=pt+strlen(POSITIONAL_ARGUMENTS_TITLE);
        pt=strstr(pt, OPTIONAL_ARGUMENTS_TITLE);
    }
    else {
        if (positional_argument_description) *positional_argument_description=NULL;
        pt=strstr(str, OPTIONAL_ARGUMENTS_TITLE);
        end_description=pt;
    }
    if (pt){
        if (optional_argument_description) *optional_argument_description=pt+strlen(OPTIONAL_ARGUMENTS_TITLE);
    }
    else
    {
        if (optional_argument_description) *optional_argument_description=NULL;
    }
    
    
    if (end_description==NULL) description=strdup(str);
    else description=strndup(str, end_description-str);
    return description;
}

int blc_parse_optional_arguments(struct blc_optional_argument **results, char const **pos, char const *optional_arguments_description)
{
    char const*help;
    char token[NAME_MAX+1];
    char text[NAME_MAX+1];
    int token_size;
    int arg=1;
    int results_nb=0;
    struct blc_optional_argument argument;
    
    *results=NULL;
    
    while(arg)
    {
        help=NULL;
        blc_remove_spaces(pos);
        if (sscanf(*pos, "[--%[^] ]%n", token, &token_size)==1){
            *pos+=token_size;
            SPRINTF(text, "--%s", token);
            argument.shortname=NULL;
            argument.longname=strdup(token);
            help=strstr(optional_arguments_description, text);
        }
        else if (sscanf(*pos, "[-%[^] ]%n", token, &token_size)==1){
            *pos+=token_size;
            SPRINTF(text, "-%s", token);
            argument.shortname=strdup(token);
            if (optional_arguments_description)
            {
                help=strstr(optional_arguments_description, text);
                if (help) {
                    help+=strlen(text);
                    sscanf(help, ", --%s%n", token, &token_size);
                    help+=token_size;
                }
            }
            argument.longname=strdup(token);
            
        }else break;
        token_size=0;
        sscanf(*pos, "%*[= ]%n", &token_size); //remove spaces or equal
        *pos+=token_size;
        if (sscanf(*pos, "%[^] ]%n", token, &token_size)==1) {
            *pos+=token_size;
            argument.value=strdup(token);  //FIXME: Memory may not be free
        }
        else  argument.value=NULL;
        
        if (help){
            help+=strlen(text);
            blc_remove_spaces(&help);
            if (argument.value) {
                help+=strlen(argument.value);
                blc_remove_spaces(&help);
            }
            argument.help=strndup(help, strchr(help, '\n') - help);
        }
        else argument.help=NULL;
        
        if (argument.longname==NULL || strcmp(argument.longname, "help")!=0) APPEND_ITEM(results, &results_nb, &argument);
        else free_optional_argument(&argument);
        
        token_size=0;
        sscanf(*pos, "%*[] ]%n", &token_size);
        *pos+=token_size;
    }
    return results_nb;
}

int blc_parse_positional_arguments(struct blc_positional_argument **results, char const **pos, char const *positional_arguments_description)
{
    char const *help;
    char token[NAME_MAX+1];
    int token_size;
    int results_nb=0;
    struct blc_positional_argument argument;
    
    *results=NULL;
    while (sscanf(*pos, "%[^\n ]%n", token, &token_size)==1)
    {
        *pos+=token_size;
        argument.name=strdup(token);
        argument.value=NULL;
        help=strstr(positional_arguments_description, token);
        if (help)
        {
            help+=strlen(token);
            blc_remove_spaces(&help);
            argument.help=strndup(help, strchr(help, '\n') - help);
        }
        else argument.help=NULL;
        APPEND_ITEM(results, &results_nb, &argument);
    }
    
    while (sscanf(*pos, "[%s]%n", token, &token_size)==1)
    {
        *pos+=token_size;
        argument.name=strdup(token);
        argument.value=NULL;
        help=strstr(positional_arguments_description, token) + strlen(token);
        while(help[0]==' ') help++;
        argument.help=strndup(help, strchr(help, '\n') - help);
        APPEND_ITEM(results, &results_nb, &argument);
    }
    return results_nb;
}

char *blc_get_help(char const *path)
{
    blc_mem mem;
    char buffer[LINE_MAX];
    int stderr_pipe_fds[2], stdout_pipe_fds[2];
    pid_t pid;
    ssize_t n;
    
    SYSTEM_ERROR_CHECK(pipe(stderr_pipe_fds), -1, NULL);
    SYSTEM_ERROR_CHECK(pipe(stdout_pipe_fds), -1, NULL);
    pid=fork();
    if (pid==0){
        SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(stderr_pipe_fds[1], STDERR_FILENO), -1, EINTR, "duplicating stderr");
        SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(stdout_pipe_fds[1], STDOUT_FILENO), -1, EINTR, "duplicating stdout");
        blc_close_pipe(stderr_pipe_fds);
        blc_close_pipe(stdout_pipe_fds);
        SYSTEM_ERROR_CHECK(execl(path, basename((char*)path), "--help", NULL), -1, "Executing '%s'.", path);
    }
    else{
        close(stdout_pipe_fds[1]);
        do{
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(n=read(stdout_pipe_fds[0], buffer, sizeof(buffer)), -1, EINTR, "Reading pipe of function '%s'.", path);
            mem.append(buffer, n);
        }while(n!=0);
        close(stdout_pipe_fds[0]);
        close(stderr_pipe_fds[1]);
        do {
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(n=read(stderr_pipe_fds[0], buffer, sizeof(buffer)), -1, EINTR, "Reading pipe of function '%s'.", path);
            mem.append(buffer, n);
        }while(n!=0);
        close(stderr_pipe_fds[0]);
        waitpid(pid, NULL, 0);
        mem.append("", 1); //end of string
    }
    return mem.data;
}

char const *blc_parse_help(char const *path, blc_optional_argument **optional_arguments, int *optional_arguments_nb, blc_positional_argument **positional_arguments, int *positional_arguments_nb, char **epilog)
{
    char const *str;
    char token[LINE_MAX];
    char  *help;
    char const *description;
    char const *description_start;
    const char *positional_argument_description;
    const char *optional_argument_description;
    (void)epilog;
    
    help=blc_get_help(path);
    
    SPRINTF(token, "usage: %s", basename((char*)path));
    str=strstr(help, token)+strlen(token);
    description_start=strchr(str, '\n')+1;
    description=blc_parse_description(description_start, &optional_argument_description, &positional_argument_description);
    *optional_arguments_nb=blc_parse_optional_arguments(optional_arguments, &str, optional_argument_description);
    *positional_arguments_nb=blc_parse_positional_arguments(positional_arguments, &str, positional_argument_description);
    
    free(help);
    return description;
}


void blc_command_add(const char *command_name, type_blc_command_cb callback, const char *prompt, const char *help, void *user_data)
{
    blc_command tmp_command;
    tmp_command.name = command_name;
    tmp_command.callback = callback;
    tmp_command.prompt = prompt;
    tmp_command.help = help;
    tmp_command.user_data = user_data;
    
    APPEND_ITEM(&blc_commands, &blc_commands_nb, &tmp_command);
}

/**Wait an input in the terminal. It match the blc_commands, if possible it executes it and return the pointer result..
 * If a command is recognise but a parameter is missing (promt not NULL), asks for more input.
 */
void blc_command_interpret()
{
    blc_mem line, parameter;
    size_t line_capability=0;
    ssize_t tmp_size;
    const blc_command *command;
    int name_size;
    
    do
    {
        tmp_size = getline(&line.data, &line_capability, stdin);
    }while ((tmp_size==-1) && (errno==EINTR));
    
    if (tmp_size==-1)
    {
        if (errno==0) exit(0); //stdin closed
        else EXIT_ON_SYSTEM_ERROR("Command interpret: getline");
    }
    
    line.size=tmp_size-1;
    line.data[line.size]=0; // Removing the last return char (\n)
    
    FOR_EACH_INV(command, blc_commands, blc_commands_nb)
    {
        name_size = strlen(command->name);
        if (strncmp(command->name, line.data, name_size) == 0)
        {
            parameter.data = line.data + name_size;
            parameter.size = line.size - name_size;
            
            if (parameter.size == 0) //No text after the command
            {
                parameter.data = NULL;
                if (command->prompt != NULL) //A text was expected
                {
                    fprintf(stderr, "%s:\n", command->prompt);
                    parameter.size = getline(&parameter.data, &line_capability, stdin)-1;
                    parameter.data[parameter.size]=0;
                }
                command->callback(command->name, parameter.data, command->user_data);
                FREE(parameter.data);
                line_capability=0;
                break;
            }
            else
            {
                if (command->prompt == NULL) continue; // If we do not wait for parameter, the command must be exact.
                else command->callback(command->name, parameter.data, command->user_data);
                break;
            }
        }
    }
    if (command < blc_commands) fprintf(stderr, "Unknown command in: %s\n", line.data);
}

int blc_command_try_to_interpret()
{
    fd_set rfds;
    struct timeval time_val={0,0};
    int retval;
    
    /* Surveiller stdin (fd 0) en attente d'entrées */
    FD_ZERO(&rfds);
    FD_SET(0, &rfds);
    
    SYSTEM_ERROR_CHECK(retval = select(1, &rfds, NULL, NULL, &time_val), -1, NULL);
    
    if (retval) blc_command_interpret();
    else return 0;
    return 1;
}

void blc_fprint_stats(FILE *file)
{
    fprintf(file, "%d iterations, average duration: %.3fms, average frequency: %.3fHz", iterations_nb, blc_duration/(1000.f*iterations_nb), 1000000*iterations_nb/(float)blc_period);
    iterations_nb=0;
}

static void pause_cb()
{
    if (status==RUN){
        status=PAUSE;
        fprintf(stderr, "=== %s: pause ===\n", blc_program_name);
        blc_command_interpret();
    }else {
        fprintf(stderr, "=== %s: continue ===\n", blc_program_name);
        status=RUN;
    }
}

static void display_stats()
{
    blc_fprint_stats(stderr);
}

void blc_program_loop_init()
{
    iterations_nb=0;
    blc_command_add("", (type_blc_command_cb)pause_cb, NULL, "set on/off pause", NULL);
    blc_command_add("h", (type_blc_command_cb)blc_command_display_help, NULL, "display this help", NULL);
    blc_command_add("q", (type_blc_command_cb)exit, NULL, "quit the application", NULL);
    blc_command_add("s", (type_blc_command_cb)display_stats, NULL, "display time stats", NULL);
    
    
    blc_command_display_help();
    fprintf(stderr, "=== %s: start ===\n", blc_program_name);
}

int blc_program_loop_start()
{
    int choice=-2;
    
    if (input_terminal) blc_command_try_to_interpret();
    else
    {
        switch(choice=getchar())
        {
            case '.':break;
            case 'q':
                if (!output_terminal) printf("q");
                blc_fprint_stats(stderr);
                return 0;
                break;
        }
    }
    
    if (iterations_nb) blc_period+=blc_tmp_duration+us_time_diff(&timer);
    else {
        blc_period=0;
        blc_duration=0;
        us_time_diff(&timer);
    }
    
    return choice;
}

void blc_program_loop_end()
{
    blc_tmp_duration=us_time_diff(&timer);
    blc_duration+=blc_tmp_duration;
    if (blc_stderr_ansi) fprintf(stderr, "\r%.3fms >", blc_tmp_duration/1000.f);
    else fputc('.', stderr);
    iterations_nb++;
}

void blc_command_display_help()
{
    const blc_command *command;
    size_t command_length_max = 0;
    size_t command_length=0;
    
    FOR_EACH(command, blc_commands, blc_commands_nb)
    {
        command_length = strlen(command->name);
        if (command->prompt) command_length+=strlen(command->prompt)+2;
        command_length_max = MAX(command_length, command_length_max);
    }
    
    FOR_EACH(command, blc_commands, blc_commands_nb)
    {
        if (command->prompt) fprintf(stderr, "%s<%s>%*c:%s\n", command->name, command->prompt, (int)(command_length_max - strlen(command->name)-strlen(command->prompt)-1), ' ', command->help);
        else    fprintf(stderr, "%-*s :%s\n", (int)command_length_max, command->name, command->help);
    }
}

void *blc_command_interpret_loop(char const *option)
{
    if (option)
    {
        if (strcmp(option,"h")==0)
        {
            while(1)
            {
                blc_command_display_help();
                fprintf(stderr, ">");
                blc_command_interpret();
            }
        }
        else EXIT_ON_ERROR("Unknown option '%s'", option);
        
    }
    else while(1) blc_command_interpret();
    return NULL;
}

void blc_command_interpret_thread(char const *option)
{
    pthread_t thread;
    SYSTEM_ERROR_CHECK(pthread_create(&thread, NULL, (type_pthread_cb)blc_command_interpret_loop, (void*)option), -1, NULL);
    
}

void add_arg( int *argc, char ***argv, char const *arg, char const *value)
{
    APPEND_ITEM(argv, argc, &arg);
    if (value!=NULL)   APPEND_ITEM(argv, argc, &value);
}

char *create_command_line_from_argv(char const *const *argv){
    int command_line_size, i;
    char *command_line, *pos;
    
    command_line_size=0;
    for(i=0; argv[i] != NULL; i++) command_line_size+=strlen(argv[i])+1; // + 1 for the space
    
    command_line=MANY_ALLOCATIONS(command_line_size+1, char);
    pos=command_line;
    for(i=0; argv[i] != NULL; i++)
        pos+=sprintf(pos, "%s ", argv[i]);
    return command_line;
}

char * const*create_argv_from_command_line(char const *command_line)
{
    char * const*argv=NULL;
    char *arg_pt;
    char const *pos;
    char tmp_arg[NAME_MAX+1];
    int length, argc=0;
    
    pos=command_line;
    while(sscanf(pos, SCAN_CONV(NAME_MAX, "s")"%n", tmp_arg, &length)==1)
    {
        arg_pt = strdup(tmp_arg);
        APPEND_ITEM(&argv, &argc, &arg_pt);
        pos+=length;
    }
    arg_pt=NULL;
    APPEND_ITEM(&argv, &argc, &arg_pt);
    return argv;
}

void blc_program_set_description(char const *description)
{
    program_description=strdup(description);
}

void blc_program_add_parameter(char const **result, char const *name, int required_nb, char const *help)
{
    struct program_parameter parameter;
    
    parameter.type=STRING;
    parameter.string_pt=result;
    parameter.name=name;
    parameter.required_nb=required_nb;
    parameter.help=help;
    APPEND_ITEM(&blc_program_parameters, &blc_program_parameters_nb, &parameter);
}

void blc_program_add_channel_parameter(blc_channel *channel, int required_nb, char const *help)
{
    struct program_parameter parameter;
    
    parameter.type=BLC_CHANNEL;
    parameter.channel=channel;
    parameter.name="blc_channel";
    parameter.required_nb=required_nb;
    parameter.help=help;
    APPEND_ITEM(&blc_program_parameters, &blc_program_parameters_nb, &parameter);
}

void blc_program_option_add( char const **result, char letter, char const *long_option, char const *parameter, const char *help, char const* default_value)
{
    type_program_option  tmp_program_option;
    tmp_program_option.type = STRING;
    tmp_program_option.string_pt =  result;
    *tmp_program_option.string_pt=NULL;
    tmp_program_option.letter = letter;
    tmp_program_option.name = long_option;
    tmp_program_option.parameter = parameter;
    tmp_program_option.help = help;
    tmp_program_option.default_value = default_value;
    APPEND_ITEM(&blc_program_options, &blc_program_options_nb, &tmp_program_option);
}

void blc_program_add_channel_option(blc_channel *channel, char letter, char const *long_option, char const *help, char const* default_value)
{
    type_program_option  tmp_program_option;
    
    tmp_program_option.type = BLC_CHANNEL;
    tmp_program_option.channel = channel;
    tmp_program_option.letter = letter;
    tmp_program_option.name = long_option;
    tmp_program_option.parameter = "blc_channel";
    tmp_program_option.help = help;
    tmp_program_option.default_value=default_value;
    APPEND_ITEM(&blc_program_options, &blc_program_options_nb, &tmp_program_option);
}

void blc_program_add_input_pipe_option(FILE *file, char letter, char const *long_option, char const *help, char const* default_value)
{
    type_program_option  tmp_program_option;
    
    tmp_program_option.type=INPUT_PIPE;
    tmp_program_option.pipe = file;
    tmp_program_option.letter = letter;
    tmp_program_option.name = long_option;
    tmp_program_option.parameter = "input_pipe";
    tmp_program_option.help = help;
    tmp_program_option.default_value=default_value;
    APPEND_ITEM(&blc_program_options, &blc_program_options_nb, &tmp_program_option);
}

void blc_program_add_output_pipe_option(FILE *file, char letter, char const *long_option, char const *help, char const* default_value)
{
    type_program_option  tmp_program_option;
    
    tmp_program_option.type=OUTPUT_PIPE;
    tmp_program_option.pipe = file;
    tmp_program_option.letter = letter;
    tmp_program_option.name = long_option;
    tmp_program_option.parameter = "output_pipe";
    tmp_program_option.help = help;
    tmp_program_option.default_value=default_value;
    APPEND_ITEM(&blc_program_options, &blc_program_options_nb, &tmp_program_option);
}

void blc_program_option_interpret(int *argc, char **argv[])
{
    char *optstring;
    int ret, option_index;
    int optstring_size=0;
    int options_nb=0;
    int i;
    struct option *long_options = NULL, tmp_option;
    struct stat stat_data;
    type_program_option *program_option;
    char pipe_name[PATH_MAX+1];
    
    //We store enough room for all the optional letters + ':' + terminating null char
    optstring = MANY_ALLOCATIONS(blc_program_options_nb*2+1, char);
    
    //We define this list and initialise the result
    FOR_EACH_INV(program_option, blc_program_options, blc_program_options_nb)
    {
        optstring[optstring_size++]=program_option->letter;
        tmp_option.name = (const char*)program_option->name;
        if (program_option->parameter) tmp_option.has_arg=required_argument;
        else tmp_option.has_arg=no_argument;
        if (tmp_option.has_arg !=0) optstring[optstring_size++]=':';
        tmp_option.flag = NULL;
        tmp_option.val=program_option->letter;
        APPEND_ITEM(&long_options, &options_nb, &tmp_option);
    }
    // We stop the string and option long_options array by NULL values */
    optstring[optstring_size++]='\0';
    CLEAR(tmp_option);
    APPEND_ITEM(&long_options, &options_nb, &tmp_option);
    
    //We parse the command line
    while ((ret = getopt_long(*argc, *argv, optstring, long_options, &option_index)) != -1)
    {
        //Wrong option
        if (ret=='?')
        {
            color_fprintf( BLC_RED, stderr, "The possibilities are:\n");
            blc_program_option_display_help();
            EXIT_ON_ERROR("Invalid program argument '%s'.", long_options[option_index].name);
        }
        
        FOR_EACH_INV(program_option, blc_program_options, blc_program_options_nb)
        {
            if (ret == program_option->letter) //TODO see what to do with special char '?' and ':'
            {
                if (optarg == NULL) *program_option->string_pt="1";
                else{
                    switch (program_option->type){
                        case BLC_CHANNEL:
                            program_option->channel->set_namef(optarg);
                            break;
                        case STRING:
                            *program_option->string_pt = optarg;
                            break;
                        case INPUT_PIPE:
                            SPRINTF(pipe_name, "/tmp/blc_pipes/%s", optarg);
                            freopen(optarg, "r", program_option->pipe);
                            break;
                        case OUTPUT_PIPE:
                            mkdir("/tmp/blc_pipes", S_IRWXU);
                            SPRINTF(pipe_name, "/tmp/blc_pipes/%s", optarg);
                            if (mkfifo(pipe_name, S_IRWXU) !=0)
                            {
                                if (errno==EEXIST)
                                {
                                    SYSTEM_SUCCESS_CHECK(stat(pipe_name, &stat_data), 0, "Checking data of '%s'.", pipe_name);
                                    if (stat_data.st_mode == S_IFIFO){
                                        freopen(pipe_name, "w", program_option->pipe);
                                    }
                                    else EXIT_ON_ERROR("Cannot use '%s' as a output pipe.", pipe_name);
                                }
                                else EXIT_ON_SYSTEM_ERROR("Creating output pipe '%s'.", pipe_name);
                            }
                            break;
                        default:EXIT_ON_ERROR("Type %d not managed.", program_option->type);
                    }
                }
            }
        }
    }
    FREE(long_options);
    
    //Set default values
    FOR_EACH_INV(program_option, blc_program_options, blc_program_options_nb)
    {
        switch (program_option->type){
            case BLC_CHANNEL:
                if (strlen(program_option->channel->name)==0)
                {
                    if(program_option->default_value)  program_option->channel->set_namef("%s%d", program_option->default_value, getpid());
                }
                break;
            case STRING:
                if (*program_option->string_pt==NULL) *program_option->string_pt=program_option->default_value;
                break;
        }
    }
    
    *argc -= optind;
    *argv += optind;
    
    FOR(i, blc_program_parameters_nb)
    {
        if (i==*argc){//There is no more argument to interpret
            if (blc_program_parameters[i].required_nb){
                if (blc_program_parameters[i].type==BLC_CHANNEL){
                    if (isatty(STDIN_FILENO)) fprintf(stderr, "%s: %s? ", blc_program_parameters[i].help, blc_program_parameters[i].name);
                    SYSTEM_ERROR_CHECK(fgets(blc_program_parameters[i].channel->name, sizeof(blc_program_parameters[i].channel->name), stdin), NULL, "Wrong channel input name");
                    blc_program_parameters[i].channel->name[strlen(blc_program_parameters[i].channel->name)-1]=0;
                }else EXIT_ON_ERROR("Missing %s: %s", blc_program_parameters[i].name, blc_program_parameters[i].help);
            }
            else break;
        }
        else{
            if (blc_program_parameters[i].channel) blc_program_parameters[i].channel->set_namef((*argv)[i]);
            else *blc_program_parameters[i].string_pt=(*argv)[i];
        }
    }
    
    *argc += optind;
    *argv -= optind;
}

void blc_program_option_interpret_and_print_title(int *argc, char ***argv)
{
    blc_program_option_interpret(argc, argv);
    underline_fprintf('=', stderr, blc_program_name);
}

void blc_program_init(int *argc, char ***argv, void (*exit_cb)(void))
{
    char const *help;

    input_terminal=isatty(STDIN_FILENO);
    output_terminal=isatty(STDOUT_FILENO);

    if (isatty(STDERR_FILENO) && input_terminal) {
        blc_stderr_ansi_detect();
        if (!blc_stderr_ansi) fprintf(stderr, "No ansi terminal. There is no color.");
    }
    
    blc_program_name = basename(*argv[0]);
    
    blc_program_option_add(&help, 'h', "help", 0, "display this help", NULL);
    blc_program_option_interpret_and_print_title(argc, argv);
    
    if (help)
    {
        blc_program_option_display_help();
        exit(0);
    }
    
    atexit(blc_quit);
    if (exit_cb) atexit(exit_cb);
}

/**Diplay format as python argparse*/
void blc_program_option_display_help()
{
    struct program_parameter *parameter;
    int i, option_length_max, tmp_length;
    type_program_option *program_option;
    
    //conventions want the help on stderr (avoid to interfere with normal output)
    fprintf(stderr, "\nusage: %s", blc_program_name);
    option_length_max=0;
    
    FOR_EACH(program_option, blc_program_options, blc_program_options_nb)
    {
        if (program_option->letter)
        {
            fprintf(stderr, " [-%c", program_option->letter);
            tmp_length=3;
            if (program_option->parameter){
                fprintf(stderr, " %s", program_option->parameter);
                tmp_length+=strlen(program_option->parameter);
            }
            fprintf(stderr, "]");
        }
        option_length_max=MAX(option_length_max, (int)strlen(program_option->name)+tmp_length);
    }
    
    FOR_EACH(parameter, blc_program_parameters, blc_program_parameters_nb)
    {
        if (parameter->required_nb==0) fprintf(stderr, " [%s]", parameter->name);
        else if (parameter->required_nb==-1) fprintf(stderr, " %s ...", parameter->name);
        else if (parameter->required_nb==1) fprintf(stderr, " %s", parameter->name);
        else FOR(i, parameter->required_nb) fprintf(stderr, " %s%d", parameter->name, i+1);
        
        option_length_max=MAX(option_length_max, (int)strlen(parameter->name));
    }
    fprintf(stderr, "\n");
    
    if (program_description) fprintf(stderr, "\n%s\n", program_description);
    
    option_length_max+=10;
    if (blc_program_parameters_nb) fprintf(stderr, POSITIONAL_ARGUMENTS_TITLE);
    FOR_EACH(parameter, blc_program_parameters, blc_program_parameters_nb)
    {
        fprintf(stderr, "  %-*s %s\n", option_length_max-2, parameter->name, parameter->help);
    }
    
    fprintf(stderr, OPTIONAL_ARGUMENTS_TITLE);
    FOR_EACH(program_option, blc_program_options, blc_program_options_nb)
    {
        if (program_option->letter) {
            tmp_length=fprintf(stderr, " -%c", program_option->letter);
            if ( program_option->name) tmp_length+=fprintf(stderr, ",");
        }
        else tmp_length=0;
        if (program_option->name) tmp_length+=fprintf(stderr, " --%s", program_option->name);
        if (program_option->parameter) tmp_length+=fprintf(stderr, " %s", program_option->parameter);
        fprintf(stderr, "%*c", option_length_max - tmp_length, ' ');
        fprintf(stderr, "%s", program_option->help);
        if (program_option->default_value) {
            if (program_option->type==BLC_CHANNEL) fprintf(stderr, " (default: %s<pid>)\n", program_option->default_value);
            else fprintf(stderr, " (default: %s)\n", program_option->default_value);
        }
        else fprintf(stderr, "\n");
    }
}

void blc_quit()
{
    fprintf(stderr, "Quitting %s\n", blc_program_name);
    if (output_terminal==0){
       printf("q");
       fflush(stdout);
    }
    exit(0);
}

void blc_close_pipe(int *pipe)
{
    SYSTEM_ERROR_CHECK(close(pipe[0]), -1, "Closing pipe 0");
    SYSTEM_ERROR_CHECK(close(pipe[1]), -1, "Closing pipe 1");
}
END_EXTERN_C
