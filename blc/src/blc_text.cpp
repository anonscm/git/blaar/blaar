/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)
 
 Author:  Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

#include "blc_text.h"

#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <termios.h>
#include <sys/select.h>
#include <getopt.h>
#include <signal.h>
#include <errno.h> //errno and EINT
#include <libgen.h>

#include "blc.h"

static void (*resize_callback)(int columns_nb, int rows_nb);
uchar blc_bar_colors[BLC_BAR_COLORS_NB] = { BLC_BLUE, BLC_BRIGHT_BLUE, BLC_CYAN, BLC_BRIGHT_CYAN, BLC_GREEN, BLC_BRIGHT_GREEN, BLC_BRIGHT_YELLOW, BLC_YELLOW,   BLC_BRIGHT_MAGENTA, BLC_MAGENTA, BLC_BRIGHT_RED, BLC_RED};

int blc_stdout_ansi = 1;
int blc_stderr_ansi = 1;

void signal_callback(int signal_id)
{
    int columns_nb, rows_nb;
    switch (signal_id)
    {
        case SIGWINCH:
            blc_terminal_get_size(&columns_nb, &rows_nb);
            resize_callback(columns_nb, rows_nb);
            break;
    }
}


START_EXTERN_C
/**It can also be used to detect if the terminal is ANSI as it fails if it is not. The funciton send a request and wait an arbitrary milisecond for an answer. If there is not, we consider it is not an ANSI terminal but it may be because the systeme is too slow and busy.
@param[out] x,y: pointer to the coordinates to be returned.
@return 1 in case of success, 0 otherwise.*/
int blc_fterminal_try_to_get_cursor_position(FILE *file, int* x, int *y)
{
    fd_set readset;
    int success = 0;
    struct timeval time;
    struct termios term, initial_term;
    
    //We store the actual properties of the input terminal and set it as no buffered (~ICANON) and no echoing (~ECHO)
    tcgetattr(STDIN_FILENO, &initial_term);
    term = initial_term;
    term.c_lflag &=~ICANON;
    term.c_lflag &=~ECHO;
    tcsetattr(STDIN_FILENO, TCSANOW, &term);
    
    //We request position
    fprint_escape_command(file, "6n");
    fflush(file);
    
    //We wait 300ms for a terminal answer
    FD_ZERO(&readset);
    FD_SET(STDIN_FILENO, &readset);
    time.tv_sec = 0;
    time.tv_usec = 300000;
    
    //If it success we try to read the cursor value
    if (select(STDIN_FILENO + 1, &readset, NULL, NULL, &time) == 1) if (scanf("\033[%d;%dR", x, y) == 2) success = 1;
    
    //We set back the properties of the terminal
    tcsetattr(STDIN_FILENO, TCSADRAIN, &initial_term);
    
    return success;
}
///Use terminal_try_to_get_cursor_position to see if the terminal has this ANSI capability. The ncurses lib use a list of compatible terminal but it seems to heavy to use.
int blc_stdout_ansi_detect()
{
    int x, y;
    
    //We request the position of the cursor. If we succeed, we consider we are using an ANSI terminal.
    blc_stdout_ansi = blc_fterminal_try_to_get_cursor_position(stdout, &x, &y);
    return blc_stdout_ansi;
}

///Use terminal_try_to_get_cursor_position to see if the terminal has this ANSI capability. The ncurses lib use a list of compatible terminal but it seems to heavy to use.
int blc_stderr_ansi_detect()
{
    int x, y;
    
    //We request the position of the cursor. If we succeed, we consider we are using an ANSI terminal.
    blc_stderr_ansi = blc_fterminal_try_to_get_cursor_position(stderr, &x, &y);
    return blc_stderr_ansi;
}

void blc_terminal_get_size(int *columns, int *lines)
{
    struct winsize size;
    
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &size);
    *columns = size.ws_col;
    *lines = size.ws_row;
}


///A signal SIGWINCH is associated to the callback. It is not yet reversible.
void blc_terminal_set_resize_callback(void (*callback)(int , int ))
{
    struct sigaction signal_action;
    
    resize_callback = callback;
    
    sigemptyset(&signal_action.sa_mask);
    signal_action.sa_flags = 0;
    signal_action.sa_handler = signal_callback;
    SYSTEM_ERROR_CHECK(sigaction(SIGWINCH,  &signal_action, NULL), -1, NULL);
}

void vfprint_escape_command(FILE *file, char const *format, va_list arguments)
{
        fprintf(file, "\x1b[");
        vfprintf(file, format, arguments);
}

void fprint_escape_command(FILE *file, char const *format, ...)
{
    va_list arguments;
    va_start(arguments, format);
    vfprint_escape_command(file, format, arguments);
    va_end(arguments);
}

void fprint_start_color(FILE *file, int color_id)
{
        if (color_id < 0 || color_id >= 16) EXIT_ON_ERROR("Wrong color color_id %d", color_id);
        if (color_id & BLC_BRIGHT) fprintf(file, "\x1b[9%d", color_id &~BLC_BRIGHT);
        else fprintf(file, "\x1b[3%d", color_id &~BLC_BRIGHT);
        fprintf(file, "m");
}

int sprint_start_color(char *string, int color_id)
{
    int pos=0;
        if (color_id < 0 || color_id >= 16) EXIT_ON_ERROR("Wrong color color_id %d", color_id);
        if (color_id & BLC_BRIGHT) pos+=sprintf(string+pos, "\x1b[9%d", color_id &~BLC_BRIGHT);
        else pos+=sprintf(string+pos, "\x1b[3%d", color_id &~BLC_BRIGHT);
        pos+=sprintf(string+pos, "m");
    return pos;
}

void fprint_stop_color(FILE *file)
{
    fprintf(file, "\x1b[0m");
    fflush(file);
}

int sprint_stop_color(char *string)
{
    int pos=0;
    pos+=sprintf(string, "\x1b[0m");
    return pos;
}

void fprint_backgournd_start_color(FILE *file, int color_id)
{

        if (color_id < 0 || color_id >= 16) EXIT_ON_ERROR("Wrong color color_id %d", color_id);
        
        if (color_id & BLC_BRIGHT) fprintf(file, "\x1b[10%d", color_id &~BLC_BRIGHT);
        else fprintf(file, "\x1b[4%d", color_id &~BLC_BRIGHT);
        fprintf(file, "m");
}

void fprint_backgournd_stop_color(FILE *file)
{
    fprintf(file, "\x1b[9m");
    fflush(file);
}

void color_vfprintf(int color_id, FILE *file, char const *format, va_list arguments)
{
    
    if (file!=stderr || blc_stderr_ansi) fprint_start_color(file, color_id);
    vfprintf(file, format, arguments);
    if (file!=stderr || blc_stderr_ansi) fprint_stop_color(file);
}

int color_vsprintf(int color_id, char *string, char const *format, va_list arguments)
{
    int pos=0;
    pos+=sprint_start_color(string, color_id);
    pos+=vsprintf(string+pos, format, arguments);
    pos+=sprint_stop_color(string+pos);
    return pos;
}

void color_fprintf(int color_id, FILE *file, const char *format, ...)
{
    va_list arguments;
    va_start(arguments, format);
    color_vfprintf(color_id, file, format, arguments);
    va_end(arguments);
}

int color_sprintf(int color_id, char *string, const char *format, ...)
{
    int pos=0;
    va_list arguments;
    va_start(arguments, format);
    pos+=color_vsprintf(color_id, string, format, arguments);
    va_end(arguments);
    return pos;
}

void fprint_human_size(FILE *file, size_t size)
{
    typedef struct
    {
        char const* const name;
        size_t value;
    } type_unit;
    
    int  num;
    size_t i;
    type_unit units[]= {{"Gb", 1<<30}, {"Mb", 1<<20}, {"Kb", 1<<10}, {"b", 1}} ;
    
    FOR(i, sizeof(units)/sizeof(type_unit))
    {
        if (size > units[i].value)
        {
            num = size/units[i].value;
            fprintf(file, " %d%s", num, units[i].name);
            size -= num*units[i].value;
        }
    }
}

void underline_fprintf(char c, FILE * file,  char const *format, ...)
{
    
    int i, size;
    
    va_list arguments;
    va_start(arguments, format);
    size = vfprintf(file, format, arguments);
    fputc('\n', file);
    FOR_INV(i, size) fputc(c, file);
    fputc('\n', file);
    va_end(arguments);
}

void fprint_tsv_floats(FILE *file, float *values, int values_nb)
{
    int i;
    FOR(i, values_nb) fprintf(file, "%f\t", values[i]);
    fprintf(file, "\n");
}

void fscan_tsv_floats(FILE *file, float *values, int values_nb)
{
    int i;
    FOR(i, values_nb)
    FSCANF(1, file, "%f\t", &values[i]);
    if (fscanf(file, "\n")!=0) EXIT_ON_ERROR("fscanf");
}

int fprint_fourcc(FILE *file, uint32_t value)
{
    HTONL(value);
    fprintf(file, "%.4s", (char*)&value);
    return 4;
}

char *sprint_fourcc(char *string, uint32_t value)
{
    HTONL(value);
    return string+sprintf(string, "%.4s", (char*)&value);
}

int blc_sscan_dims(int **lengths, char const *str)
{
    int dims_nb=0;
    int length;
    int pos;
    
    *lengths=NULL;
    if (sscanf(str, "%d%n", &length, &pos)==1)
        do
        {
            APPEND_ITEM(lengths, &dims_nb, &length);
            str+=pos;
        }while(sscanf(str, "x%d%n", &length, &pos)==1);
    return dims_nb;
}


END_EXTERN_C
