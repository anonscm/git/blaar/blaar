#ifndef BYTE_DISPLAY_H
#define BYTE_DISPLAY_H

#include <gtk/gtk.h>
#include <blc_channel.h>


GtkWidget *create_byte_display(blc_channel *channel);

#endif