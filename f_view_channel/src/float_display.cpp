#include "float_display.hpp"

#include "main.hpp"

#include <fcntl.h> // O_RDONLY ...
#include <stdio.h>
#include <gtk/gtk.h>
#include <string.h>
#include <stdint.h> //uint32_t
#include <float.h>
//#include <jpeglib.h>
#include <sys/mman.h>
#include <errno.h> //errno
#include "blc.h"



float *values;
int values_nb;

typedef void (*type_update_function)(GtkWidget*, gdouble);

struct update
{
    GtkWidget *main_widget;
    type_update_function function;
    void **values;
    int values_nb;
    GtkWidget **widgets;
    int widgets_nb;
    int g_source_mode;
    uint32_t type;
    
    update(uint32_t type);
    void add(GtkWidget *widget, void *value);
    void update_all();
};

update::update(uint32_t type):type(type), values(NULL), values_nb(0), widgets(NULL), widgets_nb(0), g_source_mode(G_SOURCE_CONTINUE){
}

void update::add(GtkWidget *widget, void *value_pt)
{
    APPEND_ITEM(&values, &values_nb, &value_pt);
    APPEND_ITEM(&widgets, &widgets_nb, &widget);
}

void update::update_all()
{
    int i;
    uint32_t type_tmp;

    switch (type)
    {
        case 'UIN8': FOR_INV(i, values_nb) function(widgets[i], (gdouble)*(uchar*)values[i]);
            break;
        case 'INT8': FOR_INV(i, values_nb) function(widgets[i], (gdouble)*(char*)values[i]);
            break;
        case 'FL32':
            FOR_INV(i, values_nb) function(widgets[i], (gdouble)*(float*)values[i]);
            break;
        case 'IN32':
            FOR_INV(i, values_nb) function(widgets[i], (gdouble)*(int32_t*)values[i]);
            break;
        case 'UI32':
            FOR_INV(i, values_nb) function(widgets[i], (gdouble)*(uint32_t*)values[i]);
            break;
        default:EXIT_ON_ERROR("type '%.4s' is not managed", UINT32_TO_STRING(type_tmp, type));
    }
}

float value_min=0, value_max=1;

void getting_range(GtkRange *range, float *value){
    *value = gtk_range_get_value(range);
}

void getting_spin_value(GtkSpinButton *button, float *value){
    *value = gtk_spin_button_get_value(button);
}

void getting_check_value(GtkToggleButton *button, float *value){
    if (gtk_toggle_button_get_active(button)) *value = 1.0;
    else *value = 0.0;
}

void update_entry(GtkWidget *entry, gdouble value){
    char *entry_string;
    asprintf(&entry_string, "%lf", value);
    gtk_entry_set_text(GTK_ENTRY(entry), entry_string);
    free(entry_string);
}

void update_check_value(GtkWidget *button, gdouble value){
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), (value > 0.5));
}

gboolean tick_callback(GtkRange *range, GdkFrameClock *frame_clock, struct update *update){
    (void)range;
    (void)frame_clock;
    
    update->update_all();
    return update->g_source_mode;
}

void *refresh_update(void *user_data)
{
    int ch=' ';
    struct update *update=(struct update*)user_data;
    
    update->g_source_mode=G_SOURCE_REMOVE;
    while(ch!='q' && ch!=-1)
    {
        ch=fgetc(stdin);
        switch (ch){
                
            case '.':gtk_widget_add_tick_callback(update->main_widget, (GtkTickCallback)tick_callback, (void*)update, NULL);
                break;
            case 'q': g_application_quit(G_APPLICATION(app)); break;
            case '\n':break;
            case -1: if (feof(stdin)) exit(0);break;
            default:fprintf(stderr, "Unknown command '%c' code '%d'.\n", ch, ch);
        }
    }
    return NULL;
}

GtkWidget *create_float_display(blc_channel *channel)
{
    GtkWidget *widget = NULL, *grid, *vbox, *toolbar;
    char label_text[NAME_MAX + 1];
    GtkWidget *display;
    int steps, width, height, h, i, j;
    char *values;
    uint32_t format_display=ntohl(channel->format);
    pthread_t thread;
    struct update *update;
    SPRINTF(label_text, "%s %.4s", channel->name, (char*)&format_display);
    display = gtk_frame_new(label_text);
    void *value_pt;
    size_t value_size;
    
    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 3);
    
    toolbar = gtk_toolbar_new();
    gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_BOTH);
    
    if (channel->dims_nb==1)
    {
        steps=1;
        width=channel->lengths[0];
        height=1;
    }
    if (channel->dims_nb==2)
    {
        steps=1;
        width=channel->lengths[0];
        height=channel->lengths[1];
    }
    else if (channel->dims_nb==3)
    {
        steps=channel->lengths[0];
        width=channel->lengths[1];
        height=channel->lengths[2];
    };
    
    display = gtk_frame_new(channel->name);
    grid = gtk_grid_new();
    
    values=channel->data;
    update=new struct update(channel->type);
    value_size=blc_get_type_size(channel->type);

    FOR(j, height) FOR(i, width) FOR(h, steps)
    {
        value_pt=values+(j*width*steps+i*steps+h)*value_size;
        
        switch(format)
        {
            case 'TBOX': case 'NDEF':
                widget = gtk_entry_new();
                update->function=(type_update_function)update_entry;
           //     g_signal_connect(G_OBJECT(widget), "value-changed", G_CALLBACK(getting_spin_value), (void*)value_pt);
                break;
            case  'VMET':
                widget = gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, value_min, value_max, 0.001);
                gtk_range_set_inverted(GTK_RANGE(widget), TRUE);
                update->function=(type_update_function)gtk_range_set_value;
                g_signal_connect(G_OBJECT(widget), "value-changed", G_CALLBACK(getting_range), (void*)value_pt);
                break;
            case 'CBOX':
                widget = gtk_check_button_new();
                update->function= update_check_value;
                g_signal_connect(G_OBJECT(widget), "toggled", G_CALLBACK(getting_check_value), (void*)value_pt);
                break;
            default:
                snprintf(label_text, NAME_MAX, "Unknown format ('%.4s') for float values.", (char*)&format_display);
                widget = gtk_label_new(label_text);
                gtk_container_add(GTK_CONTAINER(grid), widget);
        }
        update->add(widget, value_pt);
        update->main_widget=grid;
        gtk_grid_attach(GTK_GRID(grid), widget, i+h, j, 1, 1);
        gtk_widget_set_vexpand(widget, TRUE);
        gtk_widget_set_hexpand(widget, TRUE);
        
        if (interactive_mode)  SYSTEM_ERROR_CHECK(pthread_create(&thread, NULL, refresh_update, (void*)update), -1, NULL);
        else gtk_widget_add_tick_callback(widget, (GtkTickCallback)tick_callback,(void*)update, NULL);
    }
    
    gtk_container_add(GTK_CONTAINER(vbox), toolbar);
    gtk_container_add(GTK_CONTAINER(vbox), grid);
    gtk_container_add(GTK_CONTAINER(display), vbox);
    
    return display;
}



