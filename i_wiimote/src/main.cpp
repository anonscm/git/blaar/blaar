//test sur la récupération des données de Promethe

#include <stdio.h>
#include "../wiiuse-master/src/wiiuse.h"
#include "blc.h"

int main()
{
blc_channel *channel_A = NULL;
blc_channel *channel_B = NULL;
blc_channel *channel_Up = NULL;
blc_channel *channel_Down = NULL;
blc_channel *channel_Left = NULL;
blc_channel *channel_Right = NULL;
blc_channel *channel_Plus = NULL;
blc_channel *channel_Minus = NULL;
blc_channel *channel_Home = NULL;
blc_channel *channel_One = NULL;
blc_channel *channel_Two = NULL;

blc_channel *channel_roll = NULL;
blc_channel *channel_pitch = NULL;

blc_channel *channel_x = NULL;
blc_channel *channel_y = NULL;
blc_channel *channel_z = NULL;

channel_A = create_or_open_blc_channel("/wii.A-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);
channel_B = create_or_open_blc_channel("/wii.B-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);
channel_Up = create_or_open_blc_channel("/wii.Up-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);
channel_Down = create_or_open_blc_channel("/wii.Down-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);
channel_Left = create_or_open_blc_channel("/wii.Left-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);
channel_Right = create_or_open_blc_channel("/wii.Right-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);
channel_Plus = create_or_open_blc_channel("/wii.Plus-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);
channel_Minus = create_or_open_blc_channel("/wii.Minus-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);
channel_Home = create_or_open_blc_channel("/wii.Home-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);
channel_One = create_or_open_blc_channel("/wii.One-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);
channel_Two = create_or_open_blc_channel("/wii.Two-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);

channel_roll = create_or_open_blc_channel("/wii.roll-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);
channel_pitch = create_or_open_blc_channel("/wii.pitch-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);
channel_x = create_or_open_blc_channel("/wii.x-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);
channel_y = create_or_open_blc_channel("/wii.y-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);
channel_z = create_or_open_blc_channel("/wii.z-tfv",NULL, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("VMET"), NULL,1,1);


wiimote** wiimotes;
        int found, connected;
        wiimotes=wiiuse_init(1);
        if(!wiiuse_find(wiimotes,1,9000))
        {
            fprintf(stderr,"Aucune Wiimote trouvee");
            exit(EXIT_FAILURE);
        }
        while (!(wiiuse_connect(wiimotes,1))) {}
        wiiuse_set_leds(wiimotes[0],WIIMOTE_LED_1);
        wiiuse_rumble(wiimotes[0],1);
        //SDL_Delay(400);
        wiiuse_rumble(wiimotes[0],0);
	wiiuse_set_ir(wiimotes[0],1);
	wiiuse_motion_sensing(wiimotes[0], 1);

	int continuer =1;
	
	while (continuer)
	{
		if(wiiuse_poll(wiimotes,1))
		{
			if (IS_JUST_PRESSED(wiimotes[0], WIIMOTE_BUTTON_HOME))
			{
				*(float*)channel_Home->data = 1;
				printf("Home pressed\n");
			}
			if (IS_RELEASED(wiimotes[0], WIIMOTE_BUTTON_HOME))
			{
				*(float*)channel_Home->data = 0;
				printf("Home released\n");
			}

			if (IS_JUST_PRESSED(wiimotes[0], WIIMOTE_BUTTON_TWO))
			{
				*(float*)channel_Two->data = 1;
				printf("Two pressed\n");
			}
			if (IS_RELEASED(wiimotes[0], WIIMOTE_BUTTON_TWO))
			{
				*(float*)channel_Two->data = 0;
				printf("Two released\n");
			}

			if (IS_JUST_PRESSED(wiimotes[0], WIIMOTE_BUTTON_ONE))
			{	
				*(float*)channel_One->data = 1;
				printf("one pressed\n");
			}
			if (IS_RELEASED(wiimotes[0], WIIMOTE_BUTTON_ONE))
			{	
				*(float*)channel_One->data = 0;
				printf("one released\n");
			}

			if (IS_JUST_PRESSED(wiimotes[0], WIIMOTE_BUTTON_B))
				{
				*(float*)channel_B->data =1;
				printf("B pressed\n");
				}
			if (IS_RELEASED(wiimotes[0], WIIMOTE_BUTTON_B))
				{
				*(float*)channel_B->data =0;
				printf("B released\n");
				}
			
			if (IS_JUST_PRESSED(wiimotes[0], WIIMOTE_BUTTON_A))
			{
				*(float*)channel_A->data  = 1;
				printf("A pressed\n");
			}
			if (IS_RELEASED(wiimotes[0], WIIMOTE_BUTTON_A))
			{
				*(float*)channel_A->data  = 0;
				printf("A released\n");
			}

			if (IS_JUST_PRESSED(wiimotes[0], WIIMOTE_BUTTON_MINUS))
			{
				*(float*)channel_Minus->data  = 1;
				printf("Minus pressed\n");
			}
			if (IS_RELEASED(wiimotes[0], WIIMOTE_BUTTON_MINUS))
			{
				*(float*)channel_Minus->data  = 0;
				printf("Minus released\n");
			}
			
			if (IS_JUST_PRESSED(wiimotes[0], WIIMOTE_BUTTON_LEFT))
			{
				*(float*)channel_Left->data  = 1;
				printf("Left pressed\n");
			}
			if (IS_RELEASED(wiimotes[0], WIIMOTE_BUTTON_LEFT))
			{
				*(float*)channel_Left->data  = 0;
				printf("Left released\n");
			}
			
			if (IS_JUST_PRESSED(wiimotes[0], WIIMOTE_BUTTON_RIGHT))
			{
				*(float*)channel_Right->data  = 1;
				printf("Right pressed\n");
			}
			if (IS_RELEASED(wiimotes[0], WIIMOTE_BUTTON_RIGHT))
			{
				*(float*)channel_Right->data  = 0;
				printf("Right released\n");
			}
			
			if (IS_JUST_PRESSED(wiimotes[0], WIIMOTE_BUTTON_DOWN))
			{
				*(float*)channel_Down->data  = 1;
				printf("Down pressed\n");
			}
			if (IS_RELEASED(wiimotes[0], WIIMOTE_BUTTON_DOWN))
			{
				*(float*)channel_Down->data  = 0;
				printf("Down released\n");
			}
			
			if (IS_JUST_PRESSED(wiimotes[0], WIIMOTE_BUTTON_UP))
			{
				*(float*)channel_Up->data  = 1;
				printf("Up pressed\n");
			}
			if (IS_RELEASED(wiimotes[0], WIIMOTE_BUTTON_UP))
			{
				*(float*)channel_Up->data  = 0;
				printf("Up released\n");
			}
			
			if (IS_JUST_PRESSED(wiimotes[0], WIIMOTE_BUTTON_PLUS))
			{
				*(float*)channel_Plus->data  = 1;
				printf("Plus pressed\n");
			}
			if (IS_RELEASED(wiimotes[0], WIIMOTE_BUTTON_PLUS))
			{
				*(float*)channel_Plus->data  = 0;
				printf("Plus released\n");
			}
			if ((IS_PRESSED(wiimotes[0], WIIMOTE_BUTTON_MINUS))&&(IS_PRESSED(wiimotes[0], WIIMOTE_BUTTON_PLUS))&&(IS_PRESSED(wiimotes[0], WIIMOTE_BUTTON_B)))
			{
				*(float*)channel_Plus->data  = 0;
				*(float*)channel_Minus->data  = 0;
				*(float*)channel_B->data  = 0;
				printf(" arret\n");
				continuer = 0;
			}

			*(float*) channel_roll->data = wiimotes[0]->orient.roll ;
			*(float*) channel_pitch->data = wiimotes[0]->orient.pitch ;
			*(float*) channel_x->data = wiimotes[0]->gforce.x ;
		}
	}
        wiiuse_disconnect(wiimotes[0]);
	return 0;
}





//f_display_image_activity
