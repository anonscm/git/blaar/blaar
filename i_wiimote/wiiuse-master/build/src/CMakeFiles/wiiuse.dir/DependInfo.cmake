# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/src/classic.c" "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/build/src/CMakeFiles/wiiuse.dir/classic.c.o"
  "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/src/dynamics.c" "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/build/src/CMakeFiles/wiiuse.dir/dynamics.c.o"
  "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/src/events.c" "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/build/src/CMakeFiles/wiiuse.dir/events.c.o"
  "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/src/guitar_hero_3.c" "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/build/src/CMakeFiles/wiiuse.dir/guitar_hero_3.c.o"
  "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/src/io.c" "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/build/src/CMakeFiles/wiiuse.dir/io.c.o"
  "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/src/ir.c" "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/build/src/CMakeFiles/wiiuse.dir/ir.c.o"
  "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/src/motion_plus.c" "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/build/src/CMakeFiles/wiiuse.dir/motion_plus.c.o"
  "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/src/nunchuk.c" "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/build/src/CMakeFiles/wiiuse.dir/nunchuk.c.o"
  "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/src/os_nix.c" "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/build/src/CMakeFiles/wiiuse.dir/os_nix.c.o"
  "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/src/util.c" "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/build/src/CMakeFiles/wiiuse.dir/util.c.o"
  "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/src/wiiboard.c" "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/build/src/CMakeFiles/wiiuse.dir/wiiboard.c.o"
  "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/src/wiiuse.c" "/home/arnablan/simulator/basic_libs/blaa/f_wiimote/wiiuse-master/build/src/CMakeFiles/wiiuse.dir/wiiuse.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "WIIUSE_COMPILE_LIB"
  "WIIUSE_STATIC"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
