#include <fcntl.h> // O_RDONLY ...
#include <stdio.h>
#include <gtk/gtk.h>
#include <string.h>
#include <stdint.h> //uint32_t
#include <sys/mman.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h> //errno
#include <libgen.h> //dirname
#include <locale.h>


#include "main.hpp"
#include "commands.hpp"
#include "channels.hpp"
#include "processes.hpp"
#include <unistd.h>

#include "blc.h"
#include "blgtk.h"
char const *start_filter_name = "";
char const *blaa_dir=NULL;

GtkWidget *statusbar;
GtkWidget *main_window;
int statusbar_context_id;

char  *commands_filename=NULL;
char const *binary_dirname;
dictionary *ini_dictionary;
char inifile_path[PATH_MAX+1];


static void  open_commands_cb(GtkWidget *widget, gpointer user_data)
{
    GtkWidget *dialog;
    GtkFileChooser *chooser;
    int answer;
    (void)widget;
    
    if (user_data==NULL)
    {
        dialog = gtk_file_chooser_dialog_new ("Open commands file (.sh)", GTK_WINDOW(main_window),  GTK_FILE_CHOOSER_ACTION_OPEN,
                                              "_Cancel", GTK_RESPONSE_CANCEL,
                                              "_Open", GTK_RESPONSE_ACCEPT, NULL);
        
        answer = gtk_dialog_run (GTK_DIALOG (dialog));
        if (answer == GTK_RESPONSE_ACCEPT)
        {
            chooser = GTK_FILE_CHOOSER (dialog);
            FREE(commands_filename);
            commands_filename = gtk_file_chooser_get_filename (chooser);
            gtk_window_set_title(GTK_WINDOW(main_window), commands_filename);
            open_commands(commands_filename);
        }
        gtk_widget_destroy (dialog);
    }
}

static void save_commands_cb(GtkWidget *widget, gpointer user_data)
{
    GtkWidget *dialog;
    GtkFileChooser *chooser;
    int answer;
    (void)widget;
    (void)user_data;
    
    if (user_data==NULL)
    {
        dialog = gtk_file_chooser_dialog_new ("Save commands file (.sh)", GTK_WINDOW(main_window),  GTK_FILE_CHOOSER_ACTION_SAVE,
                                              "_Cancel", GTK_RESPONSE_CANCEL,
                                              "_Save", GTK_RESPONSE_ACCEPT, NULL);
        
        answer = gtk_dialog_run (GTK_DIALOG (dialog));
        if (answer == GTK_RESPONSE_ACCEPT)
        {
            chooser = GTK_FILE_CHOOSER (dialog);
            FREE(commands_filename);
            commands_filename = gtk_file_chooser_get_filename (chooser);
            save_commands(commands_filename);
            gtk_window_set_title(GTK_WINDOW(main_window), commands_filename);
        }
        gtk_widget_destroy (dialog);
    }
    else
    {
        commands_filename = (char*)user_data;
        save_commands(commands_filename);
        gtk_window_set_title(GTK_WINDOW(main_window), commands_filename);

    }
}

static void preferences()
{
    GtkWidget *dialog;
    
  //  dialog=gtk_dialog_new_with_buttons("Preferences", main_window, "_Ok", NULL);
    
    
}

/** Look for the blc_channels and then create the graphical interface */
static void activate(GtkApplication *app)
{
    char title[NAME_MAX + 1];
    GtkWidget *layout, *hbox, *vpanes;
    GtkWidget *toolbar, *dialog;
    gint ret;
    
    main_window = gtk_application_window_new(app);
    snprintf(title, NAME_MAX, "blc_channels %s", start_filter_name);
    gtk_window_set_title(GTK_WINDOW(main_window), title);
    
    if (blaa_dir==NULL)
    {
        dialog=gtk_file_chooser_dialog_new("Select your blaa directory.",  GTK_WINDOW(main_window), GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER, "_OK", NULL);
        ret=gtk_dialog_run(GTK_DIALOG(dialog));
        gtk_widget_destroy(dialog);
        blaa_dir=gtk_file_chooser_get_current_folder(GTK_FILE_CHOOSER(dialog));
        iniparser_set(ini_dictionary, "directories", blaa_dir);
        iniparser_set(ini_dictionary, "directories:blaa_dir", blaa_dir);
        iniparser_save(ini_dictionary, inifile_path);
    }
    statusbar=gtk_statusbar_new();
    statusbar_context_id = gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "default");
    layout = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    hbox=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    vpanes =gtk_paned_new(GTK_ORIENTATION_VERTICAL);
    gtk_paned_set_wide_handle(GTK_PANED(hbox), TRUE);
    gtk_paned_set_wide_handle(GTK_PANED(vpanes), TRUE);


    toolbar = gtk_toolbar_new();
    gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_BOTH);
    
    blgtk_add_tool_button(toolbar, "open", "document-open", G_CALLBACK(open_commands_cb), NULL);
    blgtk_add_tool_button(toolbar, "save", "document-save", G_CALLBACK(save_commands_cb), (void*)commands_filename);
    blgtk_add_tool_button(toolbar, "save as", "document-save-as", G_CALLBACK(save_commands_cb), NULL);

    gtk_container_add(GTK_CONTAINER(layout), toolbar);
    gtk_container_add(GTK_CONTAINER(vpanes), create_channels_display());
    gtk_container_add(GTK_CONTAINER(hbox), create_commands_display());
    gtk_container_add(GTK_CONTAINER(hbox), create_processes_display());

    gtk_container_add(GTK_CONTAINER(vpanes), hbox);
    gtk_container_add(GTK_CONTAINER(layout), vpanes);
    gtk_container_add(GTK_CONTAINER(layout), statusbar);
    gtk_container_add(GTK_CONTAINER(main_window), layout);
    
    gtk_widget_show_all(main_window);
    gtk_window_resize(GTK_WINDOW(main_window), 1024, 800);
}

/** Classical GTK application.
 * The first optional argument is the name of the experience. Otherwise all the existing shared memory are used.
 * */
int main(int argc, char **argv)
{
    GtkApplication *app;
    char *path_copy;
    int status;
    
    blc_program_set_description("Display details of all the blc_channels.");
    blc_program_init(&argc, &argv, NULL);

    SPRINTF(inifile_path, "%s/.blaar", getenv("HOME"));
    
    if (access(inifile_path, R_OK | W_OK))  SYSTEM_ERROR_CHECK(open(inifile_path, O_CREAT | O_EXCL | O_RDWR, S_IRWXG), -1, "Creating ini file '%s'.", inifile_path);
    ini_dictionary=iniparser_load(inifile_path);
    blaa_dir=iniparser_getstring(ini_dictionary, "directories:blaa_dir", NULL);
    
    gtk_init(&argc, &argv);
    setenv("LC_ALL", "C", 1);
    setlocale(LC_ALL, "C");

    if (argc == 2)
    {
        start_filter_name = argv[1];
        argc = 1; // To avoid g_application to try to interpret it.
    }
    path_copy = strdup(argv[0]);
    binary_dirname=dirname(path_copy);
    
    app = gtk_application_new(NULL, G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
    status = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);
    free(path_copy);
    return (status);
}

