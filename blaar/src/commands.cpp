#include "commands.hpp"
#include "blc.h"
#include "blgtk.h"
#include "main.hpp"

#include <vte/vte.h>

#include <sys/wait.h>
#include <libgen.h>
#include <errno.h>
#include <pthread.h>

enum {WATCH_PROCESS_COLUMN, DIR_COLUMN, COMMAND_LINE_COLUMN, CONTROL_ICON_COLUMN, STATUS_ICON_COLUMN, TERMINAL_STDOUT_ICON_COLUMN, TERMINAL_STDERR_ICON_COLUMN, TERMINAL_STDIN_ICON_COLUMN, COLUMNS_NB};

static struct tree_view_column{
    int id;
    char const *name;
    int type;
}columns[]={
    {WATCH_PROCESS_COLUMN, NULL, G_TYPE_POINTER},
    {DIR_COLUMN, NULL, G_TYPE_STRING},
    {COMMAND_LINE_COLUMN, NULL, G_TYPE_STRING},
    {CONTROL_ICON_COLUMN, NULL, G_TYPE_STRING},
    {STATUS_ICON_COLUMN, NULL, G_TYPE_STRING},
    {TERMINAL_STDOUT_ICON_COLUMN, NULL, G_TYPE_STRING},
    {TERMINAL_STDERR_ICON_COLUMN, NULL, G_TYPE_STRING},
    {TERMINAL_STDIN_ICON_COLUMN, NULL, G_TYPE_STRING}
};


GtkTreeStore *commands_store;
static GtkWidget *tree_view, *dir_chooser;
static GtkCellRenderer *control_renderer, *stderr_renderer, *stdout_renderer, *stdin_renderer;
static fd_set fd_write_set;

static int save_each_process_command(GtkTreeModel *model,  GtkTreePath *tree_path, GtkTreeIter *iter,   gpointer user_data)
{
    char const *command_line;
    char  *dir;
    char const *filename = (char const *)user_data;
    FILE *file;
    (void)model;
    (void)tree_path;
    
    file=fopen(filename, "w");
    fprintf(file, "#!/bin/sh\n");
    gtk_tree_model_get(GTK_TREE_MODEL(commands_store),  iter, DIR_COLUMN, &dir, COMMAND_LINE_COLUMN, &command_line, -1);
    if (strlen(dir)) fprintf(file, "cd %s && ./%s&\n", dir, command_line);
    else fprintf(file, "%s&\n", command_line);
    fclose(file);
    return FALSE; //Continue
}

static void dir_edited_cb(GtkCellRendererText *renderer, gchar *path, gchar *new_text, gpointer user_data)
{
    char *dir;
    GtkTreeIter iter;
    (void)renderer;
    (void)user_data;
    
    gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(commands_store),  &iter, path);
    gtk_tree_model_get(GTK_TREE_MODEL(commands_store), &iter, DIR_COLUMN, &dir, -1);
    free(dir);
    dir=strdup(new_text);
    gtk_tree_store_set(commands_store, &iter, DIR_COLUMN, dir, -1);

}

static void command_line_edited_cb(GtkCellRendererText *renderer, gchar *path, gchar *new_text, gpointer user_data)
{
    char *command_line;
    GtkTreeIter iter;
    (void)renderer;
    (void)user_data;
    
    gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(commands_store),  &iter, path);
    gtk_tree_model_get(GTK_TREE_MODEL(commands_store), &iter, COMMAND_LINE_COLUMN, &command_line, -1);
    free(command_line);
    command_line=strdup(new_text);
    gtk_tree_store_set(commands_store, &iter, COMMAND_LINE_COLUMN, command_line, -1);
}

static void add_command_cb(GtkToolButton *toolbutton, gpointer *user_data)
{
    (void)toolbutton;
    (void)user_data;
    
    char *argv=NULL;
    create_command_line("", &argv);
}


static void terminal_input(VteTerminal *vteterminal, gchar *text, guint size, gpointer user_data)
{
    int x, y;
    struct watch_process *watch_process = (struct watch_process*)user_data;
    
    SYSTEM_ERROR_CHECK(write(watch_process->stdin_pipe[1], text, size), -1, "stdin write");
    
    // Not elegant but avoid to display the answer escape answer ( In this case the cursor position).
    if (sscanf(text, "\33[%d;%dR", &x, &y) !=2) vte_terminal_feed(VTE_TERMINAL(vteterminal), text, size);
    //We add \n in case or \r
    if (text[size-1]=='\r') vte_terminal_feed(VTE_TERMINAL(vteterminal), "\n", 1);
}

static int terminal_set_warning_cb(gpointer user_data){
    struct watch_process *wp = (struct watch_process*)user_data;
    gtk_tree_store_set(commands_store, &wp->iter, TERMINAL_STDERR_ICON_COLUMN, "dialog-warning", -1);
    return G_SOURCE_REMOVE;
}

static int terminal_set_info_cb(gpointer user_data)
{
    struct watch_process *wp = (struct watch_process*)user_data;
    gtk_tree_store_set(commands_store, &wp->iter, TERMINAL_STDOUT_ICON_COLUMN, "dialog-information", -1);
    return G_SOURCE_REMOVE;
}

static int terminal_set_input_cb(gpointer user_data)
{
    struct watch_process *wp = (struct watch_process*)user_data;
    gtk_tree_store_set(commands_store, &wp->iter, TERMINAL_STDIN_ICON_COLUMN, "input-keyboard", -1);
    return G_SOURCE_REMOVE;
}


static void *watch_process_run(void *user_data) {
    char buf[4096];
    char const *end_of_line, *pos;
    gulong commit_signal_handler;
    int status, n=-1, fd_max;
    struct watch_process *watch_process;
    watch_process = (struct watch_process*)user_data;
    fd_set fd_read_set;
    
    fd_max=MAX(watch_process->stderr_pipe[0], watch_process->stdout_pipe[0]);
    fd_max=MAX(fd_max, watch_process->stdin_pipe[1]);
    
    commit_signal_handler= g_signal_connect(G_OBJECT(watch_process->terminal), "commit", G_CALLBACK(terminal_input), watch_process);
    
    FD_ZERO(&fd_write_set);
    FD_SET(watch_process->stdin_pipe[1], &fd_write_set);
    
    while(n!=0)
    {
        FD_ZERO(&fd_read_set);
        
        FD_SET(watch_process->stderr_pipe[0], &fd_read_set);
        FD_SET(watch_process->stdout_pipe[0], &fd_read_set);
        
        select(fd_max+1, &fd_read_set, &fd_write_set, NULL, NULL);
        
        if (FD_ISSET(watch_process->stderr_pipe[0], &fd_read_set))
        {
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(n=read(watch_process->stderr_pipe[0], buf, sizeof(buf)), -1, EINTR, "stderr");
            if (n!=0)
            {
                pos=buf;
            //in order to have carraige return on each line
            do
            {
                end_of_line=strchr(pos, '\n');
                if (end_of_line==NULL) vte_terminal_feed(VTE_TERMINAL(watch_process->error_terminal), pos, -1);
                else
                {
                    vte_terminal_feed(VTE_TERMINAL(watch_process->error_terminal), pos, end_of_line-pos+1);
                    vte_terminal_feed(VTE_TERMINAL(watch_process->error_terminal), "\r", 1);
                }
                pos=end_of_line+1;
            }while(end_of_line);
            gdk_threads_add_idle(terminal_set_warning_cb, watch_process);
            }
            
        }
        
        if (FD_ISSET(watch_process->stdout_pipe[0], &fd_read_set))
        {
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(n=read(watch_process->stdout_pipe[0], buf, sizeof(buf)), -1, EINTR, "stdin");
            if (n!=0)
            {
            //in order to hava carraige return on each line
            pos=buf;
            do
            {
                end_of_line=strchr(pos, '\n');
                if (end_of_line==NULL) vte_terminal_feed(VTE_TERMINAL(watch_process->terminal), pos, -1);
                else
                {
                    vte_terminal_feed(VTE_TERMINAL(watch_process->terminal), pos, end_of_line-pos+1);
                    vte_terminal_feed(VTE_TERMINAL(watch_process->terminal), "\r", 1);
                }
                pos=end_of_line+1;
            }while(end_of_line);
            
            gdk_threads_add_idle(terminal_set_info_cb, watch_process);
            }
        }
        
        
        // The gold is to check if the process is waiting input but it does not work */
        if (FD_ISSET(watch_process->stdin_pipe[1], &fd_write_set))
        {
            gdk_threads_add_idle(terminal_set_input_cb, watch_process);
            FD_ZERO(&fd_write_set);
            
            n=-1;
            continue;
        }
        
    }
    g_signal_handler_disconnect(watch_process->terminal,commit_signal_handler);
    close(watch_process->stderr_pipe[0]);
    close(watch_process->stdout_pipe[0]);
    close(watch_process->stdin_pipe[1]);
    
    waitpid(watch_process->pid, &status, 0);
    watch_process->pid=0;
    if (status != 0)     gtk_tree_store_set(commands_store, &watch_process->iter, STATUS_ICON_COLUMN, "dialog-error", -1);
    else gtk_tree_store_set(commands_store, &watch_process->iter, STATUS_ICON_COLUMN, "process-stop", -1);


    gtk_tree_store_set(commands_store, &watch_process->iter, CONTROL_ICON_COLUMN, "media-playback-start", -1);
    return NULL;
}

void execute_process_with_iter(GtkTreeIter *iter) {
    char const *directory;
    char const *command_line;
    char *const *argv;
    char tmp_path[PATH_MAX+1];
    char *cwd;
    pthread_t thread;
    struct watch_process *watch_process;
   /* GPid pid;
    GError *error=NULL;*/
    
    
    gtk_tree_model_get(GTK_TREE_MODEL(commands_store), iter, WATCH_PROCESS_COLUMN, &watch_process,
                       DIR_COLUMN, &directory,
                       COMMAND_LINE_COLUMN, &command_line, -1);
    
/*    argv=create_argv_from_command_line(command_line);
    vte_terminal_spawn_sync(VTE_TERMINAL(watch_process->terminal), VTE_PTY_DEFAULT, (const char *)directory, (char**)argv, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL, &pid, NULL, &error);
  */

    SYSTEM_ERROR_CHECK(pipe(watch_process->stdout_pipe), -1, NULL);
    SYSTEM_ERROR_CHECK(pipe(watch_process->stderr_pipe), -1, NULL);
    SYSTEM_ERROR_CHECK(pipe(watch_process->stdin_pipe), -1, NULL);
    
    watch_process->pid = fork();
    if (watch_process->pid==0)
    {
        SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(watch_process->stdout_pipe[1], STDOUT_FILENO), -1, EINTR, NULL);
        SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(watch_process->stderr_pipe[1], STDERR_FILENO), -1, EINTR, NULL);
        SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(watch_process->stdin_pipe[0] , STDIN_FILENO ), -1, EINTR, NULL);
        
        blc_close_pipe(watch_process->stdout_pipe);
        blc_close_pipe(watch_process->stderr_pipe);
        blc_close_pipe(watch_process->stdin_pipe );
        
        argv=create_argv_from_command_line(command_line);
        
        cwd=gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dir_chooser));
        chdir(cwd);
        g_free(cwd);
        
        if (strlen(directory)==0)  STRCPY(tmp_path, argv[0]);
        else SPRINTF(tmp_path, "%s/%s", directory, argv[0]);
        
        if (execvp(tmp_path, argv)==-1)
        {
            color_fprintf(BLC_RED, stderr, "Impossible to execute in working directory '%s' the path '%s' command line '%s'.\nSystem error: %s\n", cwd, tmp_path, command_line, strerror(errno));
            exit(1);
        }
    }
    else
    {
        close(watch_process->stdout_pipe[1]);
        close(watch_process->stderr_pipe[1]);
        close(watch_process->stdin_pipe[0]);
        
        pthread_create(&thread, NULL, watch_process_run, watch_process);
        gtk_tree_store_set(commands_store, iter, CONTROL_ICON_COLUMN, "media-playback-stop",
                           STATUS_ICON_COLUMN, "emblem-default",
                           TERMINAL_STDOUT_ICON_COLUMN, "utilities-terminal",
                           TERMINAL_STDERR_ICON_COLUMN, "utilities-terminal", -1);
        
    }
}

static gboolean processes_tree_button_release_cb(GtkWidget *tree_view, GdkEventButton *event, gpointer user_data)
{
    GtkWidget *terminal_box, *frame1, *frame2;
    GtkTreeViewColumn *column;
    GtkTreePath *tree_path;
    GtkTreeIter iter;
    struct watch_process *wp;
    (void)user_data;
    
    if (event->button==GDK_BUTTON_PRIMARY)
    {
        gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(tree_view), event->x, event->y, &tree_path, &column, NULL, NULL);
        
        if (tree_path!=NULL)
        {
            gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(commands_store),  &iter,  gtk_tree_path_to_string(tree_path));
            if (gtk_tree_view_column_cell_get_position(column, control_renderer, NULL, NULL))
            {
                gtk_tree_model_get(GTK_TREE_MODEL(commands_store), &iter, WATCH_PROCESS_COLUMN, &wp, -1);
                if (wp->pid==0) execute_process_with_iter(&iter);
                else   kill(wp->pid, SIGTERM);
            }
            else if (gtk_tree_view_column_cell_get_position(column, stdout_renderer, NULL, NULL)
                     || gtk_tree_view_column_cell_get_position(column, stderr_renderer, NULL, NULL)
                     || gtk_tree_view_column_cell_get_position(column, stdin_renderer, NULL, NULL))
                
            {
                gtk_tree_model_get(GTK_TREE_MODEL(commands_store), &iter, WATCH_PROCESS_COLUMN, &wp, -1);
                
                if (wp->terminal_window==NULL)
                {
                    wp->terminal_window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
                    terminal_box=gtk_paned_new(GTK_ORIENTATION_VERTICAL);
                    frame1=gtk_frame_new("stdout");
                    frame2=gtk_frame_new("stderr");
                    gtk_container_add(GTK_CONTAINER(frame1), wp->terminal);
                    gtk_container_add(GTK_CONTAINER(frame2), wp->error_terminal);


                    gtk_widget_set_vexpand(wp->terminal, TRUE);
                    gtk_widget_set_vexpand(wp->error_terminal, TRUE);
                    gtk_paned_pack1(GTK_PANED(terminal_box),frame1, TRUE,FALSE);
                    gtk_paned_pack2(GTK_PANED(terminal_box), frame2, TRUE, FALSE);
                    gtk_container_add(GTK_CONTAINER(wp->terminal_window), terminal_box);
                    
                    gtk_widget_show_all(wp->terminal_window);
                    g_signal_connect(wp->terminal_window, "delete-event", G_CALLBACK(gtk_widget_hide_on_delete), NULL);

                }
                else gtk_widget_set_visible(wp->terminal_window, 1);
                gtk_window_activate_focus(GTK_WINDOW(wp->terminal_window));

                gtk_tree_store_set(commands_store, &wp->iter, TERMINAL_STDERR_ICON_COLUMN, "utilities-terminal", -1);
                gtk_tree_store_set(commands_store, &wp->iter, TERMINAL_STDOUT_ICON_COLUMN, "utilities-terminal", -1);
            }
        }
    }
    return TRUE;
}

void save_commands(char const *filename)
{
    gtk_tree_model_foreach(GTK_TREE_MODEL(commands_store), save_each_process_command, (void*)filename);
}

GtkTreeIter *create_command_line(char const *dir, char *const *argv){
    char *command_line, *dir_copy;
    GtkTreeIter *iter_pt;
    struct watch_process *watch_process;
    
    
    iter_pt=ALLOCATION(GtkTreeIter);
    command_line=create_command_line_from_argv(argv);
    
    dir_copy=strdup(dir);
    
    watch_process=ALLOCATION(struct watch_process);

    gtk_tree_store_insert_with_values(commands_store, iter_pt, NULL, -1, WATCH_PROCESS_COLUMN, watch_process,
                                      DIR_COLUMN, dir_copy,
                                      COMMAND_LINE_COLUMN, command_line,
                                      CONTROL_ICON_COLUMN, "media-playback-start",
                                      TERMINAL_STDOUT_ICON_COLUMN, "utilities-terminal",
                                      TERMINAL_STDERR_ICON_COLUMN, "utilities-terminal", -1);
    
    watch_process->pid=0;
    watch_process->iter=*iter_pt;
    watch_process->terminal=vte_terminal_new();
    watch_process->error_terminal=vte_terminal_new();
    watch_process->terminal_window=NULL;
   
    FREE(command_line);
    return iter_pt;
}

void open_commands(char const *filename)
{
    char  *const*argv;
    char command_line[LINE_MAX+1];
    char  cwd[PATH_MAX+1];
    FILE *file;
    SYSTEM_ERROR_CHECK(file=fopen(filename, "r"), NULL, "Opening '%s'.", filename);
    fscanf(file, "%*[^\n]\n"); //#!/bin/sh
    while(fscanf(file, "cd %s && %[^&]&\n", cwd, command_line)==2)
    {
        argv=create_argv_from_command_line(command_line);
        create_command_line(cwd, argv);
    }
}

GtkWidget *create_commands_display() {
    char cwd[PATH_MAX+1];
    GtkWidget *frame, *scrolled_window, *toolbar, *vbox, *add_command_button, *remove_command_button;
    GtkCellRenderer *command_renderer, *dir_renderer, *status_renderer;
    GtkRequisition natural_size, minimum_size;
    GType types[COLUMNS_NB];
    int i;
    FOR(i, COLUMNS_NB) if (i==columns[i].id) types[i]=columns[i].type; else EXIT_ON_ERROR("The order and id of columns does not fit.");
    
    frame=gtk_frame_new("Commands");
    gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
    
    vbox=gtk_box_new(GTK_ORIENTATION_VERTICAL, 3);
    
    toolbar=gtk_toolbar_new();
    gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_BOTH);
    add_command_button=GTK_WIDGET(gtk_tool_button_new(gtk_image_new_from_icon_name("list-add", GTK_ICON_SIZE_SMALL_TOOLBAR), "add\ncommand"));
    remove_command_button=GTK_WIDGET(gtk_tool_button_new(gtk_image_new_from_icon_name("list-remove", GTK_ICON_SIZE_SMALL_TOOLBAR), "remove\ncommand"));
    
    dir_chooser = gtk_file_chooser_button_new("working dir", GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER);
    SYSTEM_ERROR_CHECK(getcwd(cwd, PATH_MAX), NULL, "Getting the current directory");

    gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dir_chooser),   cwd);
     gtk_container_add(GTK_CONTAINER(toolbar), add_command_button);
 //    gtk_container_add(GTK_CONTAINER(toolbar), remove_command_button);
    
    scrolled_window=gtk_scrolled_window_new(NULL, NULL);
    
    commands_store = gtk_tree_store_newv(COLUMNS_NB, types);
    tree_view = gtk_tree_view_new_with_model(GTK_TREE_MODEL(commands_store));
    
    
    control_renderer = gtk_cell_renderer_pixbuf_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree_view), -1, "control", control_renderer,
                                                 "icon-name", CONTROL_ICON_COLUMN,  NULL);
    
    status_renderer = gtk_cell_renderer_pixbuf_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree_view), -1, "status", status_renderer,
                                                 "icon-name", STATUS_ICON_COLUMN,  NULL);
    
    stdout_renderer = gtk_cell_renderer_pixbuf_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree_view), -1, "stdout", stdout_renderer,
                                                 "icon-name", TERMINAL_STDOUT_ICON_COLUMN,  NULL);
    
    stderr_renderer = gtk_cell_renderer_pixbuf_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree_view), -1, "stderr", stderr_renderer,
                                                 "icon-name", TERMINAL_STDERR_ICON_COLUMN,  NULL);
    
    /*  stdin_renderer = gtk_cell_renderer_pixbuf_new ();
     gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree_view), -1,
     "stdin", stdin_renderer,
     "icon-name", TERMINAL_STDIN_ICON_COLUMN,  NULL);
     */

    dir_renderer = gtk_cell_renderer_text_new ();
    g_object_set(G_OBJECT(dir_renderer), "editable", 1, NULL);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree_view), -1, "directory", dir_renderer,
                                                 "text", DIR_COLUMN, NULL);
    
    command_renderer = gtk_cell_renderer_text_new ();
    g_object_set(G_OBJECT(command_renderer), "editable", 1, NULL);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree_view), -1, "command line", command_renderer,
                                                 "text", COMMAND_LINE_COLUMN, NULL);
    
    gtk_container_add(GTK_CONTAINER(scrolled_window), tree_view);
    
    gtk_container_add(GTK_CONTAINER(vbox), toolbar);
    gtk_container_add(GTK_CONTAINER(vbox), dir_chooser);
    gtk_container_add(GTK_CONTAINER(vbox), scrolled_window);
    
    gtk_container_add(GTK_CONTAINER(frame), vbox);
    
    gtk_widget_show_all(tree_view);
    
    gtk_widget_get_preferred_size(tree_view, &minimum_size, &natural_size);
    gtk_widget_set_size_request(scrolled_window, natural_size.width+10, natural_size.height+10);
    
    gtk_widget_set_hexpand(scrolled_window, 1);
    gtk_widget_set_vexpand(scrolled_window, 1);
    
    g_signal_connect(G_OBJECT(add_command_button), "clicked", G_CALLBACK(add_command_cb), NULL);
  //  g_signal_connect(G_OBJECT(remove_command_button), "clicked", G_CALLBACK(remove_command_cb), NULL);

    g_signal_connect(G_OBJECT(dir_renderer), "edited", G_CALLBACK(dir_edited_cb), NULL);
    g_signal_connect(G_OBJECT(command_renderer), "edited", G_CALLBACK(command_line_edited_cb), NULL);
    g_signal_connect(G_OBJECT(tree_view), "button-release-event", G_CALLBACK(processes_tree_button_release_cb), NULL);
    
    return frame;
}



