/*
 http://glsof.sourceforge.net
 */



#include "channels.hpp"
#include "blgtk.h"

#include <fcntl.h> // O_RDONLY ...
#include <stdio.h>
#include <gtk/gtk.h>
#include <string.h>
#include <stdint.h> //uint32_t
#include <sys/mman.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h> //errno
#include <libgen.h> //dirname

#include "main.hpp"
#include "commands.hpp"
#include "blc.h"

enum
{
    NAME_COLUMN, ACK_COLUMN, ACK_VALUE_COLUMN, ACK_INCONSISTENT_COLUMN, BLOCK_COLUMN,  BLOCK_VALUE_COLUMN, BLOCK_INCONSISTENT_COLUMN, PROTECT_COLUMN,  PROTECT_VALUE_COLUMN, PROTECT_INCONSISTENT_COLUMN, TYPE_COLUMN,  FORMAT_COLUMN,  DIMS_COLUMN,  USED_COLUMN, POINTER_COLUMN, COLUMNS_NB
};

struct menu_action
{
    blc_channel_info *channel_info;
    char const *program_name;
    char const *description;
    blc_optional_argument *optional_arguments;
    int optional_arguments_nb;
    blc_positional_argument *positional_arguments;
    int positional_arguments_nb;
    
    menu_action(char const*program_name, char const*description);
    menu_action(char const*program_name, char const*description, blc_optional_argument *, int, blc_positional_argument *, int);
    void dialog();
};


struct channel_context
{
    char *name;
    GtkTreeIter iter;
};

struct process
{
    pid_t pid;
    char command[16];
    
}**processes=NULL;
int processes_nb=0;

struct file_opening
{
    struct process *process;
    char access;
    char type[8];
};

struct file
{
    blc_channel_info channel_info;
    struct file_opening *openings;
    int openings_nb;
}*files=NULL;
int files_nb=0;

int shared_file_need_refresh=1;

char functions_dir[PATH_MAX+1];
GtkWidget *channels_tree;
GtkCellRenderer *used_renderer;

blc_channel_info *channels_infos=NULL;
//blc_channel **channels;
int channels_nb;

int refresh_id=0;
struct channel_context *channel_contexts=NULL;
int channel_contexts_nb=0;

GtkToolItem *remove_tool_item, *refresh_tool_item;
GtkTreeStore *channels_store;
pthread_mutex_t file_update_mutex=PTHREAD_MUTEX_INITIALIZER;

void radio_value_change_cb(GtkRadioButton *widget, gpointer value_addr)
{
    char **value = (char**)value_addr;
    free(*value);
    *value=strdup(gtk_button_get_label(GTK_BUTTON(widget)));
}

void toggled_option_cb(GtkToggleButton *button, gpointer widget)
{
    gtk_widget_set_sensitive(GTK_WIDGET(widget), blgtk_is_toggled(button));
}

void entry_option_changed_cb (GtkEditable *widget, gpointer value_addr)
{
    char **value;
    value = (char**)value_addr;
    free(*value);
    *value=strdup(gtk_entry_get_text(GTK_ENTRY(widget)));
}

menu_action::menu_action(char const *program_name, char const *description): description(description){
    this->program_name=strdup(program_name);
}

menu_action::menu_action(char const*program_name, char const*description, blc_optional_argument *optional_arguments, int optional_arguments_nb, blc_positional_argument *positional_arguments, int positional_arguments_nb)
:description(description), optional_arguments(optional_arguments), optional_arguments_nb(optional_arguments_nb), positional_arguments(positional_arguments), positional_arguments_nb(positional_arguments_nb)
{
    this->program_name=strdup(program_name);
}

void menu_action::dialog()
{
    char const *pos;
    char *arg;
    char **argv;
    int argc;
    
    char token[NAME_MAX+1];
    char text[NAME_MAX+1];
    int line_id;
    char path[PATH_MAX+1];
    
    int token_size;
    GtkWidget  *entry, *dialog, *box;
    GtkWidget  *parameter_label, *description_label, *grid, *radio_button, *help_label;
    int i;
    
    dialog=gtk_dialog_new_with_buttons(program_name, GTK_WINDOW(main_window), GTK_DIALOG_MODAL,  "_OK", GTK_RESPONSE_ACCEPT, "_Cancel", GTK_RESPONSE_REJECT, NULL);
    box=gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    description_label=gtk_label_new(description);
    grid=gtk_grid_new();
    
    line_id=0;
    
    FOR(i, optional_arguments_nb) {
        if (optional_arguments[i].value==NULL) optional_arguments[i].display=NULL;
        else {
            if (strchr(optional_arguments[i].value, '|')){
                pos=optional_arguments[i].value;
                optional_arguments[i].display=gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 1);
                radio_button = NULL;
                token_size=0;
                sscanf(pos, "%*[ ]%n", &token_size); //remove spaces or equal
                pos+=token_size;
                while(sscanf(pos, "%[^| ]%n", token, &token_size)==1) {
                    pos+=token_size;
                    token_size=0;
                    sscanf(pos, "%*[| ]%n", &token_size); //remove spaces or equal
                    pos+=token_size;
                    radio_button=gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(radio_button), token);
                    gtk_container_add(GTK_CONTAINER(optional_arguments[i].display), radio_button);
                    g_signal_connect(G_OBJECT(radio_button), "toggled", G_CALLBACK(radio_value_change_cb), &optional_arguments[i].value);
                }
            }
            else {
                optional_arguments[i].display=gtk_entry_new();
                gtk_entry_set_text(GTK_ENTRY(optional_arguments[i].display), optional_arguments[i].value);
                g_signal_connect(G_OBJECT(optional_arguments[i].display), "changed", G_CALLBACK(entry_option_changed_cb), &optional_arguments[i].value);
            }
        }
        
        if (optional_arguments[i].longname) SPRINTF(text, "--%s", optional_arguments[i].longname);
        else SPRINTF(text, "-%s", optional_arguments[i].shortname);
        parameter_label=gtk_toggle_button_new_with_label(text);
        gtk_widget_set_hexpand(parameter_label, 1);
        gtk_widget_set_halign(parameter_label, GTK_ALIGN_START);
        gtk_grid_attach(GTK_GRID(grid), parameter_label, 0, line_id, 1, 1);
        help_label=gtk_label_new(optional_arguments[i].help);
        gtk_widget_set_halign(help_label, GTK_ALIGN_START);
        if (optional_arguments[i].display) {
            gtk_grid_attach(GTK_GRID(grid), GTK_WIDGET(optional_arguments[i].display), 1, line_id, 1, 1);
        }
        else  optional_arguments[i].display=help_label;
        
        gtk_widget_set_sensitive(GTK_WIDGET(optional_arguments[i].display), FALSE);
        g_signal_connect(G_OBJECT(parameter_label), "toggled", G_CALLBACK(toggled_option_cb), optional_arguments[i].display);
        
        gtk_grid_attach(GTK_GRID(grid), help_label, 2, line_id, 1, 1);
        line_id++;
    }
    
    FOR(i, positional_arguments_nb) {
        parameter_label=gtk_label_new(positional_arguments[i].name);
        entry=gtk_entry_new();
        
        if (positional_arguments[i].value) gtk_entry_set_text(GTK_ENTRY(entry), positional_arguments[i].value);
        gtk_widget_set_hexpand(parameter_label, 0);
        
        gtk_widget_set_halign(parameter_label, GTK_ALIGN_START);
        gtk_grid_attach(GTK_GRID(grid), parameter_label, 0, line_id, 1, 1);
        gtk_grid_attach(GTK_GRID(grid), entry, 1, line_id, 1, 1);
        line_id++;
        g_signal_connect(G_OBJECT(entry), "changed", G_CALLBACK(entry), &positional_arguments[i].value);
    }
    
    gtk_label_set_line_wrap (GTK_LABEL(description_label), 1);
    gtk_widget_set_halign(description_label, GTK_ALIGN_START);
    
    gtk_container_add(GTK_CONTAINER(box), description_label);
    gtk_container_add(GTK_CONTAINER(box), grid);
    
    gtk_widget_show_all(dialog);
    
    switch (gtk_dialog_run(GTK_DIALOG(dialog)))
    {
        case GTK_RESPONSE_ACCEPT:
            SPRINTF(path, "%s/%s", functions_dir, program_name);
            argv=NULL;
            argc=0;
            
            APPEND_ITEM(&argv, &argc, &program_name);
            FOR(i, optional_arguments_nb){
                if (gtk_widget_get_sensitive(GTK_WIDGET(optional_arguments[i].display)))
                {
                    if (optional_arguments[i].longname) asprintf(&arg, "--%s", optional_arguments[i].longname);
                    else if (optional_arguments[i].shortname) asprintf(&arg, "-%s", optional_arguments[i].shortname);
                    else EXIT_ON_ERROR("An option must have at least a short or longt name");
                    APPEND_ITEM(&argv, &argc, &arg);
                    if (optional_arguments[i].value) APPEND_ITEM(&argv, &argc, &optional_arguments[i].value);
                }
            }
            FOR(i, positional_arguments_nb){
                APPEND_ITEM(&argv, &argc, &positional_arguments[i].value);
            }
            arg=NULL;
            APPEND_ITEM(&argv, &argc, &arg);
            execute_process_with_iter(create_command_line(dirname(path), argv));
            
            break;
        case GTK_RESPONSE_CANCEL:break;
    }
    gtk_widget_destroy (dialog);
    FREE(optional_arguments);
    FREE(positional_arguments);
}

static struct process *find_process(pid_t pid)
{
    int i;
    FOR_INV(i, processes_nb) if (processes[i]->pid==pid) return processes[i];
    return NULL;
}

static struct file *find_file(char const *name)
{
    int i;
    FOR_INV(i, files_nb) if (strcmp(files[i].channel_info.name, name)==0) return &files[i];
    return NULL;
}

static gboolean display_file_status(void *user_data)
{
    blc_channel_info *channel_info;
    GtkTreeIter iter;
    struct file *file;
    char const *file_status;
    (void)user_data;
    
    if (gtk_tree_model_get_iter_first(GTK_TREE_MODEL(channels_store), &iter)) do
    {
        gtk_tree_model_get(GTK_TREE_MODEL(channels_store), &iter, POINTER_COLUMN, &channel_info, -1);
        file=find_file(channel_info->name);
        if (file) file_status="task-due";
        else file_status="task-past-due";
        gtk_tree_store_set(GTK_TREE_STORE(channels_store), &iter, USED_COLUMN, file_status, -1);
    }
    while(gtk_tree_model_iter_next (GTK_TREE_MODEL(channels_store), &iter));
    
    return G_SOURCE_REMOVE;
}

static gboolean clean_file_status(void *user_data)
{
    GtkTreeIter iter;
    (void)user_data;
    
    if (gtk_tree_model_get_iter_first(GTK_TREE_MODEL(channels_store), &iter)) do
    {
        gtk_tree_store_set(GTK_TREE_STORE(channels_store), &iter, USED_COLUMN, "system-search", -1);
    }
    while(gtk_tree_model_iter_next (GTK_TREE_MODEL(channels_store), &iter));
    return G_SOURCE_REMOVE;
}


void destroy_file_infos(struct file **files_infos, int *files_infos_nb)
{
    struct file *current_file;
    FOR_EACH(current_file, *files_infos, *files_infos_nb)  FREE(current_file->openings);
    FREE(*files_infos);
    *files_infos_nb=0;
}

static void update_shared_files()
{
    blc_mem mem;
    char buf[LINE_MAX];
    char *path;
    int stdout_pipe[2];
    pid_t pid;
    char *str;
    char letter;
    ssize_t n;
    int status, pos;
    struct process *process, tmp_process;
    struct blc_channel_info tmp_info;
    struct file_opening tmp_file_opening;
    struct file tmp_file;
    struct file *file;
    struct timeval time;
    blc_channel_info *channel_info;
    
    SYSTEM_ERROR_CHECK(pipe(stdout_pipe), -1, NULL);
    pid=fork();
    if (pid==0)
    {
        //SPRINTF(buf, "-p^%d", blaar_pid); Exclude itself
        SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(stdout_pipe[1], STDOUT_FILENO), -1, EINTR, NULL);
        blc_close_pipe(stdout_pipe);
        SYSTEM_ERROR_CHECK(execl("/usr/sbin/lsof", "lsof", "-wlnP", "-F", "actn", NULL), -1, NULL);
    }
    else
    {
        printf("%.3fms\n", us_time_diff(&time)/1000.);
        close(stdout_pipe[1]);
        destroy_file_infos(&files, &files_nb);
        str=buf;
        do
        {
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(n=read(stdout_pipe[0], buf, sizeof(buf)), -1, EINTR, "read lsof stdout");
            mem.append(buf, n);
        }while(n!=0);
        waitpid(pid, &status, 0);
        close(stdout_pipe[0]);
        printf("%.3fms\n", us_time_diff(&time)/1000.);
        
        mem.append("", 1); //end of strin
        str=mem.data;
        do
        {
            letter=str[0];
            pos=0;
            str++;
            switch (letter)
            {
                case 'p':
                    SYSTEM_SUCCESS_CHECK(sscanf(str, "%d\n%n", &tmp_process.pid, &pos), 1, NULL);
                    process=NULL;
                    break;
                case 'c':
                    SYSTEM_SUCCESS_CHECK(sscanf(str, "%15s\n%n", tmp_process.command, &pos), 1, NULL);
                    break;
                case 'a':
                    tmp_file_opening.access=str[0];
                    str+=3; //c\nt
                    if (strncmp(str, "PSXSHM", strlen("PSXSHM"))!=0)
                    {
                        str=strchr(str, '\n')+1;
                    }
                    str=strchr(str, '\n')+1;
                    break;
                case 'n':
                    path=str;
                    str=strchr(str, '\n');
                    FOR_EACH_INV(channel_info, channels_infos, channels_nb) if (strncmp(channel_info->name, path, str-path)==0) break;
                    
                    if (channel_info != channels_infos-1)
                    {
                        if (process==NULL) // It is a new process session.
                        {
                            process=find_process(tmp_process.pid);
                            if (process==NULL)
                            {
                                APPEND_ITEM(&process, NULL, &tmp_process);
                                APPEND_ITEM(&processes, &processes_nb, &process);
                            }
                        }
                        
                        file=find_file(buf);
                        if (file==NULL)
                        {
                            tmp_file.openings=NULL;
                            tmp_file.openings_nb=0;
                            tmp_file.channel_info=*channel_info;
                            file=(struct file*)APPEND_ITEM(&files, &files_nb, &tmp_file);
                        }
                        tmp_file_opening.process=process;
                        APPEND_ITEM(&file->openings, &file->openings_nb, &tmp_file_opening);
                    }
                    str++;
                    break;
                default:
                    str=strchr(str, '\n')+1;
                    break;
            }
            str+=pos;
        }while(str[0]!=0);
        mem.allocate(0);
    }
}

static void *update_shared_files_thread(void *user_data)
{
    (void)user_data;
    while(1)
    {
        SYSTEM_ERROR_CHECK(pthread_mutex_lock(&file_update_mutex), -1, NULL);
        update_shared_files();
        g_idle_add(display_file_status, NULL);
    }
    return NULL;
}

/*
 static int sems_set_inconsitent(void *user_data)
 {
 GtkTreeIter *iter = (GtkTreeIter*)user_data;
 
 gtk_tree_store_set(GTK_TREE_STORE(channels_store),  iter,
 ACK_INCONSISTENT_COLUMN, 1,
 BLOCK_INCONSISTENT_COLUMN, 1,
 PROTECT_INCONSISTENT_COLUMN, 1, -1);
 free(iter);
 return G_SOURCE_REMOVE;
 }*/

static int refresh_each_sem (GtkTreeModel *model,  GtkTreePath *path, GtkTreeIter *iter,   gpointer user_data)
{
    blc_channel *channel;
    //    GtkTreeIter *iter_copy = ALLOCATION(GtkTreeIter);
    (void) path;
    (void)user_data;
    
    //   *iter_copy=*iter;
    gtk_tree_model_get(model,  iter, POINTER_COLUMN, &channel, -1);
    
    if (channel->sem_ack) gtk_tree_store_set(GTK_TREE_STORE(channels_store),  iter, ACK_VALUE_COLUMN, sem_is_locked(channel->sem_ack), ACK_INCONSISTENT_COLUMN, 0, -1);
    if (channel->sem_block)gtk_tree_store_set(GTK_TREE_STORE(channels_store),  iter, BLOCK_VALUE_COLUMN, sem_is_locked(channel->sem_block), BLOCK_INCONSISTENT_COLUMN, 0, -1);
    if (channel->sem_protect)gtk_tree_store_set(GTK_TREE_STORE(channels_store),  iter, PROTECT_VALUE_COLUMN, sem_is_locked(channel->sem_protect), PROTECT_INCONSISTENT_COLUMN, 0, -1);
    
    //  g_timeout_add_seconds(1, sems_set_inconsitent,iter_copy);
    return FALSE;
}

void unlink_unused_cb(GtkWidget *widget, gpointer user_data)
{
    blc_channel_info *channel_info;
    GtkTreeIter iter;
    (void)widget;
    (void)user_data;
    
    g_idle_add(clean_file_status, NULL);
    update_shared_files();
    if (gtk_tree_model_get_iter_first(GTK_TREE_MODEL(channels_store), &iter)) do
    {
        gtk_tree_model_get(GTK_TREE_MODEL(channels_store),  &iter, POINTER_COLUMN, &channel_info, -1);
        if (channel_info) if (find_file(channel_info->name)==NULL) channel_info->unlink();
    }while(gtk_tree_model_iter_next(GTK_TREE_MODEL(channels_store), &iter));
    
    g_idle_add(display_file_status, NULL);
}

void refresh_cb(GtkWidget *widget, gpointer user_data)
{
    (void)widget;
    //   gtk_tree_model_foreach(GTK_TREE_MODEL(channels_store), refresh_each_sem, user_data);
    g_idle_add(clean_file_status, NULL);
    
    pthread_mutex_unlock(&file_update_mutex);
}

void ack_sem_cb (GtkCellRendererToggle *cell_renderer, gchar *path_string, gpointer user_data)
{
    blc_channel *channel;
    GtkTreeIter *iter = ALLOCATION(GtkTreeIter); //free by inconsistent
    (void)user_data;
    
    gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(channels_store),  iter,  path_string);
    gtk_tree_model_get(GTK_TREE_MODEL(channels_store),  iter, POINTER_COLUMN, &channel, -1);
    
    if (gtk_cell_renderer_toggle_get_active( cell_renderer)) sem_post(channel->sem_block);
    else sem_trywait(channel->sem_ack);
    
    refresh_each_sem(GTK_TREE_MODEL(channels_store), NULL, iter, channel);
}

void block_sem_cb (GtkCellRendererToggle *cell_renderer, gchar *path_string, gpointer tree_model)
{
    blc_channel *channel;
    GtkTreeIter *iter = ALLOCATION(GtkTreeIter); //free by inconsistent
    
    gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(tree_model),  iter,  path_string);
    gtk_tree_model_get(GTK_TREE_MODEL(tree_model),  iter, POINTER_COLUMN, &channel, -1);
    
    if (gtk_cell_renderer_toggle_get_active( cell_renderer)) sem_post(channel->sem_block);
    else sem_trywait(channel->sem_block);
    
    gtk_tree_store_set(GTK_TREE_STORE(tree_model),  iter, BLOCK_VALUE_COLUMN, sem_is_locked(channel->sem_block), BLOCK_INCONSISTENT_COLUMN, 0, -1);
    //   g_timeout_add_seconds(1, set_inconsitent_block, iter);
}

void protect_sem_cb (GtkCellRendererToggle *cell_renderer, gchar *path_string, gpointer tree_model)
{
    blc_channel *channel;
    GtkTreeIter *iter = ALLOCATION(GtkTreeIter); //free by inconsistent
    
    gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(tree_model),  iter,  path_string);
    gtk_tree_model_get(GTK_TREE_MODEL(tree_model),  iter, POINTER_COLUMN, &channel, -1);
    
    if (gtk_cell_renderer_toggle_get_active( cell_renderer)) sem_post(channel->sem_protect);
    else sem_trywait(channel->sem_protect);
    
    gtk_tree_store_set(GTK_TREE_STORE(tree_model),  iter, PROTECT_VALUE_COLUMN, sem_is_locked(channel->sem_protect),PROTECT_INCONSISTENT_COLUMN,0,  -1);
    //   g_timeout_add_seconds(1, set_inconsitent_protect, iter);
    
}

int reload_channels(void *user_data)
{
    char tmp_dims_text[NAME_MAX];
    uint32_t tmp_uint32;
    
    blc_channel_info *info;
    //  blc_channel *channel;
    int i, fd;
    int ack_value, block_value, protect_value;
    char *type;
    char *format;
    char const *status;
    char *dims_text;
    GtkWidget *dialog;
    struct channel_context *channel_context;
    struct channel_context tmp_channel_context;
    char *variable_name = NULL;
    int channel_context_name_len;
    GtkTreeIter iter, iter_parent, *parent_pt=NULL;
    struct file *file;
    (void)user_data;
    
    
    channels_nb=0;
    FREE(channels_infos);
    FREE(channel_contexts);
    channel_contexts_nb=0;
    gtk_tree_store_clear(channels_store); // Need to free allocated data
    
    channels_nb = blc_channel_get_all_infos_with_filter(&channels_infos, start_filter_name);
    //  channels = MANY_ALLOCATIONS(channels_nb, blc_channel*);
    
    FOR (i, channels_nb)
    {
        info=&channels_infos[i];
        fd = shm_open(info->name, O_RDWR, S_IRWXU);
        
        if (fd ==-1) {
            dialog = (GtkWidget*)gtk_message_dialog_new (GTK_WINDOW(main_window), GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, "Trying to open shared memory '%s'.\nSystem error '%s'.", info->name,  g_strerror (errno));
            gtk_dialog_run (GTK_DIALOG (dialog));
            gtk_widget_destroy (dialog);
            //On le supprime de la liste
            channels_infos[i] = channels_infos[channels_nb-1];
            channels_nb--;
            i--;
            continue;
        }
        else close(fd);
        
        /*
         channel=new blc_channel(info->name);
         if (channel->sem_ack) ack_value=sem_is_locked(channel->sem_ack);
         if (channel->sem_block) block_value=sem_is_locked(channel->sem_block);
         if (channel->sem_protect) protect_value=sem_is_locked(channel->sem_protect);
         */
        
        
        asprintf(&type, "%.4s", UINT32_TO_STRING(tmp_uint32, info->type));
        asprintf(&format, "%.4s", UINT32_TO_STRING(tmp_uint32, info->format));
        
        info->sprint_dims(tmp_dims_text, sizeof(tmp_dims_text));
        dims_text=strdup(tmp_dims_text);
        
        variable_name = strchr(info->name, '.');
        if (variable_name==NULL)
        {
            variable_name=info->name; //There is no group
            parent_pt=NULL;
        }
        else
        {
            channel_context_name_len=variable_name-info->name;
            if (strncmp(info->name, start_filter_name, channel_context_name_len) == 0) parent_pt=NULL;
            else
            {
                FOR_EACH_INV(channel_context, channel_contexts, channel_contexts_nb) if (strncmp(channel_context->name, info->name , channel_context_name_len)==0) break;
                
                if (channel_context==channel_contexts-1)
                {
                    tmp_channel_context.name=strndup(info->name, channel_context_name_len);
                    gtk_tree_store_insert_with_values(channels_store, &iter_parent, NULL, -1,
                                                      NAME_COLUMN, tmp_channel_context.name, -1);
                    tmp_channel_context.iter = iter_parent;
                    channel_context=(struct channel_context*)APPEND_ITEM(&channel_contexts, &channel_contexts_nb, &tmp_channel_context);
                }
                parent_pt=&channel_context->iter;
            }
            variable_name++; // We start after the dot
        }
        
        info->sprint_dims(tmp_dims_text, sizeof(tmp_dims_text));
        dims_text=strdup(tmp_dims_text);
        
        gtk_tree_store_insert_with_values(channels_store, &iter, parent_pt, -1, NAME_COLUMN, variable_name,
                                          /*              ACK_COLUMN, channel->sem_ack, ACK_VALUE_COLUMN, ack_value,
                                           BLOCK_COLUMN, channel->sem_block, BLOCK_VALUE_COLUMN, block_value,
                                           PROTECT_COLUMN, channel->sem_protect, PROTECT_VALUE_COLUMN, protect_value,*/
                                          TYPE_COLUMN, type,
                                          FORMAT_COLUMN, format,
                                          DIMS_COLUMN, dims_text,
                                          POINTER_COLUMN, info, -1);
    }
    g_idle_add(clean_file_status, NULL);
    pthread_mutex_unlock(&file_update_mutex);
    return G_SOURCE_REMOVE;
}

void unlink_channel_cb(GtkWidget *widget, gpointer user_data)
{
    (void)widget;
    blc_channel_info *channel_info = (blc_channel_info*)user_data;
    channel_info->unlink();
}

void action_channel_cb(GtkWidget*, gpointer user_data)
{
    char  *argv[3];
    char path[PATH_MAX+1];
    struct menu_action *menu_action = (struct menu_action*)user_data;
    
    SPRINTF(path, "%s/%s", functions_dir, menu_action->program_name);
    
    argv[0]=basename(path);
    argv[1]=(char*)menu_action->positional_arguments[0].value;
    argv[2]=NULL;
    execute_process_with_iter(create_command_line(dirname(path), argv));
}

void action_input_cb(GtkWidget *widget, gpointer user_data)
{
    char  *argv[2];
    char path[PATH_MAX+1];
    struct menu_action *menu_action=(struct menu_action*)user_data;
    (void)widget;
    (void)user_data;
    
    SPRINTF(path, "%s/%s", functions_dir, menu_action->program_name);
    
    argv[0]=basename(path);
    argv[1]=NULL;
    execute_process_with_iter(create_command_line(dirname(path), argv));
}

void menu_action_input_cb(GtkWidget *widget, gpointer user_data)
{
    char  *argv[2];
    char path[PATH_MAX+1];
    struct menu_action *menu_action=(struct menu_action*)user_data;
    (void)widget;
    (void)user_data;
    
    SPRINTF(path, "%s/%s", functions_dir, menu_action->program_name);
    
    argv[0]=basename(path);
    argv[1]=NULL;
    execute_process_with_iter(create_command_line(dirname(path), argv));
}

void menu_action_channel_cb(GtkWidget *widget, gpointer user_data)
{
    struct menu_action *menu_action=(struct menu_action*)user_data;
    (void)widget;
    
    menu_action->dialog();
}

int tree_button_press_cb (GtkWidget *tree_view, GdkEventButton *event, gpointer user_data)
{
    GtkWidget *unlink_button, *action_button, *menu_action_button, *default_menu_item;
    GtkWidget *menu, *sub_menu, *menu_item;
    GtkTreePath *tree_path;
    GtkTreeIter iter;
    blc_channel_info *info;
    struct dirent program, *result;
    DIR *directory;
    struct stat status;
    int i;
    int optional_arguments_nb, positional_arguments_nb;
    blc_optional_argument *optional_arguments;
    blc_positional_argument *positional_arguments;
    
    char const  *description;
    char path[PATH_MAX+1];
    struct timeval timer;
    struct menu_action *menu_action;
    struct file *file;
    GtkTreeViewColumn *column;
    char message[NAME_MAX];
    
    (void)tree_view;
    (void)user_data;
    
    
    if (event->button==GDK_BUTTON_PRIMARY)
    {
        gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(tree_view), event->x, event->y, &tree_path, &column, NULL, NULL);
        
        if (tree_path!=NULL)
        {
            gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(channels_store),  &iter,  gtk_tree_path_to_string(tree_path));
            if (gtk_tree_view_column_cell_get_position(column, used_renderer, NULL, NULL))
            {
                gtk_tree_model_get(GTK_TREE_MODEL(channels_store), &iter, POINTER_COLUMN, &info, -1);
                
                file=find_file(info->name);
                if (file){
                    menu = gtk_menu_new ();
                    FOR(i, file->openings_nb)
                    {
                        SPRINTF(message, "%d:%s...",  file->openings[i].process->pid, file->openings[i].process->command);
                        menu_item=gtk_menu_item_new_with_label(message);
                        gtk_widget_set_sensitive(menu_item, FALSE);
                        gtk_container_add(GTK_CONTAINER(menu), menu_item) ;
                    }
                    
                    gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, GDK_BUTTON_PRIMARY, event->time);
                    gtk_widget_show_all(menu);
                }
            }
        }
    }
    else  if (event->button==GDK_BUTTON_SECONDARY)
    {
        menu = gtk_menu_new ();
        unlink_button = gtk_menu_item_new_with_label("unlink");
        gtk_container_add(GTK_CONTAINER(menu),  unlink_button);
        gtk_container_add(GTK_CONTAINER(menu),  gtk_separator_menu_item_new());
        
        gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(channels_tree), event->x, event->y, &tree_path, NULL, NULL, NULL);
        gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(channels_store),  &iter,  gtk_tree_path_to_string(tree_path));
        gtk_tree_model_get(GTK_TREE_MODEL(channels_store), &iter, POINTER_COLUMN, &info, -1);
        
        CLEAR(timer);
        us_time_diff(&timer);
        
        SPRINTF(functions_dir, "%s/bin", blaa_dir);
        SYSTEM_ERROR_CHECK(directory = opendir(functions_dir), NULL, "Opening dir '%s'.", functions_dir);
        do
        {
            if (readdir_r(directory, &program, &result) != 0)  EXIT_ON_ERROR("readdir_r error parsing dir.");
            else if (result)
            {
                if (result->d_type==DT_REG && ( strncmp(result->d_name, "f_", 2)==0))
                {
                    fstat(result->d_reclen, &status);
                    if (1)//status.st_mode & S_IXUSR) ???
                    {
                        SPRINTF(path, "%s/%s", functions_dir, result->d_name);
                        description=blc_parse_help(path, &optional_arguments, &optional_arguments_nb, &positional_arguments, &positional_arguments_nb, NULL);
                        
                        action_button=gtk_menu_item_new_with_label(result->d_name);
                        gtk_container_add(GTK_CONTAINER(menu), action_button);
                        gtk_widget_set_tooltip_text(action_button, description);

                        menu_action=new struct menu_action(result->d_name, description, optional_arguments, optional_arguments_nb, positional_arguments, positional_arguments_nb);
                        menu_action->positional_arguments[0].value=strdup(info->name);
                        
                        if (optional_arguments_nb > 1)
                        {
                            default_menu_item=gtk_menu_item_new_with_label("default");
                            menu_action_button = gtk_menu_item_new_with_label("options ...");
                            sub_menu = gtk_menu_new ();
                            gtk_container_add(GTK_CONTAINER(sub_menu), default_menu_item);
                            gtk_container_add(GTK_CONTAINER(sub_menu), menu_action_button);
                            gtk_menu_item_set_submenu(GTK_MENU_ITEM(action_button), sub_menu);
                            gtk_widget_set_events(action_button,  GDK_BUTTON_PRESS_MASK);

                            g_signal_connect(G_OBJECT(default_menu_item), "activate", G_CALLBACK(action_channel_cb), menu_action);
                            g_signal_connect(G_OBJECT(menu_action_button), "activate", G_CALLBACK(menu_action_channel_cb), menu_action);
                        }
                        else  g_signal_connect(G_OBJECT(action_button), "activate", G_CALLBACK(action_channel_cb), menu_action);
                    }
                }
            }
        }while(result);
        closedir(directory);
        gtk_widget_show_all(menu);
        
        gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, GDK_BUTTON_PRIMARY, event->time);
        g_signal_connect(G_OBJECT(unlink_button), "activate", G_CALLBACK(unlink_channel_cb), info);
        
        return true;
    }
    return false;
}

void input_menu_update(GtkWidget *menu_shell)
{
    GtkWidget  *action_button, *menu, *default_item;
    
    DIR *directory;
    struct stat status;
    struct dirent program, *result;
    char path[PATH_MAX+1];
    char const *description;
    char token[NAME_MAX+1];
    int positional_arguments_nb, optional_arguments_nb;
    struct menu_action *menu_action;
    struct blc_optional_argument *optional_arguments;
    struct blc_positional_argument *positional_arguments;
    
    SPRINTF(functions_dir, "%s/bin", blaa_dir);
    SYSTEM_ERROR_CHECK(directory = opendir(functions_dir), NULL, "Opening dir '%s'.", functions_dir);
    do
    {
        if (readdir_r(directory, &program, &result) != 0)  EXIT_ON_ERROR("readdir_r error parsing dir.");
        else if (result){
            if (result->d_type==DT_REG && ( strncmp(result->d_name, "i_", 2)==0)){
                fstat(result->d_reclen, &status);
                SPRINTF(path, "%s/%s", functions_dir, result->d_name);
                description=blc_parse_help(path, &optional_arguments, &optional_arguments_nb, &positional_arguments, &positional_arguments_nb, NULL);
                
                default_item=NULL;
                if (positional_arguments_nb==0)
                {
                    default_item = gtk_menu_item_new_with_label(result->d_name);
                }
                
                if (positional_arguments_nb || optional_arguments_nb>1)
                {
                    menu_action=new struct menu_action(result->d_name, description, optional_arguments, optional_arguments_nb, positional_arguments, positional_arguments_nb);
                    if (default_item)
                    {
                        menu=gtk_menu_new();
                        gtk_container_add(GTK_CONTAINER(menu_shell), default_item);
                        gtk_menu_item_set_submenu(GTK_MENU_ITEM(default_item), menu);
                        default_item = gtk_menu_item_new_with_label("default");
                        action_button = gtk_menu_item_new_with_label("options ...");
                        gtk_container_add(GTK_CONTAINER(menu), default_item);
                        gtk_container_add(GTK_CONTAINER(menu), action_button);
                        g_signal_connect(G_OBJECT(default_item), "activate", G_CALLBACK(menu_action_input_cb), menu_action);
                        
                    }
                    else
                    {
                        SPRINTF(token, "%s ...",result->d_name);
                        action_button = gtk_menu_item_new_with_label(token);
                        gtk_container_add(GTK_CONTAINER(menu_shell), action_button);
                        
                    }
                    g_signal_connect(G_OBJECT(action_button), "activate", G_CALLBACK(menu_action_channel_cb), menu_action);
                }
                else
                {
                    if (default_item)
                    {
                        menu_action=new struct menu_action(result->d_name, description);
                        gtk_container_add(GTK_CONTAINER(menu_shell), default_item);
                        g_signal_connect(G_OBJECT(default_item), "activate", G_CALLBACK(menu_action_input_cb), menu_action);
                    }
                }
                
            }
        }
    }while(result);
    closedir(directory);
}

static void *check_blc_channel_change(void *user_data)
{
    sem_t *sems[2];
    int i=0;
    (void)user_data;
    
    SYSTEM_ERROR_CHECK(sems[0] = sem_open("blc_channels_sem1", O_CREAT | O_RDWR, S_IRWXU, 0), NULL, "Creating blc_channels_sem1");
    SYSTEM_ERROR_CHECK(sems[1] = sem_open("blc_channels_sem2", O_CREAT | O_RDWR, S_IRWXU, 0), NULL, "Creating blc_channels_sem2");
    
    while(1)
    {
        sem_wait(sems[i]);
        while(sem_trywait(sems[1-i]) == 0); //We are sure to be locked before continuing
        sem_post(sems[i]);
        gdk_threads_add_idle(reload_channels, NULL);
        i=1-i;
    }
    return NULL;
}

GtkWidget *create_channels_display()
{
    pthread_t thread;
    GtkWidget *box, *toolbar, *frame, *scrolled_window, *add_input_menu;
    GtkCellRenderer *renderer;
    GtkRequisition natural_size, minimum_size;
    
    frame=gtk_frame_new("Channels");
    gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
    
    scrolled_window=gtk_scrolled_window_new(NULL, NULL);
    box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 1);
    
    toolbar = gtk_toolbar_new();
    gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_BOTH);
    
    channels_store = gtk_tree_store_new(COLUMNS_NB,  G_TYPE_STRING, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_POINTER);
    SYSTEM_SUCCESS_CHECK(pthread_create(&thread, NULL,  check_blc_channel_change, NULL), 0, "Creating check_blc_channel_change thread.");
    
    reload_channels(NULL);
    
    channels_tree= gtk_tree_view_new_with_model (GTK_TREE_MODEL (channels_store));
    
    renderer = gtk_cell_renderer_text_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (channels_tree), -1, "name", renderer, "text", NAME_COLUMN, NULL);
    
    renderer = gtk_cell_renderer_toggle_new();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (channels_tree), -1, "ack", renderer, "visible", ACK_COLUMN, "active", ACK_VALUE_COLUMN, "inconsistent", ACK_INCONSISTENT_COLUMN, NULL);
    g_signal_connect(G_OBJECT(renderer), "toggled", G_CALLBACK(ack_sem_cb), channels_store);
    
    renderer = gtk_cell_renderer_toggle_new();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (channels_tree), -1, "block", renderer, "visible", BLOCK_COLUMN, "active", BLOCK_VALUE_COLUMN, "inconsistent", BLOCK_INCONSISTENT_COLUMN , NULL);
    g_signal_connect(G_OBJECT(renderer), "toggled", G_CALLBACK(block_sem_cb), channels_store);
    
    renderer = gtk_cell_renderer_toggle_new();
    gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW (channels_tree), -1, "protect", renderer, "visible", PROTECT_COLUMN, "active", PROTECT_VALUE_COLUMN, "inconsistent", PROTECT_INCONSISTENT_COLUMN, NULL);
    g_signal_connect(G_OBJECT(renderer), "toggled", G_CALLBACK(protect_sem_cb), channels_store);
    
    renderer = gtk_cell_renderer_text_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (channels_tree), -1,"type", renderer, "text", TYPE_COLUMN, NULL);
    
    renderer = gtk_cell_renderer_text_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (channels_tree), -1, "format", renderer, "text", FORMAT_COLUMN, NULL);
    
    renderer = gtk_cell_renderer_text_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (channels_tree), -1, "dims", renderer, "text", DIMS_COLUMN, NULL);
    
    used_renderer = gtk_cell_renderer_pixbuf_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (channels_tree), -1, "used", used_renderer, "icon-name", USED_COLUMN,  NULL);
    
    gtk_widget_get_preferred_size(channels_tree, &minimum_size, &natural_size);
    gtk_widget_set_size_request(scrolled_window, natural_size.width+100, natural_size.height+100);
    
    gtk_widget_set_vexpand(frame, 1);
    gtk_widget_set_vexpand(channels_tree, 1);
    
    gtk_widget_set_vexpand(scrolled_window, 1);
    gtk_widget_set_hexpand(scrolled_window, 1);
    
    gtk_container_add(GTK_CONTAINER(box), toolbar);
    gtk_container_add(GTK_CONTAINER(scrolled_window), channels_tree);
    
    gtk_container_add(GTK_CONTAINER(box), scrolled_window);
    gtk_container_add(GTK_CONTAINER(frame), box);
    
    add_input_menu=gtk_menu_new();
    input_menu_update(add_input_menu);
    
    blgtk_add_tool_button(toolbar, "refresh sem", "view-refresh", G_CALLBACK(refresh_cb), NULL);
    blgtk_add_tool_button(toolbar, "unlink\nunused", "edit-delete", G_CALLBACK(unlink_unused_cb), NULL);
    blgtk_add_menu_tool_button(toolbar, "inputs", "insert-object", add_input_menu);
    gtk_widget_show_all(channels_tree);
    gtk_widget_show_all(add_input_menu);
    g_signal_connect(G_OBJECT(channels_tree), "button-release-event", G_CALLBACK(tree_button_press_cb), NULL);
    
    SYSTEM_ERROR_CHECK(pthread_create(&thread, NULL, update_shared_files_thread, NULL), -1, NULL);
    
    return frame;
}



