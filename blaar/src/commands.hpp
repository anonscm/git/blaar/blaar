

#ifndef COMMANDS_HPP
#define COMMANDS_HPP
#include <gtk/gtk.h>


struct watch_process{
    char const *command_line;
    pid_t pid;
    GtkTreeIter iter;
    int stderr_pipe[2], stdout_pipe[2], stdin_pipe[2];
    GtkWidget *terminal, *error_terminal, *terminal_window;
};

void open_commands(char const *filename);
void save_commands(char const *filename);
GtkTreeIter *create_command_line(char const *path, char *const *argv);
void execute_process_with_iter(GtkTreeIter *iter);
GtkWidget *create_commands_display();

extern GtkTreeStore *commands_store;

#endif