#include "processes.hpp"
#include "commands.hpp"

#include "blc.h"
#include "main.hpp"
#include "blgtk.h"

#include <sys/wait.h>
#include <libgen.h>
#include <errno.h>
#include <pthread.h>

enum { WATCH_PROCESS_COLUMN, DIR_COLUMN, COMMAND_LINE_COLUMN, PCPU_COLUMN, MEM_COLUMN, PMEM_COLUMN, COLUMNS_NB};

static struct tree_view_column{
    int id;
    char const *name;
    int type;
} columns[]={
    {WATCH_PROCESS_COLUMN, NULL, G_TYPE_POINTER},
    {DIR_COLUMN, NULL, G_TYPE_STRING},
    {COMMAND_LINE_COLUMN, NULL, G_TYPE_STRING},
    {PCPU_COLUMN, NULL, G_TYPE_FLOAT},
    {MEM_COLUMN, NULL, G_TYPE_INT},
    {PMEM_COLUMN, NULL, G_TYPE_FLOAT},
  };

static GtkTreeStore *processes_store;
static GtkWidget *tree_view, *scrolled_window;
//static GtkCellRenderer *status_renderer, *stderr_renderer, *stdout_renderer, *stdin_renderer;
static guint refresh_timer_id;
static int manage_all_processes=0;

/*
static void processes_tree_button_press_cb(GtkWidget *tree_view, GdkEventButton *event, gpointer user_data)
{
    char *command_line, *path;
    char *const* argv;
    GtkWidget *terminal_box;
    GtkTreeViewColumn *column;
    GtkTreePath *tree_path;
    GtkTreeIter iter;
    struct watch_process *wp;
    (void)tree_view;
    (void)event;
    (void)user_data;
    
    if (event->button==GDK_BUTTON_PRIMARY)
    {
        gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(tree_view), event->x, event->y, &tree_path, &column, NULL, NULL);
        gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(processes_store),  &iter,  gtk_tree_path_to_string(tree_path));
        
        if (gtk_tree_view_column_cell_get_position(column, status_renderer, NULL, NULL))
        {
            gtk_tree_model_get(GTK_TREE_MODEL(processes_store), &iter,
                               WATCH_PROCESS_COLUMN, &wp, -1);
            if (wp==NULL)
            {
                gtk_tree_model_get(GTK_TREE_MODEL(processes_store), &iter,
                                   DIR_COLUMN, &path,
                                   COMMAND_LINE_COLUMN, &command_line, -1);
                argv=create_argv_from_command_line(command_line);
                
                //    execute_process_with_iter(&iter);
            }
            else   kill(wp->pid, SIGTERM);
        }
        else if (gtk_tree_view_column_cell_get_position(column, stdout_renderer, NULL, NULL)
                 || gtk_tree_view_column_cell_get_position(column, stderr_renderer, NULL, NULL)
                 || gtk_tree_view_column_cell_get_position(column, stdin_renderer, NULL, NULL))
            
        {
            gtk_tree_model_get(GTK_TREE_MODEL(processes_store), &iter,
                               WATCH_PROCESS_COLUMN, &wp, -1);
            
            if (wp->terminal_window==NULL)
            {
                wp->terminal_window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
                terminal_box=gtk_box_new(GTK_ORIENTATION_VERTICAL, 1);
                gtk_container_add(GTK_CONTAINER(terminal_box), wp->terminal);
                gtk_container_add(GTK_CONTAINER(terminal_box), wp->error_terminal);
                gtk_container_add(GTK_CONTAINER(wp->terminal_window), terminal_box);
                g_signal_connect(wp->terminal_window, "delete-event", G_CALLBACK(gtk_widget_hide_on_delete), NULL);
                gtk_widget_show_all(wp->terminal_window);
            }
            else gtk_widget_set_visible(wp->terminal_window, 1);
        }
    }
}*/



static void refresh_processes()
{
    char full_command_line[LINE_MAX];
    float pcpu;
    int process_mem;
    float pmem;
    blc_mem mem;
    char buf[4096];
    int stdout_pipe[2], n, status, length, pos;
    int width;
    pid_t pid;
    GList *columns, *element;
    char **argv=NULL;
    int argc=0;
    GtkTreeIter iter;
    int ret;
    struct watch_process *wp;
    char  *arg;
    int found;
    char *command_line, *line;
    
    gtk_tree_store_clear(processes_store);
    SYSTEM_ERROR_CHECK(pipe(stdout_pipe), -1, NULL);
    
    pid=fork();
    if (pid==0)
    {
        add_arg(&argc, &argv, "ps", NULL);
        add_arg(&argc, &argv, "-o", "%cpu");
        add_arg(&argc, &argv, "-o", "rss");
        add_arg(&argc, &argv, "-o", "%mem");
        add_arg(&argc, &argv, "-o", "command");
        
        if (manage_all_processes) add_arg(&argc, &argv, "-A", NULL);
        else
        {
            found=0;
            for (ret=gtk_tree_model_get_iter_first(GTK_TREE_MODEL(commands_store), &iter); ret==TRUE ; ret = gtk_tree_model_iter_next(GTK_TREE_MODEL(commands_store), &iter))
            {
                gtk_tree_model_get(GTK_TREE_MODEL(commands_store), &iter, WATCH_PROCESS_COLUMN, &wp, -1);
                
                if (wp->pid !=0)  {
                    asprintf(&arg, "%d", wp->pid);  //Do not need to be freed, it will be freed when execvp
                    add_arg(&argc, &argv, "-p", arg);
                    found=1;
                }
            }
            if (!found) add_arg(&argc, &argv, "-p", "0");
        }
        add_arg(&argc, &argv, NULL, NULL);
        command_line=create_command_line_from_argv(argv);
        
        SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(stdout_pipe[1], STDOUT_FILENO), -1, EINTR, NULL);
        blc_close_pipe(stdout_pipe);

        SYSTEM_ERROR_CHECK(execvp("/bin/ps", argv), -1, NULL);
    }
    else
    {
        close(stdout_pipe[1]);
        do
        {
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(n=read(stdout_pipe[0], buf, sizeof(buf)), -1, EINTR, "read ps stdout");
            mem.append(buf, n);
        }while(n!=0);
        close(stdout_pipe[0]);

        mem.append("", 1); //end of string
        waitpid(pid, &status, 0);

        sscanf(mem.data, "%*[^\n]\n%n", &pos); // remove title
        if (mem.size>1)
        {
            line=mem.data+pos;
            while((ret=sscanf(line, " %f %d %f %[^\n]\n%n",  &pcpu, &process_mem, &pmem, full_command_line, &length))==4)
            {
           //     command=basename(full_command_line);
              //  dir=dirname(full_command_line); // It is never read ?...
            
                gtk_tree_store_insert_with_values(processes_store, NULL, NULL, -1, COMMAND_LINE_COLUMN, full_command_line,
                                              PCPU_COLUMN, pcpu,
                                              MEM_COLUMN, process_mem,
                                              PMEM_COLUMN, pmem, -1);
                line+=length;
            }
        }
        mem.allocate(0);
    }
    
    columns=gtk_tree_view_get_columns(GTK_TREE_VIEW(tree_view));
    width=10;
    for(element=columns; element != NULL; element=element->next)
    {
        width+=gtk_tree_view_column_get_width(GTK_TREE_VIEW_COLUMN(element->data));
    }
     g_list_free(columns);
        
    
  //  gtk_widget_get_preferred_size(tree_view, &minimum_size, &natural_size);
   // gtk_widget_set_size_request(scrolled_window, width+10, natural_size.height+10);
  //  gtk_window_resize(GTK_WINDOW(main_window), width+10+400, 800);

}

static gboolean repeat_refresh(gpointer user_data)
{
    (void)user_data;
    
    refresh_processes();
    return G_SOURCE_CONTINUE;

}

static void refresh_processes_cb(GtkToolButton *toolbutton, gpointer *user_data)
{
    (void) toolbutton;
    (void) user_data;
    refresh_processes();
}

static void repeat_refresh_cb(GtkToolButton *toolbutton, gpointer *user_data)
{
    if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toolbutton)))  refresh_timer_id=g_timeout_add_seconds(1, repeat_refresh, user_data);
    else g_source_remove(refresh_timer_id);
}

static void all_processes_cb(GtkToolButton *toolbutton, gpointer *user_data)
{
    (void)user_data;
    if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toolbutton)))
        manage_all_processes=1;
    else manage_all_processes=0;
    refresh_processes();
}

static void  copy_command(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter,  gpointer data)
{
    char *command_line;
    char *const *argv;
    char *const *arg_tmp;
    (void) path;
    (void) data;
 
    gtk_tree_model_get(model, iter, COMMAND_LINE_COLUMN, &command_line);
    
    argv = create_argv_from_command_line(command_line);
    create_command_line("", argv);
    
    for (arg_tmp=argv; (*arg_tmp)!=NULL; arg_tmp++)
    {
        free(*arg_tmp);
    }
    free((void*)argv);
}

static void copy_command_cb(GtkToolButton *toolbutton, gpointer *user_data)
{
    GtkTreeSelection *selection;
    (void)toolbutton;
    (void)user_data;

    selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(tree_view));
    gtk_tree_selection_selected_foreach (selection, copy_command, NULL);
}

GtkWidget *create_processes_display() {
    GtkWidget *frame;
    GtkCellRenderer *command_renderer;
    GtkToggleToolButton *repeat_refresh_tool_button;
    GtkWidget  *box, *toolbar;
    GType types[COLUMNS_NB];
    GtkTreeViewColumn *column;
    GtkCellRenderer *renderer;
    
    int i;
    
    FOR(i, COLUMNS_NB) {
        if (i==columns[i].id) types[i]=columns[i].type;
        else EXIT_ON_ERROR("The order and id of columns does not fit.");
    }
    
    toolbar=gtk_toolbar_new();
    gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_BOTH);
    
    blgtk_add_tool_button(toolbar, "refresh\nprocesses", "view-refresh", G_CALLBACK(refresh_processes_cb), NULL);
    repeat_refresh_tool_button=blgtk_add_toggle_tool_button(toolbar, "repeat\nrefresh", "media-playlist-repeat", G_CALLBACK(repeat_refresh_cb), NULL);
    blgtk_add_toggle_tool_button(toolbar, "all\nprocesses", "edit-select-all", G_CALLBACK(all_processes_cb), NULL);
    blgtk_add_tool_button(toolbar, "copy\ncommands", "accessories-text-editor", G_CALLBACK(copy_command_cb), NULL);
    
    gtk_toggle_tool_button_set_active(repeat_refresh_tool_button, TRUE);

    box=gtk_box_new(GTK_ORIENTATION_VERTICAL, TRUE);
    scrolled_window=gtk_scrolled_window_new(NULL, NULL);
    frame=gtk_frame_new("Running processes");
    gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
    processes_store = gtk_tree_store_newv(COLUMNS_NB, types);
    tree_view=gtk_tree_view_new_with_model(GTK_TREE_MODEL(processes_store));
    
    renderer = gtk_cell_renderer_text_new ();
    gtk_cell_renderer_set_alignment(GTK_CELL_RENDERER(renderer), 1.0, 0.5);
    column=gtk_tree_view_column_new_with_attributes( "%cpu", renderer,  "text", PCPU_COLUMN, NULL);
    gtk_tree_view_column_set_sort_indicator(column, TRUE);
    gtk_tree_view_column_set_sort_column_id(column, PCPU_COLUMN);
    gtk_tree_view_insert_column (GTK_TREE_VIEW(tree_view), column, -1);
    
    renderer = gtk_cell_renderer_text_new ();
    gtk_cell_renderer_set_alignment(GTK_CELL_RENDERER(renderer), 1.0, 0.5);
    column=gtk_tree_view_column_new_with_attributes( "mem(rss)", renderer,  "text", MEM_COLUMN, NULL);
    gtk_tree_view_column_set_sort_indicator(column, TRUE);
    gtk_tree_view_column_set_sort_column_id(column, MEM_COLUMN);
    gtk_tree_view_insert_column (GTK_TREE_VIEW(tree_view), column, -1);
    
    renderer = gtk_cell_renderer_text_new ();
    gtk_cell_renderer_set_alignment(GTK_CELL_RENDERER(renderer), 1.0, 0.5);

    column=gtk_tree_view_column_new_with_attributes( "%mem", renderer,  "text", PMEM_COLUMN, NULL);
    gtk_tree_view_column_set_sort_indicator(column, TRUE);
    gtk_tree_view_column_set_sort_column_id(column, PMEM_COLUMN);
    gtk_tree_view_insert_column (GTK_TREE_VIEW(tree_view), column, -1);
    
    command_renderer = gtk_cell_renderer_text_new();
    column=gtk_tree_view_column_new_with_attributes( "command line", command_renderer,  "text", COMMAND_LINE_COLUMN, NULL);
    gtk_tree_view_column_set_sort_indicator(column, TRUE);
    gtk_tree_view_column_set_sort_order(column,GTK_SORT_ASCENDING);
    gtk_tree_view_column_set_sort_column_id(column, COMMAND_LINE_COLUMN);
    gtk_tree_view_insert_column (GTK_TREE_VIEW(tree_view), column, -1);
    
    gtk_container_add(GTK_CONTAINER(scrolled_window), tree_view);
    gtk_container_add(GTK_CONTAINER(box), toolbar);
    gtk_container_add(GTK_CONTAINER(box), scrolled_window);
    gtk_container_add(GTK_CONTAINER(frame), box);
    gtk_widget_set_hexpand(scrolled_window, 1);
    gtk_widget_set_vexpand(scrolled_window, 1);

    refresh_processes();
    
    return frame;
}



