FAQ
===

- **The text display ('d') does not work properly** : make sure you are not using `rlwrap`. Rlwrap is used by default in `test.sh`.