//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include "main.h"
#include "blc.h"
#include "blcl.h"
#include "blQTKit.h"
#include <sys/signal.h>
#include <stdlib.h>
#include <unistd.h>

enum {RUN, PAUSE, STOP, STATUS_NB};
struct blcl_target gpu_target;
size_t work_group_sizes[2];
size_t items_nbs[2];
int subimage=0;
//uint32_t output_format;

/**
 @main
 Exemple of using the console to display the camera
 */
int status=RUN;
double period;
int iteration=0;
struct timeval timestamp={};
char const *force_ansi=NULL;
blc_narray input;
int scale;
char  const *channel_name;
blc_channel *filter=NULL;
blc_channel output;
int pipe_mode;

static int initialised = 0;

struct timeval global_time;
size_t image_size=0;
size_t subimage_offset;

char const *Y0_code="y0";
char const *U_code="u";
char const *Y1_code="y1";
char const *V_code="v";
char *source_code=NULL;

int refresh_filter=1;
cl_mem cl_input, cl_output, cl_mask;
cl_kernel kernel;

int display=0, is_zoom=0;

char *activation=(char*)"convert_uchar_sat(p)";
int terminal_width, terminal_height;

int output_width, output_height, input_width, input_height;
// Simple compute kernel which computes the square of an input array
int offset_x, offset_y, center_x=-1, center_y=-1;


void terminal_resize_cb(int w, int h){
    terminal_width=w;
    terminal_height=h;
}

cl_kernel update_kernel(blcl_target *target, blc_channel *output, blc_narray *input, blc_channel *filter)
{
    char  *defines, *source_code;
    cl_int error_id;
    cl_kernel kernel;
    size_t work_group_size;
    
    asprintf(&defines,
             "#define TILE_WIDTH 34\n#define TILE_HEIGHT 18\n"\
             "#define FILTER_WIDTH %d\n#define FILTER_HEIGHT %d\n"\
             "#define INPUT_WIDTH %d\n#define INPUT_HEIGHT %d\n"\
             "#define OUTPUT_WIDTH %d\n#define OUTPUT_HEIGHT %d\n", filter->lengths[0],  filter->lengths[1], input->lengths[1], input->lengths[2], output->lengths[0], output->lengths[1]);
    asprintf(&source_code,\
             "%s\n"\
             "kernel void filter(global uchar *output, global uchar2 *input, constant float *filter){\n" \
             "int i,j;\n"\
             "   uchar y0, u, y1, v;\n"\
             "   uchar Y0, U, Y1, V;\n"\
             "   uchar y01, u1, y11, v1;\n"\
             "   local uchar16 mem[TILE_WIDTH][TILE_HEIGHT];\n"\
             "   local uchar dxp[TILE_WIDTH-1][TILE_HEIGHT-2], dxn[TILE_WIDTH-1][TILE_HEIGHT-2];\n"\
             "   local uchar dyp[TILE_WIDTH-2][TILE_HEIGHT-1], dyn[TILE_WIDTH-2][TILE_HEIGHT-1];\n"\
             "   local uchar ON[TILE_WIDTH-2][TILE_HEIGHT-2];\n"\
             "   local uchar OFF[TILE_WIDTH-2][TILE_HEIGHT-2];\n"\
             "   uchar tmp0, tmp1, tmp2, tmp3;\n"\
             "   size_t x = get_global_id(0)+1;\n"\
             "   size_t y= get_global_id(1)+1;\n"\
             "   size_t k=get_local_id(0)+1;\n"\
             "   size_t l=get_local_id(1)+1;\n"\
             "   int val\n;"\
             "   mem[k][l]=input[x+y*INPUT_WIDTH].y;\n"\
             "   if (k==1){\n"\
             "     mem[0][l]=input[x-1+y*INPUT_WIDTH].y; \n"\
             "     if (l==1) mem[k-1][l-1]=input[x-1+(y-1)*INPUT_WIDTH].y; \n"\
             "     else if (l==TILE_HEIGHT-2) mem[k-1][l+1]=input[x-1+(y+1)*INPUT_WIDTH].y; \n"\
             "   }else if (k==TILE_WIDTH-2){\n"\
             "     mem[k+1][l]=input[x+1+(y)*INPUT_WIDTH].y; \n"\
             "     if (l==1) mem[k+1][l-1]=input[x+1+(y-1)*INPUT_WIDTH].y; \n"\
             "     else if (l==TILE_HEIGHT-2) mem[k+1][l+1]=input[x+1+(y+1)*INPUT_WIDTH].y; \n"\
             "   }\n"\
             "   if (l==1) mem[k][l-1]=input[x+(y-1)*INPUT_WIDTH].y; \n"\
             "   else if (l==TILE_HEIGHT-2) mem[k][l+1]=input[x+(y+1)*INPUT_WIDTH].y; \n"\
             "   barrier(CLK_LOCAL_MEM_FENCE);\n"\
             "   val=mem[k][l]-mem[k-1][l];\n"\
             "   if (k==1) val=mem[k][l]-mem[k-1][l];\n"\
             "   if (val==0) dxp[k][l]=dxn[k][l]=0;\n"\
             "   else if (val>0){\n"\
             "      dxp[k][l]=val;\n"\
             "      dxn[k][l]=0;\n"\
             "   } else {\n"\
             "      dxp[k][l]=0;\n"\
             "      dxn[k][l]=-val;\n"\
             "   }\n"\
             "   val=mem[k][l]-mem[k][l-1];\n"\
             "   if (val==0) dyp[k][l]=dyn[k][l]=0;\n"\
             "   else if (val>0){\n"\
             "      dyp[k][l]=val;\n"\
             "      dyn[k][l]=0;\n"\
             "   } else {\n"\
             "      dyp[k][l]=0;\n"\
             "      dyn[k][l]=-val;\n"\
             "   }\n"\
             "   barrier(CLK_LOCAL_MEM_FENCE);\n"\
             "   ON[k-1][l-l]=convert_uchar_sat(dxp[k][l]+dxn[k+1][l]+dyp[k][l]+dyn[k][l+1]);\n"\
             "   OFF[k-1][l-1]=convert_uchar_sat(dxp[k+1][l]+dxn[k][l]+dyp[k][l+1]+dyn[k][l]))\n"\
             "   output[(x-1)+(y-1)*OUTPUT_WIDTH]=convert_uchar_sat((dxp[k][l]+dxn[k+1][l]+dyp[k][l]+dyn[k][l+1])+(dxp[k+1][l]+dxn[k][l]+dyp[k][l+1]+dyn[k][l]));\n"\
             "  // output[(x-1)+(y-1)*OUTPUT_WIDTH]=dyp[k][l]+128-dyn[k][l];\n"\
             "}\n", defines);
    
    
    BLCL_ERROR_CHECK(cl_output=clCreateBuffer(target->context, CL_MEM_WRITE_ONLY, output->size, NULL, &error_id), NULL, error_id,  NULL);
    BLCL_ERROR_CHECK(cl_input=clCreateBuffer(target->context, CL_MEM_READ_ONLY, input->size, NULL, &error_id), NULL, error_id,  NULL);
    BLCL_ERROR_CHECK(cl_mask=clCreateBuffer(target->context, CL_MEM_READ_ONLY, filter->size, NULL, &error_id), NULL, error_id,  NULL);
    BLCL_CHECK(clEnqueueWriteBuffer(target->command_queue, cl_mask, CL_TRUE, 0, filter->size, filter->data, 0, NULL, NULL ), "Failed to write input array!");
    kernel=BLCL_CREATE_KERNEL_3P(target, "filter", source_code, cl_output, cl_input, cl_mask);
    
    free(defines);
    free(source_code);
    return kernel;
}

static void command_callback(char const *command, char const *, void *)
{
    uint32_t input_format_string, output_format_string;
    int i;
    
    switch (*command)
    {
        case 'd':
            display=1-display;
            fprintf(stderr, "\n");
            FOR(i, BLC_BAR_COLORS_NB-1) color_printf(blc_bar_colors[i],"%d < ", i*256/BLC_BAR_COLORS_NB);
            color_printf(blc_bar_colors[i],"%d", i*256/BLC_BAR_COLORS_NB);
            break;
        case 'i':
            input_format_string=htonl(input.format);
            output_format_string=htonl(output.format);
            fprintf(stderr, "=== INFO: %s input:%.4s output:%.4s ===\n", blc_program_name, (char*)&input_format_string, (char*)&output_format_string);
            printf("i");
            fflush(stdout);break;
    }
}


static void edit_callback(char const *, char const *argument, void *user_data)
{
    struct timeval time={0, 0};
    if (strlen(argument)==0)  fprintf(stderr, "=%s\n", *((char**)user_data));
    else {
        //    free(user_data);
        *((char**)user_data)=strdup(argument);
        us_time_diff(&time);
        //  BLCL_CREATE_KERNEL_4P("square", update_source_code(), cl_output, cl_inputs, size, frame);
        fprintf(stderr, "Recompilation time %ld µs\n", us_time_diff(&time));
    }
}


void blc_display_text_image(blc_narray *image, int width, int height)
{
    int component_offset, offset, step1, step2;
    switch (image->format)
    {
        case 'yuv2':
            component_offset=1;
            if (is_zoom)
            {
                step1=2;
                step2=width*2;
                offset=component_offset+(int)((image->lengths[1]-width/2)/2)*2+((int)(image->lengths[2]-height)/2)*step2;
            }
            else
            {
                step1 =(int)(image->lengths[1]/(width/2))*2;
                step2=(int)(image->lengths[2]/height)*image->lengths[1]*2;
                offset=component_offset;
            }
            fprint_3Dmatrix(stderr, image, offset, 2, 1, step1, width/2, step2, height, 1);
            break;
    }
}

void capture_cb(CVImageBufferRef videoFrame, void *)
{
    uint32_t format_str, input_format_str, output_format_str;
    int bytes_per_pixel, i, j;
    
    if (initialised==0){
        input.format = ntohl(CVPixelBufferGetPixelFormatType(videoFrame));
        output.type='UIN8';
        
        if (output.format=='NDEF') output.format = input.format;
        bytes_per_pixel=blc_get_bytes_per_pixel(&input);
        
        if (bytes_per_pixel!=1) input.add_dim(bytes_per_pixel);
        input.add_dim(CVPixelBufferGetWidth(videoFrame));
        input.add_dim(CVPixelBufferGetHeight(videoFrame));
        
        bytes_per_pixel=blc_get_bytes_per_pixel(&output);
        if (bytes_per_pixel!=1) output.add_dim(bytes_per_pixel);
        
        input_width=input.lengths[input.dims_nb-2];
        input_height=input.lengths[input.dims_nb-1];
        
        if (subimage==0){
            if (filter){
                output_width=input_width-filter->lengths[0]+1;
                output_height=input_height-filter->lengths[1]+1;
            }
            else{
                output_width=input_width;
                output_height=input_height;
            }
        }
        else{
            if (center_x==-1) center_x=input_width/2;
            if (center_y==-1) center_y=input_height/2;
            
            offset_x=MAX(center_x-output_width/2, 0);
            offset_y=MAX(center_y-output_height/2, 0);
            offset_x=MIN(offset_x, input_width-output_width);
            offset_y=MIN(offset_y, input_height-output_height);
        }
        output.add_dim(output_width);
        output.add_dim(output_height);
        output.create_or_open(output.name, BLC_SEM_PROTECT, NULL);
        fprintf(stdout, "%s\n", output.name);
        fflush(stdout);
        
        switch (output.format){
            case 'yuv2':
                blc_command_add("Y0=", edit_callback, "expression", "function of y0, u, y1, v", (void*)&Y0_code);
                blc_command_add("U=", edit_callback, "expression", "function of y0, u, y1, v", (void*)&U_code);
                blc_command_add("Y1=", edit_callback, "expression", "function of y0, u, y1, v", (void*)&Y1_code);
                blc_command_add("V=", edit_callback, "expression", "function of y0, u, y1, v", (void*)&V_code);
                break;
            case 'Y800':
                blc_command_add("Y=", edit_callback, "expression", "function of y0, u, y1, v", (void*)&Y0_code);
                break;
            default:EXIT_ON_ERROR("output format not managed '%.4s' ", UINT32_TO_STRING(format_str, output.format));
        }
        
        if (filter){
            kernel=update_kernel(&gpu_target, &output, &input, filter);
            items_nbs[0]=((output.lengths[0]-1)/32)*32;
            items_nbs[1]=((output.lengths[1]-1)/32)*32;
            work_group_sizes[0]=32;
            work_group_sizes[1]=16;
        }
        initialised=1;
        blc_program_loop_init();

    }
    
    blc_program_loop_start();
    
    CVPixelBufferLockBaseAddress(videoFrame, 0);
    input.data = (char*)CVPixelBufferGetBaseAddress(videoFrame);
    if (filter) BLCL_CHECK(clEnqueueWriteBuffer(gpu_target.command_queue, cl_input, CL_TRUE, 0, input.size, input.data, 0, NULL, NULL ), "Failed to write input array!");
    else {
        output.protect();
        if (subimage){
            switch(input.format){
                case 'yuv2':
                    switch (output.format){
                        case 'Y800':
                            FOR_INV(j, output_height)
                            FOR_INV(i, output_width) output.data[i+j*output_width]=input.data[((offset_x+i)+(j+offset_y)*input_width)*2+1];
                            break;
                        case 'yuv2':
                            FOR_INV(j, output_height)
                            FOR_INV(i, output_width){
                                output.data[(i+j*output_width)*2]=input.data[(i+offset_x+(j+offset_y)*input_width)*2];
                                output.data[(i+j*output_width)*2+1]=input.data[(i+offset_x+(j+offset_y)*input_width)*2+1];
                            }
                            break;
                        default:
                            EXIT_ON_ERROR("Output format '%.4s' is not managed with input format '%.4s'.", UINT32_TO_STRING(output_format_str, output.format), UINT32_TO_STRING(input_format_str, input.format));
                    }
                    break;
                default:  EXIT_ON_ERROR("Input format '%.4s' is not managed", UINT32_TO_STRING(input_format_str, input.format));
            }
        }
        else{
            blc_image_copy(&output, &input);            
        }
        output.protect(0);
    }
    CVPixelBufferUnlockBaseAddress(videoFrame, 0);
    
    if (filter){
        BLCL_CHECK(clEnqueueNDRangeKernel(gpu_target.command_queue, kernel, 2, NULL, items_nbs, work_group_sizes, 0, NULL, NULL), "Failed to execute kernel! items_nb '%lu', work_group_size '%lu'", items_nbs[0], work_group_sizes[0]);
        BLCL_CHECK(clFinish(gpu_target.command_queue), "finishing filter");
        output.protect();
        BLCL_CHECK(clEnqueueReadBuffer(gpu_target.command_queue, cl_output, CL_TRUE, 0, output.size, output.data, 0, NULL, NULL ), "Failed to read output array!");
        output.protect(0);
    }
    
    if (display) {
        fprint_escape_command(stderr, "0,0H");
        blc_display_text_image(&output, terminal_width, terminal_height-1);
    }
    blc_program_loop_end();
    
}
void update_activation_cb(char const *, char const *argument, void *)
{
    if (strlen(argument)==0) fprintf(stderr, "%s\n", activation);
    else {
        activation=(char*)argument;
        refresh_filter=1;
    }
}

static void on_quit()
{
    
    if (output.fd != -1 ) output.unlink();
    blcl_release(&gpu_target);
}

int main(int argc, char **argv)
{
    char const *output_format_option, *filter_channel_name, *size_string, *center_string;
    
    blc_program_set_description("Acquire webcam images on Apple computer (with Quicktime).");
    blc_program_option_add(&force_ansi, 'a', "ansi", 0, "force ansi mode", NULL);
    blc_program_option_add(&output_format_option, 'f', "format", "Y800", "set ouput video format", "NDEF");
    blc_program_option_add(&filter_channel_name, 'F', "filter", "blc_channel", "filter to convolute with", NULL);
    blc_program_option_add(&center_string, 'c', "center", "(integer)x(integer)", "center of the subimage", NULL);
    blc_program_option_add(&size_string, 's', "size", "(integer)x(integer)", "size of the subimage", NULL);
    blc_program_add_channel_option(&output, 'o', "output", "output channel name", "/image");
    blc_program_add_output_pipe_option(stdout, 'O', "output-pipe", "output pipe name", NULL);

    blc_program_init(&argc, &argv, on_quit);
    
    blc_terminal_get_size(&terminal_width, &terminal_height);
    blc_terminal_set_resize_callback(terminal_resize_cb);
    
    output.format=STRING_TO_UINT32(output_format_option);

    if (center_string){
        if (sscanf(size_string, "%dx%d", &center_x, &center_y)!=2) EXIT_ON_ERROR("Parsing --center=%s", center_string);
    }

    if (size_string){
        if (sscanf(size_string, "%dx%d", &output_width, &output_height)!=2) EXIT_ON_ERROR("Parsing --size=%s", size_string);
        subimage=1;
    }
    else subimage=0;

    blc_command_add("activation=", update_activation_cb, "expression", "change the activation function", NULL);
    blc_command_add("d", command_callback, NULL, "display text image", NULL);
    blc_command_add("i", command_callback, NULL, "info", NULL);
    
    blcl_init(&gpu_target, CL_DEVICE_TYPE_GPU);
    if (filter_channel_name) filter=new blc_channel(filter_channel_name);
    init_capture(capture_cb, NULL);
    
    return 0;
}
