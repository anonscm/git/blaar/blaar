//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include "blc.h"

enum status {STOP, PAUSE, RUN};

blc_channel *channel;
char channel_name[NAME_MAX];
char data_name[NAME_MAX];
int iteration, component_offset=1;
char const *is_zoom=NULL;
double period;
int columns_nb, rows_nb, status=RUN;

static void quit()
{
    fflush(stdout);
    print_escape_command("0m");
    print_escape_command(DEL_END_SCREEN);
    printf("\nQuitting.");
}

void resize_callback(int new_columns_nb, int new_rows_nb)
{
    columns_nb = new_columns_nb;
    rows_nb = new_rows_nb;
}

void command_callback(char const *mem, void *user_data)
{
    int i;
    (void)mem;
    print_escape_command("0m");
    switch (*(char*)user_data)
    {
        case   0:if (status==RUN)
        {
            status=PAUSE;
            FOR(i, BLC_BAR_COLORS_NB-1) color_printf(blc_bar_colors[i],"%d < ", i*256/BLC_BAR_COLORS_NB);
            color_printf(blc_bar_colors[i],"%d\n", i*256/BLC_BAR_COLORS_NB);
            printf("%d iterations, average period: %.3lf ms\n", iteration, period/1000.);
            iteration=0;
            blc_command_display_help();
            blc_command_interpret();
        } else status=RUN;
            break;
        case 'c':component_offset=1-component_offset;break;
        case 'q':status=STOP;
        case 'z':if (is_zoom) is_zoom=NULL;
        else is_zoom="1";
            break;
    }
}

int main(int argc, char **argv)
{
    char const *input = NULL;
    int offset, step1, length1, step2, length2, width, height;
    uint32_t pixel_format;
    struct timeval timestamp;
    blc_image image_info;
    
    int ret, ch;
    
    program_option_add(&input, 'd', "data", 1, "input data to display");
    program_option_add(&is_zoom, 'z', "zoom", 0, "full resolution centered");
    program_option_interpret(&argc, &argv);
    
    
    //  terminal_ansi_detect();
    //if (ansi_terminal)
    terminal_get_size(&columns_nb, &rows_nb);
    if (columns_nb==0) columns_nb=32; //fake terminal
    if (rows_nb==0) rows_nb=32; //fake terminal
    
    
    terminal_set_resize_callback(resize_callback);
    blc_command_add("", command_callback, NULL, "pause/run", (void*)"");
    blc_command_add("c", command_callback, NULL, "change the component (Y-UV)", (void*)"c");
    blc_command_add("q", command_callback, NULL, "quit", (void*)"q");
    blc_command_add("z", command_callback, NULL, "zoom/unzoom", (void*)"z");
    atexit(quit);
    program_option_interpret_and_print_title(&argc, &argv);
    
    if (input==NULL)
    {
        ret = scanf( "%s", channel_name);
        if (ret != 1) EXIT_ON_ERROR("Fail reading channel name");
    }
    else strncpy(channel_name, input, NAME_MAX);
    
    
    channel = new blc_channel(channel_name, 0, "p");
    image_info.load_parameter(channel->parameter);
    width=image_info.width;
    height=image_info.height;
    pixel_format=image_info.format;
    
    while(1)
    {
        //        SYSTEM_ERROR_CHECK(sem_wait(channel->sem_block), -1, "");
        ch = getc(stdin);
        switch (ch) {
            case 'q': printf("q");
                fflush(stdout);
                exit(0);
                break;
                
            default:
                break;
        }
        
        length1=columns_nb/2;
        length2=rows_nb;
        
        if (pixel_format == STRING_TO_UINT32("Y800"))
        {
            if (is_zoom)
            {
                step1=1;
                step2= width;
                offset=(int)((width-length1)/2)+((int)(height-length2)/2)*step2;
            }
            else
            {
                step1 = (int)(width/length1);
                step2=(int)(height/length2)*width;
                offset=0; // 0 for color
            }
        }
        else
        {
            
            if (is_zoom)
            {
                step1=2;
                step2=width*2;
                offset=component_offset+(int)((width-length1)/2)*2+((int)(height-length2)/2)*step2;
            }
            else
            {
                step1 = (int)(width/length1)*2;
                step2=(int)(height/length2)*width*2;
                offset=component_offset; // 0 for color
            }
        }
        channel->protect();
        
        fprint_3Dmatrix(stdout, channel, offset, 2, 1, step1, length1, step2, length2);
        channel->protect(0);
        period= period + (diff_us_time(&timestamp)-period)/(double)iteration;
        /*     blc_command_try_to_interpret();
         if (status == STOP) exit(0);*/
        print_escape_command("0,0H");
        iteration++;
        
        
        //       SYSTEM_ERROR_CHECK(sem_post(channel->sem_ack), -1, "");
    }
    
    return 0;
}
