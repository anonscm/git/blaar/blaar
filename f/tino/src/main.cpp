#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <net/if_arp.h>
#include <net/if.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/ioctl.h>

#include "blc.h"

#define SYSTEM_COMMAND_MAX 4096
#define READ_CMD 0x16ff
#define WRITE_CMD 0x1aff
#define MAC_ADDR_LEN 6
#define MOTORS_NB 7

#pragma pack(2)
typedef struct {
  char Dest[6];
  char Src[6];
  short EtherType; /* 0x1313 pour les centrales*/
  unsigned short Length; /* nboctet apres entete*/
  short Type;
  short Bloc; /* bit 15=1 si dernier bloc*/
  int res1;
  int res2;
  short Cpt10k;
} TEnteteTrame;

typedef struct SndCmd {
  unsigned short cmd;
  unsigned int Addr;
  unsigned short Nboct;
  char _data[1502];
} TSndCmd;

#pragma pack()

static const  char *MacCentrales[8] =
  { "\0\0BIA ", "\0\0BIA!", "\0\0BIA\"", "\0\0BIA#", "\0\0BIA$", "\0\0BIA%", "\0\0BIA&", "\0\0BIA'" };
static unsigned char WinMacAddr[6] =
  { 0x00, 0x13, 0xa9, 0x8d, 0x0a, 0xa8 };

typedef struct {
  char host[64];
  char robot[64];
  char prom[64];
  char data[512];
} type_bia_server;

struct timeval timeOut;
/*----- Data management -----*/

/*----- Network management -----*/
char hostname[16];
unsigned int hostport;
struct ifreq s_ifr;
struct sockaddr_in host_addr, prom_addr;
/*----- Other -----*/
char buffer[2048];
/*----- Data transmission -----*/
int ret;
socklen_t addrlen = sizeof(struct sockaddr_in);
fd_set set;


char *interface;
int socket_fd;
struct sockaddr_ll sockaddr;


void init()
{
  char system_command[SYSTEM_COMMAND_MAX];


  sprintf(system_command, "ifconfig %s promisc", interface);
  printf("%s\n", system_command);
  if (system(system_command) != 0) EXIT_ON_ERROR("Fail to execute: `%s`. Check that you are `sudo`", system_command);

  /*----- Init data -----*/
  CLEAR(sockaddr);
  CLEAR(*buffer);

  /*----- Init network -----*/
  socket_fd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
  if (socket_fd == -1) EXIT_ON_SYSTEM_ERROR("Creating BIA socket.  Check that you are `sudo`.");

  /*----- Get the eth to be used -----*/
  /*(eth is specified on the command line cf brocker.c)*/

  /*----- Get interface index of the eth specified -----*/
  strncpy(s_ifr.ifr_name, interface, sizeof(s_ifr.ifr_name));
  ioctl(socket, SIOCGIFINDEX, &s_ifr);

  memset(&sockaddr, 0, sizeof(struct sockaddr_ll));

  sockaddr.sll_protocol = htons(ETH_P_ALL);
  sockaddr.sll_family = AF_PACKET;
  sockaddr.sll_ifindex = s_ifr.ifr_ifindex;/*physical eth output to use*/
  sockaddr.sll_hatype = ARPHRD_ETHER;
  ockaddr.sll_pkttype = 0;/*PACKET_OUTGOING;*/
  sockaddr.sll_halen = ETH_ALEN; /*defined as 6 */

  memcpy(sockaddr.sll_addr, MacCentrales[0], 6 * sizeof(char));

  /*----- Get the Mac addr of host -----*/
  /* We don't use it now because the BIA bay answer only to the window
   * computer that load the program. For now we just cheat and speak to
   * the robot as if we were the window pc
   ioctl(sockBia, SIOCGIFHWADDR, &s_ifr);
   memcpy(HostMacAddr, s_ifr.ifr_hwaddr.sa_data, MAC_ADDR_LEN);
   */

  /*----- Bind the socket to the eth specified -----*/
  if (bind(bus->socket, (struct sockaddr *) &bus->sockaddr, sizeof(struct sockaddr_ll)) != 0) PRINT_WARNING("couldn't bind sockfd");

  printf("BIA bus: bind on %s succesfull\n", interface);

}


int send_command(type_bia_bus *bus, char *data, size_t length)
{
  (void)bus;
  (void)data;
  (void)length;

  return 0;
}


int send_command_on_channel(type_bia_bus *bus, char *data, size_t length, long unsigned int channel_id)
{
  char buffer[1502];
  int size = 0;
  TEnteteTrame *header = (TEnteteTrame*) buffer;
  TSndCmd *MsgCmd = (TSndCmd*) (buffer + sizeof(TEnteteTrame));
  int i;
  struct timeval timeOut;
  fd_set set;

  if (length != 4) EXIT_ON_ERROR("You can only write 4 chars (like a float) !\n\tYou try to read %i", length);

  memset(buffer, 0x00, sizeof(buffer));

  /*----- Filling in of the message -----*/
  MsgCmd->cmd = WRITE_CMD;
  MsgCmd->Addr = channel_id;
  MsgCmd->Nboct = 4;
  memcpy(MsgCmd->_data, data, length);

  /*----- Filling in of the header -----*/
  memcpy(header->Dest, MacCentrales[0], MAC_ADDR_LEN);
  memcpy(header->Src, WinMacAddr, MAC_ADDR_LEN);
  header->EtherType = 0x1313;
  header->Length = 13;
  header->Type = 0x0002;
  header->Bloc = 0x0000;
  header->res1 = 0x00000000;
  header->res2 = 0x00000000;
  header->Cpt10k = 0x0000;

  size = 60;

  /*----- Send the message -----*/
  if ((i = sendto(bus->socket, buffer, size, 0, (struct sockaddr*)&bus->sockaddr, sizeof(struct sockaddr_ll))) < size) PRINT_WARNING("not all bytes have been sent (%d)", i);

  /*----- Wait for the message to arrive -----*/
  do
  {
    timeOut.tv_sec = 0;
    timeOut.tv_usec = 1000000;
    FD_ZERO(&set);
    FD_SET(bus->socket, &set);
    if (select(sizeof(set), &set, NULL, NULL, &timeOut) <= 0)
    {
      EXIT_ON_ERROR("select");
    }
    read(bus->socket, buffer, sizeof(buffer));
  } while (memcmp((void*) &(header->Src), (void*) MacCentrales[0], 6) != 0);

  return 0;

}

int receive_on_channel(type_bia_bus *bus, char *data, int length, long unsigned int channel_id)
{
  char buffer[1502];
  int size = 0;
  TEnteteTrame *header = (TEnteteTrame*) buffer;
  TSndCmd *MsgCmd = (TSndCmd*) (buffer + sizeof(TEnteteTrame));
  int ret, i;
  struct timeval timeOut;
  fd_set set;

  if (length != 4) EXIT_ON_ERROR("You can only read 4 char (like a float) !\n\tYou try to red %i", length);

  memset(buffer, 0x00, sizeof(buffer));

  /*----- Filling in of the message -----*/
  MsgCmd->cmd = READ_CMD;
  MsgCmd->Addr =  channel_id;
  MsgCmd->Nboct = 4;
  MsgCmd->_data[0] = 0x08;

  /*----- Filling in of the header -----*/
  memcpy(header->Dest, MacCentrales[0], MAC_ADDR_LEN);
  memcpy(header->Src, WinMacAddr, MAC_ADDR_LEN);
  header->EtherType = 0x1313;
  header->Length = 9;
  header->Type = 0x0002;
  header->Bloc = 0x0000;
  header->res1 = 0x00000000;
  header->res2 = 0x00000000;
  header->Cpt10k = 0x0000;

  size = 60;

  /*----- Send the message -----*/
  if ((i = sendto(socket, buffer, size, 0, (struct sockaddr*)&sockaddr, sizeof(struct sockaddr_ll))) < size) PRINT_WARNING("not all bytes have been sent (%d)", i);
  /*----- Wait for the message to arrive -----*/
  do
  {
    timeOut.tv_sec = 0;
    timeOut.tv_usec = 1000000;
    FD_ZERO(&set);
    FD_SET(bus->socket, &set);
    if (select(sizeof(set), &set, NULL, NULL, &timeOut) <= 0)
    {
      return -1;
    }
    /*The socket receive all the message from eth0 so we need to filter them manualy*/
    ret = read(bus->socket, buffer, sizeof(buffer));

  } while (memcmp((void*) &(header->Src), (void*) MacCentrales[0], 6) != 0);

  memcpy(data, MsgCmd, length);
  return length;
}

int main(int argc, char **argv)
{
    
    program_option_interpret_and_print_title(&argc, &argv);
    if (argc==2) interface=argv[1];
    else interface="eth0";
    
    return 0;
}

