/*
 * promihm.hh
 *
 *  Created on: Jan 9, 2015
 *      Author: arnaudblanchard
 */

#ifndef PROMIHM_HH
#define PROMIHM_HH

#include "blc_channel.h"
#include <stdint.h>

#define TO_UINT32(x) (*(uint32_t*)x)

enum { UNDEFINED_VARIABLE = 0, BYTE_VARIABLE = 'b', FLOAT_VARIABLE = 'f', INT16_VARIABLE = 'i'/*, DOUBLE_VARIABLE = 'd'*/};
enum { UNDEFINED_DISPLAY = 0, TEXT_DISPLAY = 't', VIEW_METER_DISPLAY = 'v', CHECKBOX_DISPLAY ='c', IMAGE_DISPLAY = 'i', MOUSE_DISPLAY = 'm'};
enum { DEFAULT_STATE = 0, PLAY_STATE, RECORD_STATE};

typedef struct experience {
   char const* name;
   void *display;

   experience(const char*name);
   void create_display();
} type_experience;

typedef struct variable:blc_channel {
   char type;
   char display_type;
   int state;
   char *basename;
   FILE *record_file;
   type_experience *experience;
   void *display;

   variable(char const *name, size_t size, int type,  char display_type);
   void create_display();

}type_variable;

typedef struct float_variable:type_variable {
   int values_nb;

   float_variable(char const *path, size_t size, char const *params);
   void create_display();

}type_float_variable;

typedef struct image_variable:type_variable {
   uint32_t pixel_format;
   int width, height;

   image_variable(char const *path, size_t size, char const *params);
   void create_display();

}type_image_variable;

extern int variables_nb, experiences_nb;
extern type_experience **experiences;
extern type_variable **variables;

void update_channels(char const *start_filter_name = NULL); //Cannot be threaded (global var)

#endif /* PROMIHM_HH_ */
