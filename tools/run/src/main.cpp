#include "blc.h"
#include <libgen.h> // basename
#include <ctype.h> //isspace

#define SEPARATORS " <>#|&;\n"

typedef struct process
{
    char  id[NAME_MAX];
    char const *name;
    pid_t pid;
    char **argv;
    int argc;
    struct process **next_processes;
    int next_processes_nb;
    char **input_pipes;
    int input_pipes_nb;
    char **output_pipes;
    int output_pipes_nb;
    
    process(char const *name);
    void add_arg(char const *arg);
    void add_next_process(struct process *process);
    void add_input_pipe(char const *pipe_path);
    void add_output_pipe(char const *pipe_path);
    void display_children();
    void display_children(int pos, struct process ***displayed_processes, int *displayed_processes_nb);
    int is_descendant(struct process *process, struct process **visited_processes=NULL, int visited_processes_nb=0);
    void parse_sh_script(char const *script_path);
    void export_dot(char const *filename);
    void write_dot_children(FILE *file, int pos, struct process ***displayed_processes_pt, int *displayed_processes_nb_pt);
    
}type_process;

char  script_name[NAME_MAX]={};
char  *script_path=NULL;


type_process **all_processes = NULL;
int all_processes_nb=0;

type_process::process(char const *new_name):pid(-1), argv(NULL), argc(0), next_processes(NULL), next_processes_nb(0),input_pipes(NULL), input_pipes_nb(0), output_pipes(NULL), output_pipes_nb(0)
{
    type_process *process=this;
    
    snprintf(id, NAME_MAX, "p%d", all_processes_nb);
    name = strdup(new_name);
    APPEND_ITEM(&all_processes, &all_processes_nb, &process);
}

void type_process::add_arg(char const *arg)
{
    char const *arg_copy =  strdup(arg);
    APPEND_ITEM(&argv, &argc, &arg_copy);
}

void type_process::add_input_pipe(char const *pipe_path)
{
    char const *pipe_path_copy =  strdup(pipe_path);
    int i, j;
    type_process *process;
    
    
    // If we fine a process with the same out_pipe, they are conceted
    FOR(i,  all_processes_nb)
    {
        process=all_processes[i];
        FOR(j, process->output_pipes_nb)
        {
            if (strcmp(pipe_path, process->output_pipes[j])==0) process->add_next_process(this);
        }
    }
    APPEND_ITEM(&input_pipes, &input_pipes_nb, &pipe_path_copy);
    
}

void type_process::add_output_pipe(char const *pipe_path)
{
    char const *pipe_path_copy =  strdup(pipe_path);
    int i, j;
    type_process *process;
    
    FOR(i,  all_processes_nb)
    {
        process=all_processes[i];
        FOR(j, process->input_pipes_nb)
        {
            if (strcmp(pipe_path, process->input_pipes[j])==0) add_next_process(process);
        }
    }
    APPEND_ITEM(&output_pipes, &output_pipes_nb, &pipe_path_copy);
    
}

void type_process::add_next_process(type_process *process)
{
    APPEND_ITEM(&next_processes, &next_processes_nb, &process);
}


void type_process::display_children(int pos, type_process ***displayed_processes_pt,  int *displayed_processes_nb_pt)
{
    int i, j;
    int next_position=pos;
    type_process *child;
    
    FOR(i, next_processes_nb)
    {
        child = next_processes[i];
        
        // Check that the process is not already displayed.
        if (GET_ITEM_POSITION(*displayed_processes_pt, *displayed_processes_nb_pt, &child) == -1)
        {
            if (i!=0) printf("%*c", pos, ' ');
            
            if (pos!=0) next_position+=printf("->");
            next_position+=printf("[ %s ", child->name);
            FOR(j, child->argc) next_position+=printf("%s ", child->argv[j]);
            next_position+=printf("]%d ", next_position);
            
            APPEND_ITEM(displayed_processes_pt, displayed_processes_nb_pt, &child);
            child->display_children(next_position, displayed_processes_pt, displayed_processes_nb_pt);
            next_position=0;
        }
        else if (pos!=0) printf("%d\n", pos);
    }
}

void type_process::display_children()
{
    // Init the array of already dislayed processes
    type_process **displayed_processes = NULL;
    int displayed_processes_nb = 0;
    
    display_children(0, &displayed_processes, &displayed_processes_nb);
    FREE(displayed_processes);
}

void type_process::write_dot_children(FILE *file, int pos, type_process ***displayed_processes_pt, int *displayed_processes_nb_pt)
{
    int i;
    type_process *child;
    
    FOR(i, next_processes_nb)
    {
        child = next_processes[i];
        if (GET_ITEM_POSITION(*displayed_processes_pt, *displayed_processes_nb_pt, &child) == -1)
        {
            fprintf(file, "\t%s -> ", child->id);
            APPEND_ITEM(displayed_processes_pt, displayed_processes_nb_pt, &child);
            child->write_dot_children(file, 1, displayed_processes_pt, displayed_processes_nb_pt);
        }
        else if (pos!=0) fprintf(file, "%s [constraint=false];\n", child->id);
    }
}

void type_process::export_dot(char const *filename)
{
    FILE *file;
    int i, j;

    struct process *process;
    struct process **displayed_processes=NULL;
    int displayed_processes_nb=0;
    
    SYSTEM_ERROR_CHECK(file = fopen(filename, "w"), NULL, "exporting '%s' on '%s'", script_path, filename);
    fprintf(file, "digraph %s {\n", script_name);
    fprintf(file, "\trankdir=LR;\n");
    fprintf(file, "\tnode [shape=rectangle];\n");
    FOR(i, all_processes_nb)
    {
        process=all_processes[i];
        fprintf(file, "\t%s [label=\"%s ", process->id, process->name);
        FOR(j, process->argc)  fprintf(file, "%s ", process->argv[j]);
        fprintf(file, "\"];\n");
    }
    write_dot_children(file, 0, &displayed_processes, &displayed_processes_nb);
    FREE(displayed_processes);
    fprintf(file, "}\n");
    fclose(file);
    
}
/************** callbacks *******************/
void export_graphviz_callback(char const *argument, void *user_data)
{
    ((struct process*)user_data)->export_dot(argument);
}

void run_callback(char const *argument, void *user_data)
{
    (void) argument;
    (void) user_data;
    char command_line[LINE_MAX];
    int ret;
    long us_time;
    struct timeval timestamp={};
    
    snprintf(command_line, LINE_MAX, "./%s", script_path);
    diff_us_time(&timestamp);
    ret = system(command_line);
    us_time = diff_us_time(&timestamp);
    printf("Executed in '%lf's", us_time/1000000.);
    if (ret!=0) PRINT_WARNING("Return code error: '%d'", ret);
}

void quit_callback(char const *argument, void *user_data)
{
    (void) argument;
    (void) user_data;
    exit(0);
}

void type_process::parse_sh_script(char const *script_path)
{
    char arg[LINE_MAX+1];
    char pipe_path[PATH_MAX];
    int separator = 0, ret;
    FILE *file;
    type_process *process, *previous_process = NULL;
    
    SYSTEM_ERROR_CHECK(file = fopen(script_path, "r"), NULL, "Opening: %s", script_path);
    while(!feof(file))
    {
        fscanf(file, "%*[ \t\n]");  //remove spaces, tabs and empty lines
        fscanf(file, "#%*[^\n]"); // remove comments
        if (fscanf(file, "tee%*[ ]%[^ "SEPARATORS"]", pipe_path)==1)
        {
            do
            {
                if (pipe_path[0] != '-')
                {
                    previous_process->add_output_pipe(pipe_path);
                }
                ret = fscanf(file, "%*[ ]%[^"SEPARATORS"]", pipe_path);
            }while(ret>0);
            separator=getc(file);
        }
        else
        {
            if (fscanf(file, SCAN_CONV(LINE_MAX, "[^"SEPARATORS"]"), arg)==1)
            {
                process=new type_process(arg);
                if (separator == '|') previous_process->add_next_process(process);
                else add_next_process(process);
                
                do
                {
                    fscanf(file, "%*[ \t]"); //remove spaces
                    fscanf(file, "#%*[^\n]"); // remove comments
                    
                    if (fscanf(file, SCAN_CONV(LINE_MAX, "[^"SEPARATORS"]"), arg)==1)
                    {
                        process->add_arg(arg);
                    }
                    separator=fgetc(file);
                    
                    switch (separator)
                    {
                            case '<': FSCANF(1, file, "%s", pipe_path);
                            process->add_input_pipe(pipe_path);
                            break;
                            case '>':FSCANF(1, file, "%s", pipe_path);
                            process->add_output_pipe(pipe_path);
                            break;
                        default :
                            break;
                            
                    }
                }while(strchr("|&\n;", separator) == NULL);
                previous_process = process;
            }
        }
    }
}





int main(int argc, char **argv)
{
    char const *filename = NULL;
    char const *help=NULL;
    char const *dot_filename=NULL;
    char const *interactive_mode=NULL;

    type_process root_process(argv[0]);
    
    terminal_ansi_detect();
    program_option_add(&dot_filename, '\0', "dot", 1, "Export in dot format.");
    program_option_add(&help, 'h', "help", 0, "Display this help");
    program_option_add(&interactive_mode, 'i', "interactive", 0, "Interactive mode.");

    program_option_interpret_and_print_title(&argc, &argv);
    
    if (help) program_option_display_help();
    
    
    if (argc == 2)
    {
        script_path = argv[1];
        filename = basename(script_path);
        if (sscanf(filename, "%[^.].sh", script_name) == 0) EXIT_ON_ERROR("The format of the filename '%s' is not correct. The extention should be '.sh'");
        root_process.parse_sh_script(script_path);
    }
    if (dot_filename) root_process.export_dot(dot_filename);
    
    blc_command_add("dot", export_graphviz_callback, "filename", "export the architecture as a graphviz (.dot).", &root_process);
    blc_command_add("r", run_callback, NULL, "run the architecture.", NULL);
    blc_command_add("q", quit_callback, NULL, "quit the app.", NULL);
    
    while(interactive_mode)
    {
        root_process.display_children();
        printf("\n");
        blc_command_display_help();
        blc_command_interpret();
    }
    return 0;
}

