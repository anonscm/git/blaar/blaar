# Set the minimum version of cmake required to build this project
cmake_minimum_required(VERSION 2.6)


# Set the name and the supported language of the project
get_filename_component(PROJECT_NAME ${CMAKE_SOURCE_DIR} NAME)
project(${PROJECT_NAME})

get_filename_component(blaa_dir ${CMAKE_SOURCE_DIR} PATH)
get_filename_component(blaa_build_dir ${CMAKE_BINARY_DIR} PATH)
get_filename_component(blaa_dir ${blaa_dir} PATH)
get_filename_component(blaa_build_dir ${blaa_build_dir} PATH)

add_definitions(-Wextra -Wall)
#We set blc and blQTKit as source of the project. This is only useful for developer of blc. However, it defines shared_blc.
add_subdirectory(${blaa_dir}/blc ${blaa_build_dir}/blc_build)
set(EXECUTABLE_OUTPUT_PATH  ${blaa_build_dir}/bin)

include_directories(${blaa_dir}/blc/include)
add_executable(${PROJECT_NAME} src/main.cpp)
target_link_libraries(${PROJECT_NAME} static_blc)

if(UNIX AND NOT APPLE)
target_link_libraries(${PROJECT_NAME} rt)
endif()

# *** install ***
install(TARGETS ${PROJECT_NAME} DESTINATION bin)





