#!/bin/sh

 ./compile.sh $1 Release doc_$1

blar_build_dir="${PWD}_build"

if [ $? ] ; then
if type xdg-open 2>/dev/null; then xdg-open "$blar_build_dir/doc/$1/html/index.html";
elif type open 2>/dev/null; then open "$blar_build_dir/doc/$1/html/index.html";
else echo "Open documentation in: $blar_build_dir/doc/$1/html/index.html"; fi
else
echo "Failing building the documentation"
fi




