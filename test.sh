#!/bin/sh

current_dir=`pwd`
parent_dir=`dirname $current_dir`

./compile.sh $1 >&2
fail=$?
cd $current_dir

if [ $fail != 0 ];
then
echo "fail compiling '$1'"
exit 1
fi

bin_dir="$parent_dir/`basename $current_dir`_build/release_bin"

program=`basename $1`
shift    #remove $0 i.e. test.sh
echo >&2
echo "\nexecute: $bin_dir/$program $*" >&2
rlwrap --no-warnings  $bin_dir/$program $*

