#include <string.h>
#include <fcntl.h>
#include "blc.h"
#include "blc_channel.h"

#include <sys/ioctl.h>
#include <unistd.h>
#include <linux/videodev2.h>
#include <jpeglib.h>

#include <errno.h>
#include <limits.h>

#define V4L_REQUEST(arg, request_value) request(arg, request_value, #request_value)
#define V4L_ASSERT(arg, request) if (!((arg) & (request))) EXIT_ON_ERROR("Device %s not %s", this->name, #request);

typedef struct device {
   blc_channel *channel;
   blc_channel *output_channel;

   const char *name;
   int fd;

   device(const char *name);
   void start();
   void request(void *arg, int request, char const *request_name);

} v4l_device;

v4l_device device("/dev/video0");

void convert_JPEG_to_RGB3(blc_mem &compressed_mem, blc_mem &output_mem)
{
   JSAMPROW row_pointer;
   int offset = 0;
   int row_stride, count = 0;
   uchar *pixels;
   struct jpeg_decompress_struct cinfo;
   struct jpeg_error_mgr jerr;

   cinfo.err = jpeg_std_error(&jerr);
   jpeg_create_decompress(&cinfo);
   jpeg_mem_src(&cinfo, (uchar*)compressed_mem.data, compressed_mem.size);
   jpeg_read_header(&cinfo, TRUE);
   jpeg_start_decompress(&cinfo);

   row_stride = cinfo.output_width * cinfo.output_components;
   row_pointer = (uchar*)output_mem.data;

   while (cinfo.output_scanline < cinfo.output_height)
   {
      jpeg_read_scanlines(&cinfo, &row_pointer, 1);
      row_pointer+=row_stride;
   }

   jpeg_finish_decompress(&cinfo);
   jpeg_destroy_decompress(&cinfo);
}

v4l_device::device(const char *name) :
      name(name)
{
   struct v4l2_capability cap;
   struct v4l2_format fmt;
   char channel_name[NAME_MAX+1];

   fd = open(name, O_RDWR /* required */, 0);
   if (fd == -1) EXIT_ON_SYSTEM_ERROR("Fail to open %s", name);

   V4L_REQUEST(&cap, VIDIOC_QUERYCAP);
   V4L_ASSERT(cap.capabilities, V4L2_CAP_VIDEO_CAPTURE);
   V4L_ASSERT(cap.capabilities, V4L2_CAP_READWRITE);

   CLEAR(fmt);
   fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
   V4L_REQUEST(&fmt, VIDIOC_G_FMT);

 //  printf("\nPixel format: %.4s\n", (char*)&fmt.fmt.pix.pixelformat);

   snprintf(channel_name, NAME_MAX, "v4l.demo-tbi%.4sw%dh%d", (char*)&fmt.fmt.pix.pixelformat, fmt.fmt.pix.width, fmt.fmt.pix.height);
   channel = new blc_channel(channel_name,  fmt.fmt.pix.sizeimage);
 //  output_channel = new blc_channel("v4l.demo-tbiRGB3w640h480", fmt.fmt.pix.height*fmt.fmt.pix.width*3);
}

void v4l_device::start()
{
   char const *spinner="-~=#=~";
   fd_set fds;
   int result, i=0;
   struct timeval tv, previous_time = {0,0};

   while (1)
   {
      FD_ZERO(&fds);
      FD_SET(fd, &fds);

      /* Timeout. */
      tv.tv_sec = 1;
      tv.tv_usec = 0;

      result = select(fd + 1, &fds, NULL, NULL, &tv);

      if (result == -1)
      {
         if (EINTR == errno) continue;
         else EXIT_ON_SYSTEM_ERROR("Select on device %s", name);
      }
      else if (result == 0) EXIT_ON_ERROR("Timeout device %s", name);

      result = read(fd, channel->data, channel->size);
      if (result == -1) EXIT_ON_SYSTEM_ERROR("device %s", name);

     /* convert_JPEG_to_RGB3((blc_mem&)*channel, (blc_mem&)*output_channel);*/
      printf("%c : %7.3fHz  \n", spinner[i], 1000000./(double)(diff_us_time(&previous_time)));
      print_escape_command(MOVE_N_UP_CURSOR, 1);
      if (i==5) i=0;
      else i++;
   }
}

void v4l_device::request(void *arg, int request, char const *request_name)
{
   while (ioctl(fd, request, arg) == -1)
   {
      if (errno == EINTR) continue;
      else EXIT_ON_SYSTEM_ERROR("v4l2 error with request %s on device %s", request_name, name);
   }
}

int main(int argc, char *argv[])
{
   device.start();
   return 0;
}

