#include <string.h>
#include <fcntl.h>
#include "blc.h"
#include "blv4l2.h"

#include <sys/ioctl.h>
#include <unistd.h>
#include <jpeglib.h>

#include <sys/mman.h>
#include <errno.h>
#include <limits.h>

#define V4L_REQUEST(arg, request_value) request(arg, request_value, #request_value)
#define V4L_ASSERT(arg, request) if (!((arg) & (request))) EXIT_ON_ERROR("Device %s not %s", this->name, #request);

static blc_narray image;

enum {
   READ_WRITE_MODE = 0, MMAP_MODE
};

enum { STOP = 0,  RUN};

void blv4l2_device::request(void *arg, int request, char const *request_name)
{
   while (ioctl(fd, request, arg) == -1)
   {
      if (errno == EINTR) continue;
      else EXIT_ON_SYSTEM_ERROR("v4l2 error with request %s on device %s", request_name, name);
   }
}


void blv4l2_device::init(const char *name)
{
   struct v4l2_capability cap;
   struct v4l2_format format;
   this->name=name;

   fd = open(name, O_RDWR /* required */, 0);
   if (fd == -1) EXIT_ON_SYSTEM_ERROR("Fail to open %s", name);

   V4L_REQUEST(&cap, VIDIOC_QUERYCAP);
   V4L_ASSERT(cap.capabilities, V4L2_CAP_VIDEO_CAPTURE);

   if (cap.capabilities & V4L2_CAP_READWRITE) mode = READ_WRITE_MODE;
   else if (cap.capabilities & V4L2_CAP_STREAMING) mode = MMAP_MODE;

   CLEAR(format);
   format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
   V4L_REQUEST(&format, VIDIOC_G_FMT);
   blc_narray_init_image(&image, 'UIN8', format.fmt.pix.pixelformat, format.fmt.pix.width, format.fmt.pix.height);
}

void blv4l2_device::init_mmap()
{/*
   enum v4l2_buf_type type;
   unsigned int buffers_nb;
   struct v4l2_requestbuffers v4l2_request_buffers;

   CLEAR(v4l2_request_buffers);

   v4l2_request_buffers.count = 2;
   v4l2_request_buffers.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
   v4l2_request_buffers.memory = V4L2_MEMORY_MMAP;

   V4L_REQUEST(&v4l2_request_buffers, VIDIOC_REQBUFS);

   if (v4l2_request_buffers.count < 2) EXIT_ON_ERROR("Insufficient buffer memory on %s\n", name);
   buffers_nb = v4l2_request_buffers.count;

   CLEAR(v4l2_buffer);
   v4l2_buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
   v4l2_buffer.memory = V4L2_MEMORY_MMAP;

   buffer_mems = MANY_ALLOCATIONS(buffers_nb, blc_mem);
   CLEAR(v4l2_buffer);
   v4l2_buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
   v4l2_buffer.memory = V4L2_MEMORY_MMAP;

   FOR(v4l2_buffer.index, buffers_nb)
   {
      V4L_REQUEST(&v4l2_buffer, VIDIOC_QUERYBUF);

      buffer_mems[v4l2_buffer.index].size = v4l2_buffer.length;
      buffer_mems[v4l2_buffer.index].data = (char*) mmap(NULL /* start anywhere */, v4l2_buffer.length, PROT_READ | PROT_WRITE /* required */, MAP_SHARED /* recommended */, fd, v4l2_buffer.m.offset);

      if (buffer_mems[v4l2_buffer.index].data == MAP_FAILED) EXIT_ON_SYSTEM_ERROR("mmap");

      V4L_REQUEST(&v4l2_buffer, VIDIOC_QBUF);
   }

   type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
   V4L_REQUEST(&type, VIDIOC_STREAMON);

   v4l2_buffer.index = 0;*/
}

void blv4l2_device::get_read_write(char *image_data, size_t size)
{
   fd_set fds;
   int result;
   struct timeval tv =
      { 1, 0 }; //timeout 1s

   FD_ZERO(&fds);
   FD_SET(fd, &fds);

   do
   {
      result = select(fd + 1, &fds, NULL, NULL, &tv);
      if ((result == -1) && (errno != EINTR)) EXIT_ON_SYSTEM_ERROR("Select on device %s", name); // EINTR means that the call has been interupted
      else if (result == 0) EXIT_ON_ERROR("Timeout device %s", name);
   } while (result <= 0);

   result = read(fd, image_data, size);
   if (result == -1) EXIT_ON_SYSTEM_ERROR("device %s", name);
}

void blv4l2_device::start(void (*callback)(blc_narray *image, void*), void *user_data)
{
   state = RUN;
   switch (mode)
   {
   case READ_WRITE_MODE:
      while (state == RUN)
      {
         get_read_write(image.data, image.size);
         callback(&image, user_data);
      }
      break;
   case MMAP_MODE:
      init_mmap();
      while (state == RUN)
      {
         V4L_REQUEST(&v4l2_buffer, VIDIOC_DQBUF);
      //   callback(&buffer_mems[v4l2_buffer.index], user_data);
         V4L_REQUEST(&v4l2_buffer, VIDIOC_QBUF);
      }
      break;
   default:
      EXIT_ON_ERROR("Mode %d is not managed.", mode);
   }
}
