#ifndef BLV4L2_H
#define BLV4L2_H

#include "blc_channel.h"
#include <linux/videodev2.h>

typedef struct device {
   blc_narray image;
   struct v4l2_buffer v4l2_buffer;
   const char *name;
   int fd, mode, state;

   ///init the camera with the path ( something like /dev/video0 )but do not start the acquisition.
   void init(char const *path);
   void get_read_write(char *image_data, size_t size);
   void init_mmap();
   void request(void *arg, int request, char const *request_name);

   ///start the acquisition and call 'callback' with the image data for each new image.
   void start(void(*callback)(blc_narray *image, void*), void *user_data);
}blv4l2_device;


#endif
