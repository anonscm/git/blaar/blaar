
#include "blc.h"
#include "blc_channel.h"
#include "blv4l2.h"
#include <string.h>


char *device_name = "/dev/video0";
int sprite_id = 0;
struct timeval previous_time =
   { 0, 0 };

blc_channel *image_channel, *mouse_channel;


void update_info()
{
   char const *spinner = "-~=#=~";
   printf("%c : %7.3fHz  \n", spinner[sprite_id], 1000000. / (double) (diff_us_time(&previous_time)));
   printf("Mouse %6d, %6d, %6d\n", *(int16_t*) (mouse_channel->data), *(int16_t*) (mouse_channel->data + 2), *(int16_t*) (mouse_channel->data + 4));
   print_escape_command(MOVE_N_UP_CURSOR, 2);
   if (sprite_id == 5) sprite_id = 0;
   else sprite_id++;
}

void callback(blc_mem *image_mem)
{
   if (image_mem->size > image_channel->size) EXIT_ON_ERROR("The image is bigger than the received buffer.");
   memcpy(image_channel->data, image_mem->data, image_mem->size);
   update_info();
}

int main(int argc, char *argv[])
{
   char channel_name[NAME_MAX+1];

   if (argc == 2) device_name = argv[1];
   else if (argc > 2) EXIT_ON_ERROR("You can only have one or less argument which is the device_name.");

   v4l_device device(device_name);

   snprintf(channel_name, NAME_MAX, "v4l2.demo-tbi%.4sw%dh%d", (char*) &device.fmt.fmt.pix.pixelformat, device.fmt.fmt.pix.width, device.fmt.fmt.pix.height);
   image_channel = new blc_channel(channel_name, device.fmt.fmt.pix.sizeimage);

   snprintf(channel_name, NAME_MAX, "v4l2.demo-tim");
   mouse_channel = new blc_channel(channel_name, 3 * sizeof(int16_t));

   printf("Execute : epimethe v4l2&\n");
   system("epimethe v4l2&");

   device.start(callback);


   return 0;
}

