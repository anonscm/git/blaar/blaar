#!/bin/bash

current_path=$PWD

if (( $# != 3 )) 
then
echo "You need 2 or 3 arguments but you only have $#" 
echo "Usage : create_project project_directory build_directory platform"
exit 1
fi

case "$3" in
	"eclipse") generator="Eclipse CDT4 - Unix Makefiles";;
	"xcode") generator="Xcode";;
	"*") echo "The platform can be 'eclipse' or 'xcode', '$3' is unknowned"
	exit 1;;
esac

build_dir=$2/$3/$1

mkdir -p "$build_dir"


cd "$build_dir" && cmake -G"$generator" "$current_path/$1" -DCMAKE_BUILD_TYPE="debug"
echo 
echo "Project created in $build_dir"
echo
