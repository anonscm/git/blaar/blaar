#install homebrew
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

#install needed packages
brew install git cmake doxygen pkgconfig gtk+3 vte3 gnome-icon-theme gnuplot ps lsof graphviz

#download the sources
git clone git://git.renater.fr/blar.git
cd blar

#make the link to the bin librairies
ln -s  ../blar_build/Release_bin bin
ln -s  ../blar_build/Release_lib lib

./compile.sh blaar

find . -name "tools/*" -exec ./compile.sh {} \;
find . -name "i_*" -exec ./compile.sh {} \;
find . -name "f_*" -exec ./compile.sh {} \;