//
//  Created by Arnaud Blanchard on 22/12/15.
//  Copyright ETIS 2015. All rights reserved.
//

#include "blcl.h"
#include "blc.h"

blc_channel channel;
int pipe_mode;
struct blcl_target target;
char const *display, *expr_arg;
char *expr;
int width, height;

void display_mask()
{
    int i, j;
    
    fprintf(stderr, "\n");
    FOR(j,height)
    {
        FOR (i, width) fprintf(stderr, "%12.3f ", ((float*)channel.data)[i+j*channel.lengths[0]]);
        fprintf(stderr, "\n");
    }
}

void create_mask_cb(char const*, char const *argument, void *)
{
    cl_int error_id;
    char *source_code=NULL;
    cl_mem mask;
    cl_kernel kernel;
    size_t work_group_sizes[2]={1, 1};
    size_t items_nbs[2];
    
    if (strlen(argument)==0) {
        fprintf(stderr, "%s\n", expr);
        return;
    }
    free(expr);
    
    expr=strdup(argument);
    
    items_nbs[0]=width;
    items_nbs[1]=height;
    
    if (target.device_type==CL_DEVICE_TYPE_CPU) {BLCL_ERROR_CHECK(mask = clCreateBuffer(target.context,  CL_MEM_USE_HOST_PTR , channel.size, channel.data, &error_id), NULL, error_id, NULL);}
    else{
        BLCL_ERROR_CHECK(mask=clCreateBuffer(target.context, CL_MEM_WRITE_ONLY, channel.size, NULL, &error_id), NULL, error_id,  NULL);
    }
    
    asprintf(&source_code,"#define WIDTH %d\n#define HEIGHT %d\n"\
             "float gaussian(float x, float sigma);\n"\
             "float gaussian2d(float x, float sigmax, float y, float sigmay);\n"\
             "float gaussian(float x, float sigma){\n"\
             "return 1/(sqrt(2*M_PI_F)*sigma)*exp(-(x*x)/(2*sigma*sigma));\n"\
             "}\n"\
             "float gaussian2d(float x, float sigmax, float y, float sigmay){\n"\
             "return 1/(2*M_PI_F*sigmax*sigmay)*exp(-((x*x)/(2*sigmax*sigmax)+(y*y)/(2*sigmay*sigmay)));\n"\
             "}\n"\
             "kernel void create_filter(global float *mask){\nint i, j; float x, y; i=get_global_id(0); j=get_global_id(1);x=i-WIDTH/2.f+0.5f; y=j-HEIGHT/2.f+0.5f;\n"\
             "mask[i+j*WIDTH]=%s;\n"\
             "}", channel.lengths[0], channel.lengths[1], argument);
    kernel=BLCL_CREATE_KERNEL_1P(&target, "create_filter", source_code, mask);
    FREE(source_code);
    BLCL_CHECK(clEnqueueNDRangeKernel(target.command_queue, kernel, 2, NULL, items_nbs, work_group_sizes, 0, NULL, NULL), "Failed to execute kernel!");
    BLCL_CHECK(clFinish(target.command_queue), "finishing");
    
    if (target.device_type==CL_DEVICE_TYPE_GPU) BLCL_CHECK(clEnqueueReadBuffer(target.command_queue, mask, CL_TRUE, 0, channel.size, channel.data, 0, NULL, NULL ), "Failed to write input array!");
    
    if (pipe_mode) {
        printf(".");
        fflush(stdout);
    }
    clReleaseKernel(kernel);
    clReleaseProgram(target.program);
    
    if (display) display_mask();
}

void on_quit()
{
    blcl_release(&target);
}

int main(int argc, char **argv)
{
    char const *format_string, *size_string, *type_string;
    int dims_nb, *lengths;
    
    blc_program_option_add(&display, 'd', "display", 0, "print mask", NULL);
    blc_program_option_add(&expr_arg, 'e', "expr", "expression", "c99 expression function of x and y.", "0"); //m|M are usualy for min and Max
    blc_program_option_add(&format_string, 'f', "format", "TEXT|Y800", "set prefered format", "NDEF");
    blc_program_option_add(&size_string, 's', "size", "(integer)x(integer)", "size of the mask", "3x3");
    blc_program_option_add(&type_string, 't', "type", "FL32", "type of the value of the mask", "FL32"); // Soon should also be UIN8
    blc_program_add_channel_option(&channel, 'o', "output",  "output channel name", "/mask");
    blc_program_init(&argc, &argv, on_quit);
    
    dims_nb=blc_sscan_dims(&lengths, size_string);
    switch (dims_nb){
        case 1: width=lengths[0];
            height=1;
            channel.create_or_open(BLC_SEM_PROTECT, STRING_TO_UINT32(type_string),  STRING_TO_UINT32(format_string), NULL, 1, width);
            break;
        case 2: width=lengths[0];
            height=lengths[1];
            channel.create_or_open(BLC_SEM_PROTECT, STRING_TO_UINT32(type_string), STRING_TO_UINT32(format_string), NULL, 2, width, height);
            break;
        default: EXIT_ON_ERROR("Dim nb of %d is not managed.", dims_nb);
    }
    FREE(lengths);
    
    printf("%s\n", channel.name);
    fflush(stdout);
    
    blcl_init(&target, CL_DEVICE_TYPE_GPU);
    create_mask_cb(NULL, expr_arg, NULL);
    
    blc_command_add("e=", create_mask_cb, "expression", "c99 expression function of x and y", NULL);
    blc_command_add("d", (type_blc_command_cb)display_mask, NULL, "display mask", NULL);
    blc_command_add("q", (type_blc_command_cb)exit, NULL, "Quit", NULL);
    blc_command_interpret_loop("h");
    
    return 0;
}
