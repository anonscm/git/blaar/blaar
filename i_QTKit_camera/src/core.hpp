#ifndef CORE_HPP
#define CORE_HPP

#include <stdint.h>
#include "blc_channel.h"

enum status {STOP, PAUSE, RUN};


extern char const *force_ansi;

extern double period;
extern int iteration;
extern int status;
extern int scale;
extern blc_channel *channel;
extern uint32_t output_format;
extern blc_mem input_image;

#endif