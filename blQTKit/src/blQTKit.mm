//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include "blQTKit.h"
#include "blc.h"


#import <QTKit/QTKit.h>
#import <CoreVideo/CVPixelBuffer.h> 

@interface capture:NSObject
{
@public
	void *user_data;
	void (*callback)(CVImageBufferRef imageBufferRef, void *user_data);
}
@end


@implementation capture
-(void)captureOutput:(QTCaptureOutput *)captureOutput didDropVideoFrameWithSampleBuffer:(QTSampleBuffer *)sampleBuffer fromConnection:(QTCaptureConnection *)connection
{
	(void)captureOutput;
	(void)sampleBuffer;
	(void)connection;
}

-(void)captureOutput:(QTCaptureOutput *)captureOutput didOutputVideoFrame:(CVImageBufferRef)videoFrame withSampleBuffer:(QTSampleBuffer *)sampleBuffer fromConnection:(QTCaptureConnection *)connection
{
	(void)captureOutput;
	(void)sampleBuffer;	
	(void)connection;
	
	callback(videoFrame, user_data);
}
@end

START_EXTERN_C
void init_capture(void (*callback)(CVImageBufferRef, void*), void*user_data)
{
	NSError *error; 
	QTCaptureSession					*capture_session;
	QTCaptureDevice						*device;
	QTCaptureDeviceInput				*device_input;
	QTCaptureDecompressedVideoOutput	*decompressed_video_output;
	
	capture *capture_instance = [[capture alloc] init];
	
	capture_instance->callback = callback;
	capture_instance->user_data = user_data;

	capture_session = [QTCaptureSession new];	
	device = [QTCaptureDevice defaultInputDeviceWithMediaType:QTMediaTypeVideo];
	if (![device open:&error]) EXIT_ON_ERROR("Opening device", [error localizedDescription]);			
	
	device_input = [[QTCaptureDeviceInput alloc] initWithDevice:device];
	if (![capture_session addInput:device_input error:&error]) EXIT_ON_ERROR("Setting Decompressed output.\n\t", [error localizedDescription]);
	
	decompressed_video_output = [QTCaptureDecompressedVideoOutput new];
	if(![capture_session addOutput:decompressed_video_output error:&error]) EXIT_ON_ERROR("Setting Decompressed output.\n\t", [error localizedDescription]);
	
	[decompressed_video_output setAutomaticallyDropsLateVideoFrames:YES];
	[decompressed_video_output setDelegate:capture_instance];

    [NSApplication sharedApplication];
    [NSApp setActivationPolicy:NSApplicationActivationPolicyRegular]; //doe not work on SDK 10.5
    [NSApp activateIgnoringOtherApps:YES];
    
	[capture_session startRunning];
    [NSApp run];
	
}
END_EXTERN_C